$(document).ready(function () {
    $('input[name="change_password"]').on('ifChecked', function (event) {
        $(this).closest('.form-group').next().show(500);
        $(this).closest('.form-group').next().next().show(500);
        $(this).closest('.form-group').next().next().next().show(500);
    });

    $('input[name="change_password"]').on('ifUnchecked', function (event) {
        $(this).closest('.form-group').next().hide(500);
        $(this).closest('.form-group').next().next().hide(500);
        $(this).closest('.form-group').next().next().next().hide(500);
    });
});
