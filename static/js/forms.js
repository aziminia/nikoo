$(document).ready(function () {
    // $('.select2_combo').select2();
    $("#register_form").submit(function () {
        $("#register_form").find(":disabled").removeAttr('disabled');
    });

    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green'
    });

    $('.input-group.date').datepicker({
        format: 'yyyy/mm/dd',
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    $('.jalali_date input').datepicker({
        dateFormat: 'yy/mm/dd',
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });
});