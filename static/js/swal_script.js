$(document).ready(function () {
    $('.swal_del').click(function (event) {
        event.preventDefault();
        var lnk = $(this);
        swal({
                title: "آیا برای حذف مطمئن هستید؟",
                text: "در صورت حذف امکان بازیابی اطلاعات وجود نخواهد داشت",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "بله",
                cancelButtonText: "خیر",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    window.location = lnk.attr('href');
                    swal("حذف شد!", "اطلاعات با موفقیت حذف شد", "success");
                } else {
                    swal("لغو شد!", "عملیات لغو شد", "error");
                }
            });
    });

    $('.swal_del_role').click(function (event) {
        event.preventDefault();
        var lnk = $(this);
        swal({
                title: "آیا برای حذف مطمئن هستید؟",
                text: "در صورت حذف امکان بازیابی اطلاعات وجود نخواهد داشت",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "بله",
                cancelButtonText: "خیر",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    var base_url = lnk.attr('href');
                    var split_url = base_url.split('/');
                    var last_param = split_url[split_url.length-2];
                    var url = "/iranma/check_role/" + last_param + "/";
                    $.ajax({
                        url: url,
                        type: "post",
                        success: function (result, status, t) {
                            if (result == 'Not Exist'){
                                window.location = lnk.attr('href');
                                swal("حذف شد!", "اطلاعات با موفقیت حذف شد", "success");
                            }
                            else{
                                swal("خطا!", "این نقش در حال استفاده است. نمی توانید حذف کنید", "error");
                            }
                        }
                    });
                } else {
                    swal("لغو شد!", "عملیات لغو شد", "error");
                }
            });
    });
});

