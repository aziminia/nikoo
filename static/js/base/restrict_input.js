$(function () {
    function limit_input(event) {
        if (event.charCode == 0) {
            return true
        }
        var regex = new RegExp("^[0-9.]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            var parent_div = $(this).closest('div[class="col-sm-10"]');
            var list_span = parent_div.find('span');
            list_span.remove();
            var alert_span = $('<span/>').addClass('help-block m-b-none ' +
                'limit_alert').css('color', '#ff0000').html('لطفا صفحه کلید خود را در وضعیت انگلیسی قرار دهید');
            parent_div.append(alert_span);
            event.preventDefault();
            return false;
        }
        else {
            var parent_div = $(this).closest('div[class="col-sm-10"]');
            var list_span = parent_div.find('span');
            list_span.remove();
        }
        return true;
    }

    function calculate_final_price(event) {
        var price_constant = $("input[name='price_constant']").val();
        var index_price = $("input[name='index_price']").val();
        $("input[name='price_final']").val(parseInt(price_constant)+parseInt(index_price));
    }

    function calculate_fixed_price(event) {
        var price_final = $("input[name='price_final']").val();
        var penalty_award = $("input[name='penalty_award']").val();
        $("input[name='fixed_price']").val(parseInt(price_final)+parseInt(penalty_award));
    }


    $("input[name='price_constant']").bind('keyup', calculate_final_price);
    $("input[name='index_price']").bind('keyup', calculate_final_price);

    $("input[name='penalty_award']").bind('keyup', calculate_fixed_price);
    $("input[name='price_final']").bind('change', calculate_fixed_price);

});