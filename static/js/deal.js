function index_price() {
    var $period = $('#id_period').val();
    var $id_index_types = $('#id_index_types').val();
    var $id_index_date = $('#id_index_date').val();
    if ($id_index_types !== 4) {
        $.ajax({
            type: 'GET',
            url: "/base/index_price/",
            data: {'period': $period, 'index_type': $id_index_types, 'index_date': $id_index_date},
            success: function (data, status) {
                $('#id_index_price').val(parseFloat(data).toFixed(2));
            },
            error: function (jqXHR, error, errorThrown) {
                $('#id_index_price').val(0);
                console.log(error);
            }
        });
    }
}

function action_click(dom) {
    var $name = dom.attr('name');
    var $id = dom.attr('id');
    var $is_popup = dom.attr('is_popup');
    var $new_tab = dom.attr('new_tab');
    var $is_view = dom.attr('is_view');
    var $confirm_message = dom.attr('confirm_message');
    var $help_message = dom.attr('help_message');
    var $target = "_self";
    var $spc_url = dom.attr('spc_url');
    var $manager_name = dom.attr('manager_name');
    var $url_address = dom.attr('url_address');
    if ($manager_name)
        url = "/" + $manager_name + "/actions/?t=action&n=" + $name + "&i=" + $id;
    else if ($url_address.indexOf("/deal/"))
        url = $url_address;
    else
        url = "/deal/" + $url_address;
    if ($is_popup == "False") {
        if ($new_tab == "True")
            $target = "_blank";
        if ($spc_url)
            url = $spc_url + $id;
        window.open(url, $target);
    } else {
        if ($is_view == "False") {
            swal({
                title: $confirm_message,
                text: $help_message,
                icon: "warning",
                buttons: ["خیر", "بله"],
            })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            type: "GET",
                            url: url,
                            cache: false,
                            success: function (data, status) {
                                var $toast = toastr[data.status](data.msg);
                                $('button[name="search_filter"]').click();
                            }
                        });
                    } else {
                        swal({
                            title: "عملیات لغو گردید.",
                            icon: "error"
                        });
                    }
                });
        } else {
            $(".modal-content").load(url, function () { // load the url into the modal
                $('#myModal').modal('show'); // display the modal on url load
            });
        }
    }
}