from django.conf.urls import url

from nikoo.apps.invoice.views import sell_invoice, add_account_number, del_account_number, download_invoice, \
    purchase_invoice, download_invoice_purchase

urlpatterns = [
    url(r'^sell_invoice/(?P<deal_id>\w+)/$', sell_invoice, name='sell_invoice'),

    url(r'^purchase_invoice/(?P<deal_id>\w+)/$', purchase_invoice, name='purchase_invoice'),
    url(r'^download_invoice_purchase/(?P<invoice_id>\w+)/$', download_invoice_purchase, name='download_invoice_purchase'),

    url(r'^download_invoice/(?P<invoice_id>\w+)/$', download_invoice, name='download_invoice'),
    url(r'^add_account_number/(?P<invoice_id>\w+)/$', add_account_number, name='add_account_number'),
    url(r'^del_account_number/(?P<invoice_id>\w+)/(?P<id>\w+)/$', del_account_number, name='del_account_number'),
]
