from collections import OrderedDict
from decimal import Decimal

from django.db import models
from django.utils.translation import gettext_lazy as _

from nikoo.apps.base_information.models import Cost
from nikoo.apps.deal.models import Deal, ProductDeal, ProductSpecDeal
from nikoo.apps.utils.calc import get_list_and_avg_index_price, get_avg_index_price


class Invoice(models.Model):
    DRAFT = 1
    FINISHED = 2
    STATUS_CHOICES = (
        (DRAFT, _("DRAFT")),
        (FINISHED, _("COMPLETED")),
    )

    PROFORMA = 1
    PROVISIONAL = 2
    NOR = 3
    FINAL = 4
    INVOICE_TYPE = (
        (PROFORMA, _("Proforma Invoice")),
        (PROVISIONAL, _("Provisional Invoice")),
        (NOR, _("NOR Invoice")),
        (FINAL, _("Final Invoice")),
    )

    deal = models.ForeignKey('deal.Deal', verbose_name=_(u"Deal"), related_name="invoice")
    invoice_type = models.IntegerField(_("Invoice type"), choices=INVOICE_TYPE, default=PROFORMA)
    period = models.IntegerField(_("Period"), choices=Deal.PERIOD_CHOICES, default=2)
    index_date = models.DateField(_("Index Date"), null=True, blank=True)
    email = models.TextField(_("Email"))
    status = models.IntegerField(_("Status"), choices=STATUS_CHOICES, default=DRAFT)
    provisional_payment = models.IntegerField(_("Provisional payment %"), null=True, blank=True)
    payable = models.DecimalField(_("Payable"), max_digits=12, decimal_places=4, default=0)

    def __str__(self):
        return self.invoice_type

    @staticmethod
    def calc_bp_normal(val1, val2, viu):
        max_value = max(val1, val2)
        min_value = Decimal(min(val1, val2))
        return (min_value - max_value) * viu

    @staticmethod
    def calc_percent_difference(val1, val2):
        max_value = max(val1, val2)
        min_value = Decimal(min(val1, val2))
        return ((max_value - min_value) / max_value) * 100

    @staticmethod
    def calc_analysis(difference, min_value, max_value, sgs, ciq):
        if 0 <= difference < min_value:
            return ciq
        elif min_value <= difference <= max_value:
            return (sgs + ciq) / 2
        elif difference > max_value:
            return ciq

    def get_invoice(self):
        period = self.period
        index_types = self.deal.index_types
        index_date = self.index_date

        output = dict()
        output['invoice_type_id'] = self.invoice_type
        output['agreement_no'] = self.deal.agreement_no
        output['date'] = index_date
        output['index_date'] = index_date

        # TODO ‫platts می تونه fix باشه
        platts = get_list_and_avg_index_price(period, index_types, index_date)
        output['platts'] = platts

        d = OrderedDict()
        pr = OrderedDict()
        if self.invoice_type in [self.PROVISIONAL, self.NOR]:
            info_type_in = [ProductDeal.CONTRACT, ProductDeal.SGS]
        else:
            info_type_in = [ProductDeal.CONTRACT, ProductDeal.SGS, ProductDeal.CIQ]
        product_spec_deal = ProductSpecDeal.objects.filter(deal=self.deal, product_deal__info_type__in=info_type_in)
        for spec in product_spec_deal:
            product_name = spec.product_deal.product.name
            spec_name = spec.spect_field.name
            info_type = spec.product_deal.get_info_type()
            if product_name not in d:
                d[product_name] = OrderedDict()

            if spec_name in d[product_name]:
                d[product_name][spec_name][info_type] = spec.quality
                if spec.product_deal.info_type == ProductDeal.CIQ:
                    if 'all' in d[product_name][spec_name]:
                        d[product_name][spec_name]['all'] += spec.quality * spec.product_deal.volume
                    else:
                        d[product_name][spec_name]['all'] = spec.quality * spec.product_deal.volume
                    if 'counter' in d[product_name][spec_name]:
                        d[product_name][spec_name]['counter'] += 1
                    else:
                        d[product_name][spec_name]['counter'] = 1
                    if 'ciq_quantity' in d[product_name][spec_name]:
                        d[product_name][spec_name]['ciq_quantity'] += spec.product_deal.volume
                    else:
                        d[product_name][spec_name]['ciq_quantity'] = spec.product_deal.volume
                d[product_name]['Total'][info_type] = 0
                if spec.product_deal.product.premium_factor and spec.spect_field.mainelement:
                    pr[product_name] = OrderedDict()
                    alfa = get_avg_index_price(period, index_types, index_date, 'primum')
                    share = spec.product_deal.premium_share
                    pr[product_name]['Contract'] = share
                    pr[product_name]['viu'] = alfa
                    # TODO
                    pr[product_name]['B/P'] = alfa * (share / 100) * spec.quality
                    if spec.product_deal.info_type == ProductDeal.CIQ:
                        pr[product_name][info_type] = 0
            else:
                d[product_name][spec_name] = OrderedDict()
                d[product_name][spec_name][info_type] = spec.quality
                d[product_name][spec_name]['step'] = spec.step
                d[product_name][spec_name]['mainelement'] = spec.spect_field.mainelement
                d[product_name][spec_name]['penalty_type'] = spec.product_deal.penalty_type
                if spec.product_deal.info_type == ProductDeal.CIQ:
                    d[product_name][spec_name]['all'] = spec.quality * spec.product_deal.volume
                    d[product_name][spec_name]['counter'] = 1
                    d[product_name][spec_name]['ciq_quantity'] = spec.product_deal.volume

                if spec.spect_field.mainelement:
                    viu = get_avg_index_price(period, index_types, index_date, 'viu')
                else:
                    viu = spec.viu
                d[product_name][spec_name]['viu'] = viu
                d[product_name]['Total'] = OrderedDict()
                d[product_name]['Total'][info_type] = 0
                d[product_name]['Total']['viu'] = 0
                # Calc PR
                if spec.product_deal.product.premium_factor and spec.spect_field.mainelement:
                    pr[product_name] = OrderedDict()
                    alfa = get_avg_index_price(period, index_types, index_date, 'primum')
                    share = spec.product_deal.premium_share
                    pr[product_name]['Contract'] = share
                    pr[product_name]['viu'] = alfa
                    # TODO
                    pr[product_name]['B/P'] = alfa * (share / 100) * spec.quality
                    if spec.product_deal.info_type == ProductDeal.CIQ:
                        pr[product_name][info_type] = 0

        # Calculate b_p and total
        for product, v in d.items():
            b_p = 0
            if product in pr:
                # TODO یه حلفه باید بخوره روش
                d[product]['PR'] = OrderedDict()
                d[product]['PR']['Contract'] = pr[product]['Contract']
                d[product]['PR']['viu'] = pr[product]['viu']
                d[product]['PR']['SGS'] = 0
                if 'CIQ-1' in pr[product]:
                    d[product]['PR']['CIQ-1'] = pr[product]['CIQ-1']
                if 'CIQ-2' in pr[product]:
                    d[product]['PR']['CIQ-2'] = pr[product]['CIQ-2']
                d[product]['PR']['B/P'] = pr[product]['B/P']
                b_p = pr[product]['B/P']
            for spec_name, fv in v.items():
                if spec_name == 'Total':
                    continue
                # calc b_p
                if spec_name != 'PR':
                    if 'all' in d[product][spec_name]:
                        key = 'avg'
                        print(product)
                        print(spec_name)
                        d[product][spec_name]['avg'] = d[product][spec_name]['all'] / d[product][spec_name][
                            'ciq_quantity']
                        if 'PR' in d[product]:
                            d[product]['PR']['avg'] = 0
                    else:
                        key = 'SGS'
                    b_p = (d[product][spec_name]['Contract'] - d[product][spec_name][key]) * d[product][spec_name][
                        'viu']
                    if b_p >= 0 or (spec_name.lower() == 'size' and b_p <= 0):
                        b_p = 0

                    if d[product][spec_name]['mainelement']:
                        if d[product][spec_name]['penalty_type'] == ProductDeal.PENALTY_STAIRS:
                            alfa = self.deal.from_step2 - self.deal.to_step2
                            beta = self.deal.from_step3 - self.deal.to_step3
                            n0 = self.deal.viu_step1
                            n1 = self.deal.viu_step2
                            n2 = self.deal.viu_step3
                            n3 = self.deal.viu_step4
                            c = d[product][spec_name]['Contract']
                            viu = d[product][spec_name]['viu']
                            difference = self.calc_percent_difference(c, d[product][spec_name][key])
                            analysis = self.calc_analysis(difference, self.deal.min_weight_diff,
                                                          self.deal.max_weight_diff, d[product][spec_name]['SGS'],
                                                          d[product][spec_name][key])
                            diffs = analysis - c
                            if diffs < 0:
                                if 0 < abs(diffs) <= alfa:
                                    b_p = abs(diffs) * n1 * viu
                                elif alfa < abs(diffs) <= alfa + beta:
                                    b_p = -n1 * viu + (analysis - (c - alfa)) * n2 * viu
                                elif alfa + beta < abs(diffs):
                                    b_p = -n1 * viu - beta * n2 * viu + (
                                                difference - (analysis - (alfa + beta)) * n3 * viu)
                            else:
                                b_p = diffs * viu

                    d[product][spec_name]['B/P'] = b_p * fv["step"]

                if 'B/P' in d[product]['Total']:
                    d[product]['Total']['B/P'] += b_p
                else:
                    d[product]['Total']['B/P'] = b_p

        ln = list()
        for product, v in d.items():
            counter = 0
            header = list()
            header.append("")
            j = OrderedDict()
            j['name'] = product
            j['total'] = 0
            rows = list()
            for spec_name, fv in v.items():
                row = list()
                if spec_name != 'Total':
                    row.append(spec_name)
                for ff in fv:  # Contract SGS CIQ ...
                    if spec_name == 'Total':
                        if ff == 'B/P':
                            j['total'] = d[product]['Total']['B/P']
                        continue
                    if ff not in ['all', 'counter', 'mainelement', 'penalty_type', 'step', 'deal_product_spec',
                                  'all_ciq', 'ciq_quantity']:
                        if counter == 0:
                            header.append(ff)
                        if isinstance(fv[ff], Decimal):
                            value = "{0:.4f}".format(fv[ff])
                        else:
                            value = fv[ff]
                        row.append(value)
                counter += 1
                rows.append(row)
            rows.insert(0, header)
            j['rows'] = rows
            ln.append(j)

        output['tables'] = ln

        total_payment = 0
        total_sgs_quantity = 0
        total_ciq_quantity = 0
        total_dry = 0
        abstract_list = list()
        for product in ProductDeal.objects.filter(deal=self.deal, info_type=ProductDeal.SGS):
            row = dict()
            row['name'] = product.product.name
            row['sgs_quantity'] = product.volume
            if self.invoice_type == self.FINAL:
                row['ciq_quantity'] = d[product.product.name][list(d[product.product.name])[0]]['ciq_quantity']
                difference = self.calc_percent_difference(row['sgs_quantity'], row['ciq_quantity'])
                row['difference'] = difference
                # TODO 0.g همون max و min هستا!!
                row['final_dry'] = self.calc_analysis(difference, self.deal.min_weight_diff,
                                                      self.deal.max_weight_diff, row['sgs_quantity'],
                                                      row['ciq_quantity'])
                total_ciq_quantity += row['ciq_quantity']
            else:
                row['final_dry'] = row['sgs_quantity']
            row['platts'] = platts[-1]['platts']
            row['adjustment'] = product.adjustment
            row['bonus_penalty'] = d[product.product.name]['Total']['B/P']
            row['final_price'] = float(row['platts']) + float(row['adjustment']) + float(row['bonus_penalty'])
            row['amount'] = float(row['final_price']) * float(row['final_dry'])
            abstract_list.append(row)
            total_payment += row['amount']
            total_sgs_quantity += row['sgs_quantity']
            total_dry += row['final_dry']
        abs_total = {
            'name': 'total',
            'sgs_quantity': total_sgs_quantity,
            'ciq_quantity': total_ciq_quantity,
            'difference': '-',
            'final_dry': total_dry,
            'platts': "-",
            'adjustment': "-",
            'bonus_penalty': "-",
            'final_price': "-",
            'amount': total_payment,
        }
        abstract_list.append(abs_total)
        output['abstracts'] = abstract_list

        # Finance
        finance = OrderedDict()
        finance['total'] = total_payment
        finance['payable'] = total_payment
        provisional_payment = None
        if self.invoice_type == self.PROVISIONAL or self.invoice_type == self.NOR:
            provisional_payment = "Provisional Payment (%s%%)" % self.provisional_payment
            finance[provisional_payment] = total_payment * (self.provisional_payment / 100)
            finance['payable'] = finance[provisional_payment]

        down_payment = self.deal.down_payment
        if down_payment > 0:
            finance['Advanced Payment'] = down_payment
            finance['payable'] -= float(down_payment)
        # COST
        for invoice_cost in self.invoice_cost.all():
            finance[invoice_cost.cost_name.name] = invoice_cost.price
            if invoice_cost.cost_name.operator == Cost.SUM:
                finance['payable'] += float(invoice_cost.price)
            elif invoice_cost.cost_name.operator == Cost.SUB:
                finance['payable'] -= float(invoice_cost.price)
        # BILL PAYMENT
        for bill in self.deal.bill_slip.all():
            finance[bill.name] = bill.amount_to_dollar
            finance['payable'] -= float(bill.amount_to_dollar)
        output['finance'] = finance
        self.payable = finance['payable']
        self.save()
        return output

    def get_invoice_purchase(self):
        period = self.period
        index_types = self.deal.index_types
        index_date = self.index_date

        output = dict()
        output['invoice_type_id'] = self.invoice_type
        output['vessel_name'] = self.deal.vessel_name
        output['agreement_no'] = self.deal.agreement_no
        output['date'] = index_date
        output['index_date'] = index_date

        # TODO ‫platts می تونه fix باشه
        platts = get_list_and_avg_index_price(period, index_types, index_date)
        output['platts'] = platts

        d = OrderedDict()
        pr = OrderedDict()

        info_type_in = [ProductDeal.CONTRACT, ProductDeal.SGS]
        product_spec_deal = ProductSpecDeal.objects.filter(deal=self.deal, product_deal__info_type__in=info_type_in)
        for spec in product_spec_deal:
            product_name = spec.product_deal.product.name
            spec_name = spec.spect_field.name
            info_type = spec.product_deal.get_info_type()
            if product_name not in d:
                d[product_name] = OrderedDict()
            if spec_name in d[product_name]:
                d[product_name][spec_name][info_type] = spec.quality
                d[product_name]['Total'][info_type] = 0
                if spec.product_deal.product.premium_factor and spec.spect_field.mainelement:
                    pr[product_name] = OrderedDict()
                    alfa = get_avg_index_price(period, index_types, index_date, 'primum')
                    share = spec.product_deal.premium_share
                    pr[product_name]['Contract'] = share
                    pr[product_name]['viu'] = alfa
                    # TODO
                    pr[product_name]['B/P'] = alfa * (share / 100) * spec.quality
            else:
                d[product_name][spec_name] = OrderedDict()
                d[product_name][spec_name][info_type] = spec.quality
                d[product_name][spec_name]['step'] = spec.step
                d[product_name][spec_name]['mainelement'] = spec.spect_field.mainelement
                d[product_name][spec_name]['penalty_type'] = spec.product_deal.penalty_type
                # if spec.spect_field.mainelement:
                #     viu = get_avg_index_price(period, index_types, index_date, 'viu')
                # else:
                viu = spec.viu
                d[product_name][spec_name]['viu'] = viu
                d[product_name]['Total'] = OrderedDict()
                d[product_name]['Total'][info_type] = 0
                d[product_name]['Total']['viu'] = 0
                # Calc PR
                if spec.product_deal.product.premium_factor and spec.spect_field.mainelement:
                    pr[product_name] = OrderedDict()
                    alfa = get_avg_index_price(period, index_types, index_date, 'primum')
                    share = spec.product_deal.premium_share
                    # pr[product_name]['Contract'] = share
                    # pr[product_name]['viu'] = alfa
                    # TODO
                    pr[product_name]['B/P'] = alfa * (share / 100) * spec.quality

        # Calculate b_p and total
        for product, v in d.items():
            b_p = 0
            if product in pr:
                # TODO یه حلفه باید بخوره روش
                d[product]['PR'] = OrderedDict()
                d[product]['PR']['Contract'] = pr[product]['Contract']
                d[product]['PR']['viu'] = pr[product]['viu']
                d[product]['PR']['SGS'] = 0
                d[product]['PR']['B/P'] = pr[product]['B/P']
                b_p = pr[product]['B/P']
            for spec_name, fv in v.items():
                if spec_name == 'Total':
                    continue
                # calc b_p
                if spec_name != 'PR':
                    key = 'SGS'
                    b_p = (d[product][spec_name]['Contract'] - d[product][spec_name][key]) * d[product][spec_name][
                        'viu']
                    if b_p >= 0 or (spec_name.lower() == 'size' and b_p <= 0):
                        b_p = 0

                    if d[product][spec_name]['mainelement']:
                        if d[product][spec_name]['penalty_type'] == ProductDeal.PENALTY_STAIRS:
                            alfa = self.deal.from_step2 - self.deal.to_step2
                            beta = self.deal.from_step3 - self.deal.to_step3
                            n0 = self.deal.viu_step1
                            n1 = self.deal.viu_step2
                            n2 = self.deal.viu_step3
                            n3 = self.deal.viu_step4
                            c = d[product][spec_name]['Contract']
                            viu = d[product][spec_name]['viu']
                            difference = self.calc_percent_difference(c, d[product][spec_name][key])
                            analysis = self.calc_analysis(difference, self.deal.min_weight_diff,
                                                          self.deal.max_weight_diff, d[product][spec_name]['SGS'],
                                                          d[product][spec_name][key])
                            diffs = analysis - c
                            if diffs < 0:
                                if 0 < abs(diffs) <= alfa:
                                    b_p = abs(diffs) * n1 * viu
                                elif alfa < abs(diffs) <= alfa + beta:
                                    b_p = -n1 * viu + (analysis - (c - alfa)) * n2 * viu
                                elif alfa + beta < abs(diffs):
                                    b_p = -n1 * viu - beta * n2 * viu + (
                                            difference - (analysis - (alfa + beta)) * n3 * viu)
                            else:
                                b_p = diffs * viu

                    d[product][spec_name]['B/P'] = b_p * fv["step"]

                if 'B/P' in d[product]['Total']:
                    d[product]['Total']['B/P'] += b_p
                else:
                    d[product]['Total']['B/P'] = b_p

        ln = list()
        for product, v in d.items():
            counter = 0
            header = list()
            header.append("")
            j = OrderedDict()
            j['name'] = product
            j['total'] = 0
            rows = list()
            for spec_name, fv in v.items():
                row = list()
                if spec_name != 'Total':
                    row.append(spec_name)
                for ff in fv:  # Contract SGS ...
                    if spec_name == 'Total':
                        if ff == 'B/P':
                            j['total'] = d[product]['Total']['B/P']
                        continue
                    if ff not in ['all', 'counter', 'mainelement', 'penalty_type', 'step', 'deal_product_spec']:
                        if counter == 0:
                            header.append(ff)
                        if isinstance(fv[ff], Decimal):
                            value = "{0:.4f}".format(fv[ff])
                        else:
                            value = fv[ff]
                        row.append(value)
                counter += 1
                rows.append(row)
            rows.insert(0, header)
            j['rows'] = rows
            ln.append(j)

        output['tables'] = ln

        total_payment = 0
        total_wmt = 0
        total_dmt = 0
        abstract_list = list()
        platts_nor = platts[-1]['platts']
        platts_100 = platts_nor - self.deal.platts
        less_20 = float(platts_100) * 0.2
        if less_20 <= 0:
            less_20 = 0

        platts_abs = OrderedDict()
        platts_abs["Contract platts"] = self.deal.platts
        platts_abs["platts"] = platts[-1]['platts']
        platts_abs["platts NOR"] = platts_nor
        platts_abs["100%"] = platts_100
        platts_abs['20% deduction'] = less_20

        costs = OrderedDict()
        costs['total'] = float(platts_nor) - less_20
        costs['payable'] = float(platts_nor) - less_20
        costs['Platts'] = platts_nor
        for cost in self.deal.cost_purchase_deal.all():
            costs[cost.cost_name.name] = cost.price
            if cost.cost_name.operator == Cost.SUM:
                costs['payable'] += float(cost.price)
            elif cost.cost_name.operator == Cost.SUB:
                costs['payable'] -= float(cost.price)
        costs['20% deduction'] = less_20
        for product in ProductDeal.objects.filter(deal=self.deal, info_type=ProductDeal.CONTRACT):
            row = dict()
            row['name'] = product.product.name
            row['wmt'] = product.wmt
            row['moisture'] = product.moisture
            row['dmt'] = product.wmt - (product.moisture * product.wmt)
            row['platts'] = costs['payable']
            row['adjustment'] = product.adjustment
            row['bonus_penalty'] = d[product.product.name]['Total']['B/P']
            row['demurrage'] = product.demurrage
            row['final_price'] = float(row['platts']) + float(row['adjustment']) + float(row['bonus_penalty']) + float(row['demurrage'])
            row['amount'] = float(row['dmt']) * float(row['final_price'])
            abstract_list.append(row)
            total_payment += row['amount']
            total_wmt += row['wmt']
            total_dmt += row['dmt']
        abs_total = {
            'name': 'total',
            'wmt': total_wmt,
            'moisture': '-',
            'dmt': total_dmt,
            'platts': "-",
            'adjustment': "-",
            'bonus_penalty': "-",
            'demurrage': "-",
            'final_price': "-",
            'amount': total_payment,
        }
        abstract_list.append(abs_total)
        output['abstracts'] = abstract_list
        output['finance'] = costs
        output['platts_abs'] = platts_abs
        self.save()
        return output

    def get_invoice_provisional_nor(self):
        period = self.period
        index_types = self.deal.index_types
        index_date = self.index_date

        output = dict()
        output['invoice_type_id'] = self.invoice_type
        output['agreement_no'] = self.deal.agreement_no
        output['date'] = index_date
        output['index_date'] = index_date

        platts = get_list_and_avg_index_price(period, index_types, index_date)
        output['platts'] = platts

        d = OrderedDict()
        pr = OrderedDict()
        for spec in ProductSpecDeal.objects.filter(deal=self.deal,
                                                   product_deal__info_type__in=[ProductDeal.CONTRACT, ProductDeal.SGS]):
            product_name = spec.product_deal.product.name
            spec_name = spec.spect_field.name
            info_type = spec.product_deal.get_info_type()
            if spec.product_deal.product.name in d:
                pass
            else:
                d[product_name] = OrderedDict()

            if spec_name in d[product_name]:
                d[product_name][spec_name][info_type] = spec.quality
                d[product_name]['Total'][info_type] = 0
                if spec.product_deal.product.premium_factor and spec.spect_field.mainelement:
                    pr[product_name] = OrderedDict()
                    alfa = get_avg_index_price(period, index_types, index_date, 'primum')
                    pr[product_name]['Contract'] = alfa
                    pr[product_name]['viu'] = spec.product_deal.premium_share
                    pr[product_name][
                        'B/P'] = alfa * (spec.product_deal.premium_share / 100) * spec.quality
            else:
                d[product_name][spec_name] = OrderedDict()
                d[product_name][spec_name][
                    spec.product_deal.get_info_type()] = spec.quality
                d[product_name][spec_name]['step'] = spec.step

                if spec.spect_field.mainelement:
                    viu = get_avg_index_price(period, index_types, index_date, 'viu')
                else:
                    viu = spec.viu
                d[product_name][spec_name]['viu'] = viu
                d[product_name]['Total'] = OrderedDict()
                d[product_name]['Total'][info_type] = 0
                d[product_name]['Total']['viu'] = 0
                if spec.product_deal.product.premium_factor and spec.spect_field.mainelement:
                    pr[product_name] = OrderedDict()
                    alfa = get_avg_index_price(period, index_types, index_date, 'primum')
                    pr[product_name]['Contract'] = alfa
                    pr[product_name]['viu'] = spec.product_deal.premium_share
                    pr[product_name]['B/P'] = alfa * (
                            spec.product_deal.premium_share / 100) * spec.quality

        for k, v in d.items():
            if k in pr:
                d[k]['PR'] = OrderedDict()
                d[k]['PR']['Contract'] = pr[k]['Contract']
                d[k]['PR']['viu'] = pr[k]['viu']
                d[k]['PR']['SGS'] = 0
                d[k]['PR']['B/P'] = pr[k]['B/P']
            for fk, fv in v.items():
                if fk == 'Total':
                    continue
                if fk != 'PR':
                    try:
                        b_p = self.calc_bp_normal(fv["Contract"], fv["SGS"], fv['viu'])
                    except:
                        b_p = 0
                    d[k][fk]['B/P'] = b_p * fv["step"]
                else:
                    b_p = pr[k]['B/P']

                if 'B/P' in d[k]['Total']:
                    d[k]['Total']['B/P'] += b_p
                else:
                    d[k]['Total']['B/P'] = b_p
        ln = list()
        for k, v in d.items():
            counter = 0
            header = list()
            header.append("")
            j = OrderedDict()
            j['name'] = k
            j['total'] = 0
            rows = list()
            for fk, fv in v.items():
                row = list()
                if fk != 'Total':
                    row.append(fk)
                for ff in fv:
                    if fk == 'Total':
                        if ff == 'B/P':
                            j['total'] = d[k]['Total']['B/P']
                        continue
                    if ff not in ['all', 'count', 'mainelement', 'penalty_type', 'step', 'deal_product_spec',
                                  'all_ciq']:
                        if counter == 0:
                            header.append(ff)
                        if isinstance(fv[ff], Decimal):
                            value = "{0:.4f}".format(fv[ff])
                        else:
                            value = fv[ff]
                        row.append(value)
                counter += 1
                rows.append(row)
            rows.insert(0, header)
            j['rows'] = rows
            ln.append(j)

        output['tables'] = ln

        total_payment = 0
        total_quantity = 0
        abstract_list = list()
        for product in ProductDeal.objects.filter(deal=self.deal, info_type=ProductDeal.SGS):
            row = dict()
            row['name'] = product.product.name
            row['final_dry'] = product.volume
            row['platts'] = platts[-1]['platts']
            row['adjustment'] = product.adjustment
            row['bonus_penalty'] = d[product.product.name]['Total']['B/P']
            row['final_price'] = float(row['platts']) + float(row['adjustment']) + float(row['bonus_penalty'])
            row['amount'] = float(row['final_price']) * float(row['final_dry'])
            abstract_list.append(row)
            total_payment += row['amount']
            total_quantity += row['final_dry']
        abs_total = {
            'name': 'total',
            'final_dry': total_quantity,
            'platts': "-",
            'adjustment': "-",
            'bonus_penalty': "-",
            'final_price': "-",
            'amount': total_payment,
        }
        abstract_list.append(abs_total)
        output['abstracts'] = abstract_list

        # Finance
        finance = OrderedDict()
        finance['total'] = total_payment
        finance['payable'] = total_payment
        provisional_payment = None
        if self.invoice_type == self.PROVISIONAL or self.invoice_type == self.NOR:
            provisional_payment = "Provisional Payment(%s %%)" % self.provisional_payment
            finance[provisional_payment] = total_payment * (self.provisional_payment / 100)
            finance['payable'] = finance[provisional_payment]

        down_payment = self.deal.down_payment
        if down_payment > 0:
            finance['Advanced Payment'] = down_payment
            finance['payable'] -= float(down_payment)
        # COST
        for invoice_cost in self.invoice_cost.all():
            finance[invoice_cost.cost_name.name] = invoice_cost.price
            if invoice_cost.cost_name.operator == Cost.SUM:
                finance['payable'] += float(invoice_cost.price)
            elif invoice_cost.cost_name.operator == Cost.SUB:
                finance['payable'] -= float(invoice_cost.price)
        # BILL PAYMENT
        for bill in self.deal.bill_slip.all():
            finance[bill.name] = bill.amount_to_dollar
            finance['payable'] -= float(bill.amount_to_dollar)
        output['finance'] = finance
        self.payable = finance['payable']
        self.save()
        return output

    def get_invoice_proforma(self):
        period = self.deal.period
        index_types = self.deal.index_types
        index_date = self.deal.index_date

        output = dict()
        output['invoice_type_id'] = self.invoice_type
        output['agreement_no'] = self.deal.agreement_no
        output['date'] = index_date
        output['index_date'] = index_date

        platts = get_list_and_avg_index_price(period, index_types, index_date)
        output['platts'] = platts

        total_payment = 0
        total_quantity = 0
        abstract_list = list()
        for product in ProductDeal.objects.filter(deal=self.deal, info_type=ProductDeal.CONTRACT):
            row = dict()
            row['name'] = product.product.name
            row['quantity'] = product.volume
            row['platts'] = platts[-1]['platts']
            row['adjustment'] = product.adjustment
            try:
                bonus_penalty = platts[-1]['pr'] * (product.premium_share / 100) * index_types.name
            except Exception as e:
                bonus_penalty = 0
            row['bonus_penalty'] = bonus_penalty
            row['final_price'] = float(row['platts']) + float(row['adjustment']) + float(row['bonus_penalty'])
            row['amount'] = float(row['final_price']) * float(row['quantity'])
            abstract_list.append(row)
            total_payment += row['amount']
            total_quantity += row['quantity']
        abs_total = {
            'name': 'total',
            'quantity': total_quantity,
            'platts': "-",
            'adjustment': "-",
            'bonus_penalty': "-",
            'final_price': "-",
            'amount': total_payment,
        }
        abstract_list.append(abs_total)
        output['abstracts'] = abstract_list

        # Finance
        finance = OrderedDict()
        finance['total'] = total_payment
        down_payment = self.deal.down_payment
        if down_payment > 0:
            finance['Payable(Advanced)'] = down_payment
            finance['payable'] = down_payment
        else:
            finance['payable'] -= float(down_payment)
        output['finance'] = finance
        self.payable = finance['payable']
        self.save()
        return output


class InvoiceCost(models.Model):
    invoice = models.ForeignKey('Invoice', verbose_name=_(u"Invoice"), related_name="invoice_cost")
    cost_name = models.ForeignKey('base_information.Cost', verbose_name=_("Cost Name"), related_name="invoice_cost")
    price = models.DecimalField(_("Price"), max_digits=12, decimal_places=4)

    def __str__(self):
        return self.cost_name


class InvoiceAccountNumber(models.Model):
    invoice = models.ForeignKey('Invoice', verbose_name=_(u"Invoice"), related_name="invoice_account_number")
    account_number = models.ForeignKey('base_information.AccountNumber', verbose_name=_("Account number"),
                                       related_name="invoice_account_number")
    price = models.DecimalField(_("Price"), max_digits=12, decimal_places=4)

    def __str__(self):
        return self.account_number
