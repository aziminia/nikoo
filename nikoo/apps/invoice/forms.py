from django import forms
from django.db.models import Sum
from django.db.models.functions import Coalesce
from django.forms import inlineformset_factory
from django.forms.utils import ErrorList

from nikoo.apps.invoice.models import Invoice, InvoiceCost, InvoiceAccountNumber
from nikoo.apps.utils.forms import BaseModelForm


class InvoiceForm(BaseModelForm):
    class Meta:
        model = Invoice
        exclude = ('deal', 'status', 'payable')
        widgets = {
            'email': forms.Textarea(attrs={'rows': 1}),
        }

    def __init__(self, *args, **kwargs):
        self.deal = kwargs.pop('deal', None)
        self.initial_data = kwargs.pop('initial_data', False)
        super(InvoiceForm, self).__init__(*args, **kwargs)
        self.fields['agreement_no'] = forms.CharField(label="Agreement Number", max_length=50, required=False)
        self.fields['project_date'] = forms.DateField(label="Project Date", required=False)
        self.fields['my_company'] = forms.CharField(label="My Company", max_length=50, required=False)
        self.fields['ship'] = forms.CharField(label="Ship", max_length=50, required=False)
        self.fields['agreement_types'] = forms.CharField(label="Agreement types", max_length=50, required=False)
        self.fields['provisional_payment'].widget.hidden = True

        if self.initial_data:
            self.fields['agreement_no'].initial = self.deal.agreement_no
            self.fields['project_date'].initial = self.deal.project_date
            self.fields['my_company'].initial = self.deal.my_company
            self.fields['ship'].initial = self.deal.ship
            self.fields['agreement_types'].initial = self.deal.get_agreement_types_display()
            self.fields['email'].initial = "test@nikoo.com"
            self.fields['index_date'].initial = self.deal.index_date

        self.make_readonly(['agreement_no', 'project_date', 'my_company', 'ship', 'agreement_types'])
        self.layout_general = ['agreement_no', 'project_date', 'my_company' 'ship', 'agreement_types', 'invoice_type',
                               'email', 'provisional_payment', 'index_date', 'period']

    def save(self, commit=True):
        instance = super(InvoiceForm, self).save(commit=False)
        invoice_type = self.cleaned_data.get('invoice_type')
        # FIXME ‫باید چک کنیم. صورتحساب های فبلیش رو صادر کرده باشه. مثلا porforma , ... در final
        instance.deal = self.deal
        instance.status = Invoice.DRAFT
        if invoice_type in [Invoice.PROFORMA, Invoice.FINAL]:
            instance.provisional_payment = None
        instance.save()
        return instance


class AddAccountNumberInvoiceForm(BaseModelForm):
    class Meta:
        model = InvoiceAccountNumber
        exclude = ('invoice',)

    def __init__(self, *args, **kwargs):
        self.invoice = kwargs.pop('invoice', None)
        self.complete = False
        super(AddAccountNumberInvoiceForm, self).__init__(*args, **kwargs)
        sum_price = InvoiceAccountNumber.objects.filter(invoice=self.invoice).aggregate(sum=Coalesce(Sum('price'), 0))
        amount = self.invoice.payable - sum_price['sum']
        if amount <= 0:
            self.complete = True
        self.fields['price'].initial = amount

    def clean(self):
        # TODO: کار نمی کنه
        cleaned_data = super(AddAccountNumberInvoiceForm, self).clean()
        account_number = self.cleaned_data.get('account_number')
        try:
            _ = InvoiceAccountNumber.objects.get(account_number=account_number, invoice=self.invoice)
            self._errors['account_number'] = ErrorList([_("Cannot insert duplicate account number.")])
        except:
            pass
        return cleaned_data

    def save(self, commit=True):
        instance = super(AddAccountNumberInvoiceForm, self).save(commit=False)
        instance.invoice = self.invoice
        instance.save()
        return instance


invoice_cost_formset = inlineformset_factory(Invoice,
                                             InvoiceCost,
                                             exclude=(),
                                             # fields=('field_type', 'active', 'order', 'required', 'label'),
                                             can_delete=True,
                                             extra=1,
                                             )


class InvoicePurchaseForm(BaseModelForm):
    class Meta:
        model = Invoice
        exclude = ('deal', 'invoice_type', 'status', 'payable', 'provisional_payment')
        widgets = {
            'email': forms.Textarea(attrs={'rows': 1}),
        }

    def __init__(self, *args, **kwargs):
        self.deal = kwargs.pop('deal', None)
        self.initial_data = kwargs.pop('initial_data', False)
        super(InvoicePurchaseForm, self).__init__(*args, **kwargs)
        self.fields['agreement_no'] = forms.CharField(label="Agreement Number", max_length=50, required=False)
        self.fields['project_date'] = forms.DateField(label="Project Date", required=False)
        self.fields['my_company'] = forms.CharField(label="My Company", max_length=50, required=False)
        self.fields['ship'] = forms.CharField(label="Ship", max_length=50, required=False)
        self.fields['agreement_types'] = forms.CharField(label="Agreement types", max_length=50, required=False)

        if self.initial_data:
            self.fields['agreement_no'].initial = self.deal.agreement_no
            self.fields['project_date'].initial = self.deal.project_date
            self.fields['my_company'].initial = self.deal.my_company
            self.fields['ship'].initial = self.deal.ship
            self.fields['agreement_types'].initial = self.deal.get_agreement_types_display()
            self.fields['email'].initial = "test@nikoo.com"
            self.fields['index_date'].initial = self.deal.index_date

        self.make_readonly(['agreement_no', 'project_date', 'my_company', 'ship', 'agreement_types'])
        self.layout_general = ['agreement_no', 'project_date', 'my_company' 'ship', 'agreement_types', 'invoice_type',
                               'email',  'index_date', 'period']

    def save(self, commit=True):
        instance = super(InvoicePurchaseForm, self).save(commit=False)
        instance.deal = self.deal
        instance.status = Invoice.DRAFT
        instance.provisional_payment = None
        instance.save()
        return instance
