import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from nikoo.apps.base_information.models import History
from nikoo.apps.deal.models import Deal
from nikoo.apps.invoice.forms import InvoiceForm, invoice_cost_formset, AddAccountNumberInvoiceForm, InvoicePurchaseForm
from nikoo.apps.invoice.models import Invoice, InvoiceAccountNumber
from nikoo.apps.utils.manager.sitemap import SiteMap


@login_required
def sell_invoice(request, deal_id):
    try:
        deal = Deal.objects.get(pk=deal_id, deal_type=Deal.SELL)
        if deal.product_deal.count() == 0:
            messages.error(request, _("No product added"))
            return HttpResponseRedirect(reverse('sell_view', kwargs={'deal_id': deal_id}))
    except:
        return HttpResponseRedirect(reverse('page_404'))

    formset = None
    if request.method == 'POST':
        form = InvoiceForm(request.POST, http_request=request, deal=deal)
        if form.is_valid():
            instance = form.save(commit=False)
            formset = invoice_cost_formset(request.POST, request.FILES, instance=instance)
            if formset.is_valid():
                instance.save()
                formset.save()
                form.save_m2m()
                invoice_type = instance.invoice_type
                if invoice_type == Invoice.PROFORMA:
                    output = instance.get_invoice_proforma()
                else:
                    output = instance.get_invoice()
                context = {'instance': instance, "output": output}
                return render(request, "invoice/invoice.html", context=context)
    else:
        form = InvoiceForm(http_request=request, deal=deal, initial_data=True)
        formset = invoice_cost_formset

    context = {
        'form': form,
        'formset': formset,
        'title_formset': "Costs",
        'title': _("Add invoice for Agreement Number: %s") % deal.agreement_no,
    }
    context['sitemap_items'] = [SiteMap(context['title'], "")]
    return render(request, "invoice/forms/invoice_sell_form.html", context=context)


@login_required
def download_invoice(request, invoice_id):
    try:
        invoice = Invoice.objects.get(pk=invoice_id)
    except:
        return HttpResponseRedirect(reverse('page_404'))
    invoice_type = invoice.invoice_type
    if invoice_type == Invoice.PROFORMA:
        output = invoice.get_invoice_proforma()
    else:
        output = invoice.get_invoice()
    context = {'instance': invoice, "output": output}
    return render(request, "invoice/preview/_preview_final_block.html", context=context)
    # html_template = render_to_string("invoice/_invoice_block.html", context)
    # pdf_file = HTML(string=html_template).write_pdf()
    # stylesheets=[CSS(settings.STATIC_ROOT + 'css/pdf.css')]
    # response = HttpResponse(pdf_file, content_type='application/pdf')
    # response['Content-Disposition'] = 'filename="invoice.pdf"'
    # return response


@login_required
def add_account_number(request, invoice_id):
    try:
        invoice = Invoice.objects.get(pk=invoice_id)
    except:
        return HttpResponseRedirect(reverse('page_404'))
    if request.method == 'POST':
        form = AddAccountNumberInvoiceForm(request.POST, http_request=request, invoice=invoice)
        if form.is_valid():
            account_number_invoice = form.save()
            History.action_log(user=request.user, action_type=History.ADD_ACCOUNT_NUMBER, deal_id=invoice.deal_id)
            context = {
                "account": account_number_invoice
            }
            content = render_to_string("invoice/_row_account_number.html", context, request)
            response = {'status': 'success', 'msg': str(_("Bill Slip was generated Successfully.")), 'content': content}
            return HttpResponse(json.dumps(response), content_type="application/json")
    else:
        form = AddAccountNumberInvoiceForm(http_request=request, invoice=invoice)
        if form.complete:
            form = None
            messages.error(request, _("Account number information is fully entered. You cannot add a account number."))
    context = {
        'form': form,
        'title': _("Add account number to invoice"),
    }
    return render(request, "invoice/forms/_modal_add_account_number.html", context=context)


def del_account_number(request, invoice_id, id):
    try:
        account_number = InvoiceAccountNumber.objects.get(pk=id, invoice_id=invoice_id)
        account_number.delete()
        response = {'status': 'error', 'msg': str(_("Account number deleted Successfully."))}
        return HttpResponse(json.dumps(response), content_type="application/json")
    except:
        response = {'status': 'error', 'msg': str(_("There was an error. Please try again."))}
        return HttpResponse(json.dumps(response), content_type="application/json")


@login_required
def purchase_invoice(request, deal_id):
    try:
        deal = Deal.objects.get(pk=deal_id, deal_type=Deal.PURCHASE)
        if deal.product_deal.count() == 0:
            messages.error(request, _("No product added"))
            return HttpResponseRedirect(reverse('purchase_view', kwargs={'deal_id': deal_id}))
    except:
        return HttpResponseRedirect(reverse('page_404'))

    if request.method == 'POST':
        form = InvoicePurchaseForm(request.POST, http_request=request, deal=deal)
        if form.is_valid():
            instance = form.save(commit=False)
            output = instance.get_invoice_purchase()
            context = {'instance': instance, "output": output}
            return render(request, "invoice/invoice_purchase.html", context=context)
    else:
        form = InvoicePurchaseForm(http_request=request, deal=deal, initial_data=True)

    context = {
        'form': form,
        'title': _("Add invoice for Agreement Number: %s") % deal.agreement_no,
    }
    context['sitemap_items'] = [SiteMap(context['title'], "")]
    return render(request, "invoice/forms/invoice_purchase_form.html", context=context)


@login_required
def download_invoice_purchase(request, invoice_id):
    try:
        invoice = Invoice.objects.get(pk=invoice_id)
    except:
        return HttpResponseRedirect(reverse('page_404'))
    output = invoice.get_invoice_purchase()
    context = {'instance': invoice, "output": output}
    return render(request, "invoice/preview/_preview_purchase_block.html", context=context)