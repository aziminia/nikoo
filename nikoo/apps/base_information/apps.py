from django.apps import AppConfig


class BaseInformationConfig(AppConfig):
    name = 'base_information'
