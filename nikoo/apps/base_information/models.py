from django.db import models
from django.utils.translation import gettext_lazy as _

from nikoo.apps.utils.file import ship_document_upload_to
from nikoo.apps.utils.models import LogicalDeletedManager


class Reference(models.Model):
    name = models.IntegerField(_("reference name %"))

    def __str__(self):
        return "%s%%" % self.name


class Document(models.Model):
    name = models.CharField(_("Document name"), max_length=100)

    def __str__(self):
        return self.name


class DocumentSpec(models.Model):
    FILE_TYPE = 1
    STRING_TYPE = 2
    NUMBER_TYPE = 3
    GREGORIAN_DATE_TYPE = 4
    JALALI_DATE_TYPE = 5
    BOOLEAN_TYPE = 6

    FIELD_TYPES = (
        # (FILE_TYPE, _("FILE")),
        (STRING_TYPE, _("String")),
        (NUMBER_TYPE, _("Number")),
        (GREGORIAN_DATE_TYPE, _("Gregorian Date")),
        (JALALI_DATE_TYPE, _("Jalali Date")),
        (BOOLEAN_TYPE, _("Boolean")),
    )

    document = models.ForeignKey('Document', verbose_name=_("Document"), related_name="document_specification")
    field_name = models.CharField(_("Field Name"), max_length=100)
    field_label = models.CharField(_("Field Label"), max_length=100)
    field_type = models.IntegerField(_("Field Type"), choices=FIELD_TYPES, default=0)
    required = models.BooleanField(_('Required'), default=False)

    def __str__(self):
        return self.field_name

    def get_field_name(self):
        return "doc_%s" % self.field_name


class Cost(models.Model):
    SUM = 1
    SUB = 2
    OPERATOR_CHOICES = (
        (SUM, _("+")),
        (SUB, _("-")),
    )
    name = models.CharField(_("Cost name"), max_length=100)
    operator = models.IntegerField(_("Operator"), choices=OPERATOR_CHOICES, default=SUM)

    def __str__(self):
        return "%s (%s)" % (self.name, self.get_operator_display())


class Currency(models.Model):
    name = models.CharField(_("Currency name"), max_length=100)
    code = models.CharField(_("Currency code"), max_length=100)
    symbol = models.CharField(_("Currency symbol"), max_length=1)
    is_based = models.BooleanField(_("Based currency"), default=False)

    def __str__(self):
        return self.name


class CurrencyRate(models.Model):
    date = models.DateField(_("Date"))
    name = models.ForeignKey('Currency', verbose_name=_(u"Currency"), related_name="rate_currency")
    rate = models.DecimalField(_("Rate (Dollar)"), max_digits=9, decimal_places=4)

    def __str__(self):
        return self.rate

    def get_name(self):
        return "%s" % self.name


class AccountNumber(models.Model):
    account_number = models.CharField(_("Account number"), max_length=100)
    account_name = models.CharField(_("Account name"), max_length=100)
    swift_code = models.CharField(_("Swift code"), max_length=100)
    bank_name = models.CharField(_("Bank name"), max_length=100)
    bank_address = models.CharField(_("Bank address"), max_length=100, null=True, blank=True)
    bank_code = models.CharField(_("Bank code"), max_length=100, null=True, blank=True)
    currency_unit = models.ForeignKey('Currency', verbose_name=_(u"Currency"), related_name="account_number_currency")
    consideration_fa = models.TextField(_("Consideration in persian"), null=True, blank=True)
    consideration_en = models.TextField(_("Consideration in english"), null=True, blank=True)

    def __str__(self):
        return self.account_number


class Price(models.Model):
    reference = models.ForeignKey('Reference', verbose_name=_("Reference name"))
    date = models.DateField(_("Date"))
    price = models.DecimalField(_("Price"), max_digits=9, decimal_places=4)
    viu = models.DecimalField(_("Viu"), max_digits=9, decimal_places=4)
    primum = models.DecimalField(_("Primum"), max_digits=9, decimal_places=4)

    def __str__(self):
        return self.date


class Product(models.Model):
    name = models.CharField(_("Product category name"), max_length=100)
    premium_factor = models.BooleanField(_('Premium Factor'), default=False)
    is_deleted = models.BooleanField(_("Deleted"), default=False)
    objects = LogicalDeletedManager()

    def delete(self, using=None, keep_parents=False):
        self.is_deleted = True
        self.save()

    def __str__(self):
        return "%s" % self.name


class ProductSpec(models.Model):
    product = models.ForeignKey('Product', verbose_name=_("Product"), related_name="product_specification")
    name = models.CharField(_("Field Name"), max_length=100)
    mainelement = models.BooleanField(_('Mainelement'), default=False)
    sell = models.BooleanField(_('Sell'), default=False)
    purchase = models.BooleanField(_('Purchase'), default=False)

    def __str__(self):
        return self.name

    def get_field_name(self):
        return self.name


class History(models.Model):
    CONTRACT_CANCELLATION = 0
    CONTRACT_CONFIRMATION = 1
    CONTRACT_COMPLETION = 2
    ADD_COST = 3
    ADD_BILL = 4
    REQ_ACCOUNT_NUMBER = 5
    ASG_ACCOUNT_NUMBER = 6
    EDIT = 7
    ADD_PRODUCT = 8
    ADD_DOCUMENT = 9
    ADD_PRICE = 10
    ADD_CONTRACT = 11
    ADD_SGS = 12
    ADD_CIQ = 13
    ADD_ACCOUNT_NUMBER = 14
    ADD_SHIP_INFO = 15
    EDIT_SHIP_INFO = 16

    ACTION_CHOICE = (
        (EDIT, _("Edit")),
        (ADD_PRODUCT, _("Add Product")),
        (ADD_DOCUMENT, _("Add Document")),
        (ADD_COST, _("Add Cost")),
        (ADD_BILL, _("Add Bill")),
        (REQ_ACCOUNT_NUMBER, _("Request for Account Number")),
        (ASG_ACCOUNT_NUMBER, _("Assign Account Number")),
        (CONTRACT_CONFIRMATION, _("Contract Confirmation")),
        (CONTRACT_COMPLETION, _("Contract Completion")),
        (CONTRACT_CANCELLATION, _("Contact Cancellation")),
        (ADD_PRICE, _("Add Price")),
        (ADD_CONTRACT, _("Add Contract")),
        (ADD_SGS, _("Add Sgs")),
        (ADD_CIQ, _("Add Ciq")),
        (ADD_ACCOUNT_NUMBER, _("Add account number")),
        (ADD_SHIP_INFO, _("Add ship info")),
        (EDIT_SHIP_INFO, _("Edit ship info")),
    )

    date = models.DateTimeField(_("Date"), auto_now_add=True)
    action = models.IntegerField(_("Action Name"), choices=ACTION_CHOICE)
    user = models.ForeignKey('accounts.User', verbose_name=_("User"), related_name="history_user")
    deal = models.ForeignKey('deal.Deal', verbose_name=_("Deal"), related_name="history_deal")
    department = models.ForeignKey('accounts.Department', verbose_name=_(u"Department"), related_name="history_dept")
    comment = models.TextField(_("Comments"), null=True, blank=True)
    prev_data = models.TextField(_("Previous Data"), null=True, blank=True)

    @staticmethod
    def action_log(user, action_type=None, comments="", prev_data="", deal_id=None):
        history = History()
        history.deal_id = deal_id
        history.action = action_type
        history.user = user
        history.comment = comments
        history.prev_data = prev_data
        history.department = user.department
        history.save()


class Ship(models.Model):
    name = models.CharField(_("Name"), max_length=100)
    owner = models.CharField(_("Owner"), max_length=100)
    capacity = models.DecimalField(_("Capacity"), max_digits=9, decimal_places=4)
    crain_number = models.IntegerField(_("Crain number"))
    loading_rate = models.IntegerField(_("Loading rate"))
    discharge_rate = models.IntegerField(_("Discharge rate"))
    draft = models.IntegerField(_("Draft"))
    crain_capacity = models.DecimalField(_("Crain capacity"), max_digits=9, decimal_places=4)
    is_deleted = models.BooleanField(_("Deleted"), default=False)
    objects = LogicalDeletedManager()

    def delete(self, using=None, keep_parents=False):
        self.is_deleted = True
        self.save()

    def __str__(self):
        return self.name


class ShipExtraDoc(models.Model):
    ship = models.ForeignKey(Ship, verbose_name=_("Ship"), related_name="ship_extra")
    name = models.CharField(_("Document name"), max_length=100)
    file = models.FileField(_("Document"), upload_to=ship_document_upload_to)
    is_deleted = models.BooleanField(_("Deleted"), default=False)
    objects = LogicalDeletedManager()

    def delete(self, using=None, keep_parents=False):
        self.is_deleted = True
        self.save()

    def __str__(self):
        return self.name
