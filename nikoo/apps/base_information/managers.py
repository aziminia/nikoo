from django.utils.translation import gettext_lazy as _

from nikoo.apps.base_information.form.filter_forms import ReferenceFilterForm, DocumentFilterForm, CostFilterForm, \
    CurrencyFilterForm, CurrencyRateFilterForm, AccountNumFilterForm, PriceFilterForm, ProductFilterForm, ShipFilterForm
from nikoo.apps.base_information.form.forms import ReferenceForm, DocumentForm, DocumentSpecFormSet, CostForm, \
    CurrencyForm, CurrencyRateForm, AccountNumForm, AddPriceForm, ProductForm, ProductSpecFormSet, ShipForm, \
    ShipExtraDocSet
from nikoo.apps.base_information.models import Reference, Document, Cost, Currency, CurrencyRate, AccountNumber, Price, \
    Product, Ship
from nikoo.apps.utils.manager.action import AddAction, EditAction, AddFormsetAction, DeleteAction, EditFormsetAction
from nikoo.apps.utils.manager.main import ObjectManager, ColumnManager


class ReferenceManager(ObjectManager):
    has_jalali_filter_form = False
    manager_name = "reference"
    manager_title = _("Reference")
    filter_form = ReferenceFilterForm
    top_actions = [AddAction(ReferenceForm)]

    actions = [EditAction(ReferenceForm)]

    def can_view(self):
        return True

    def get_all_data(self):
        return Reference.objects.all()

    def get_columns(self):
        columns = [
            ColumnManager('name', _("Reference name"), '10', False),
        ]
        return columns


class DocumentManager(ObjectManager):
    manager_name = "document"
    manager_title = _("Documents")
    filter_form = DocumentFilterForm
    order_field = '-id'
    top_actions = [AddFormsetAction(DocumentForm, DocumentSpecFormSet)]

    actions = []

    def can_view(self):
        return True

    def get_all_data(self):
        return Document.objects.all()

    def get_columns(self):
        columns = [
            ColumnManager('name', _("Document name"), '10', False),
        ]
        return columns


class CostManager(ObjectManager):
    manager_name = "cost"
    manager_title = _("Costs")
    filter_form = CostFilterForm
    order_field = '-id'
    top_actions = [AddAction(CostForm)]

    actions = [EditAction(CostForm)]

    def can_view(self):
        return True

    def get_all_data(self):
        return Cost.objects.all()

    def get_columns(self):
        columns = [
            ColumnManager('name', _("Cost name (Operator)"), '10', True),
        ]
        return columns

    def get_name(self, value):
        return "%s (%s)" % (value.name, value.get_operator_display())


class CurrencyDefManager(ObjectManager):
    manager_name = "currency"
    manager_title = _("Currency")
    filter_form = CurrencyFilterForm
    order_field = '-id'
    top_actions = [AddAction(CurrencyForm)]

    actions = [EditAction(CurrencyForm)]

    def can_view(self):
        return True

    def get_all_data(self):
        return Currency.objects.all()

    def get_columns(self):
        columns = [
            ColumnManager('name', _("Currency name"), '10', False),
            ColumnManager('code', _("Code"), '10', False),
        ]
        return columns


class CurrencyRateManager(ObjectManager):
    manager_name = "currency_rate"
    manager_title = _("Currency Rate")
    filter_form = CurrencyRateFilterForm
    order_field = '-id'
    top_actions = [AddAction(CurrencyRateForm)]

    actions = [EditAction(CurrencyRateForm)]

    def can_view(self):
        return True

    def get_all_data(self):
        return CurrencyRate.objects.all()

    def get_columns(self):
        columns = [
            ColumnManager('date', _("Date"), '10', False),
            ColumnManager('name', _("Currency name"), '10', True),
            ColumnManager('rate', _("Currency Rate"), '10', False),
        ]
        return columns

    def get_name(self, value):
        return str(value.name)


class AccountNumberManager(ObjectManager):
    manager_name = "account_number"
    manager_title = _("Account Number")
    filter_form = AccountNumFilterForm
    order_field = '-id'
    top_actions = [AddAction(AccountNumForm)]

    actions = [EditAction(AccountNumForm)]

    def can_view(self):
        return True

    def get_all_data(self):
        return AccountNumber.objects.all()

    def get_columns(self):
        columns = [
            ColumnManager('account_number', _("Account Number"), '10', False),
            ColumnManager('account_name', _("Account Name"), '10', False),
            ColumnManager('swift_code', _("Swift Code"), '10', False),
            ColumnManager('bank_name', _("Bank Name"), '10', False),
            ColumnManager('currency_unit', _("Currency"), '10', True),
        ]
        return columns

    def get_currency_unit(self, value):
        return str(value.currency_unit)


class PriceManager(ObjectManager):
    has_jalali_filter_form = False
    manager_name = "price"
    manager_title = _("Prices")
    filter_form = PriceFilterForm
    order_field = '-date'
    filter_handlers = (
        ('from_date', 'pdate', 'date'),
        ('to_date', 'pdate', 'date'),
        ('reference', '', 'reference'),
    )
    top_actions = [AddAction(AddPriceForm)]

    actions = [EditAction(AddPriceForm), DeleteAction()]

    def can_view(self):
        return True

    def get_all_data(self):
        return Price.objects.all()

    def get_columns(self):
        columns = [
            ColumnManager('reference', _("Reference"), '10', True),
            ColumnManager('date', _("Date"), '10', False),
            ColumnManager('price', _("Price"), '10', True),
            ColumnManager('viu', _("viu"), '10', True),
            ColumnManager('primum', _("Primum"), '10', True),
        ]
        return columns

    def get_reference(self, value):
        return str(value.reference.name)

    def get_price(self, value):
        return str(value.price)

    def get_viu(self, value):
        return str(value.viu)

    def get_primum(self, value):
        return str(value.primum)


class ProductManager(ObjectManager):
    manager_name = "product"
    manager_title = _("Products")
    filter_form = ProductFilterForm
    order_field = '-id'
    top_actions = [AddFormsetAction(ProductForm, ProductSpecFormSet)]

    actions = [EditFormsetAction(ProductForm, ProductSpecFormSet), DeleteAction()]

    def can_view(self):
        return True

    def get_all_data(self):
        return Product.objects.all()

    def get_columns(self):
        columns = [
            ColumnManager('name', _("Product category name"), '10', False),
            # ColumnManager('fe', _("Fe"), '10', True),
        ]
        return columns

    # def get_fe(self, value):
    #     return str(value.fe)


class ShipManager(ObjectManager):
    manager_name = "ship"
    manager_title = _("Ship")
    filter_form = ShipFilterForm
    order_field = '-id'
    top_actions = [AddFormsetAction(ShipForm, ShipExtraDocSet)]
    actions = [EditAction(ShipForm), DeleteAction()]

    def can_view(self):
        return True

    def get_all_data(self):
        return Ship.objects.all()

    def get_columns(self):
        columns = [
            ColumnManager('name', _("Name"), '10', False),
            ColumnManager('owner', _("Owner"), '10', False),
            ColumnManager('capacity', _("Capacity"), '10', False),
            ColumnManager('crain_capacity', _("Crain Capacity"), '10', False),
        ]
        return columns
