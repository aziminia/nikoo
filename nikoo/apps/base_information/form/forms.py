import datetime

from django import forms
from django.forms import inlineformset_factory

from nikoo.apps.base_information.models import Reference, Document, DocumentSpec, Cost, Currency, CurrencyRate, \
    AccountNumber, Price, Product, ProductSpec, Ship, ShipExtraDoc
from nikoo.apps.utils.forms import BaseModelForm


class ReferenceForm(BaseModelForm):
    jalali_date_field = False

    class Meta:
        model = Reference
        exclude = ()

    def __init__(self, *args, **kwargs):
        super(ReferenceForm, self).__init__(*args, **kwargs)


class DocumentForm(BaseModelForm):
    class Meta:
        model = Document
        exclude = ()

    def __init__(self, *args, **kwargs):
        super(DocumentForm, self).__init__(*args, **kwargs)


class DocumentSpecForm(BaseModelForm):
    use_select2 = False

    class Meta:
        model = DocumentSpec
        exclude = ()

    def __init__(self, *args, **kwargs):
        super(DocumentSpecForm, self).__init__(*args, **kwargs)
        self.use_select2 = False
        self.use_multi_select2 = False

    def save(self, commit=True):
        instance = super(DocumentSpecForm, self).save(commit=False)
        DocumentSpec.objects.create(document=self.instance.document, field_name='document_number',
                                    field_label='Document Number', field_type=DocumentSpec.STRING_TYPE, required=False)
        DocumentSpec.objects.create(document=self.instance.document, field_name='file', field_label='file',
                                    field_type=DocumentSpec.FILE_TYPE, required=False)
        instance.save()
        return instance


class CostForm(BaseModelForm):
    class Meta:
        model = Cost
        fields = ("name", "operator")

    def __init__(self, *args, **kwargs):
        super(CostForm, self).__init__(*args, **kwargs)


class CurrencyForm(BaseModelForm):
    class Meta:
        model = Currency
        fields = ("name", "code", "symbol", "is_based")

    def __init__(self, *args, **kwargs):
        super(CurrencyForm, self).__init__(*args, **kwargs)


class CurrencyRateForm(BaseModelForm):
    class Meta:
        model = CurrencyRate
        fields = ('date', 'name', 'rate')

    def __init__(self, *args, **kwargs):
        super(CurrencyRateForm, self).__init__(*args, **kwargs)
        self.fields['date'].initinal = datetime.datetime.today().date()


class AccountNumForm(BaseModelForm):
    class Meta:
        model = AccountNumber
        exclude = ()
        widgets = {
            'consideration_fa': forms.Textarea(attrs={'rows': 3}),
            'consideration_en': forms.Textarea(attrs={'rows': 3}),
        }

    def __init__(self, *args, **kwargs):
        super(AccountNumForm, self).__init__(*args, **kwargs)


class AddPriceForm(BaseModelForm):
    jalali_date_field = False

    class Meta:
        model = Price
        exclude = ()

    def __init__(self, *args, **kwargs):
        super(AddPriceForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(AddPriceForm, self).clean()
        date = cleaned_data.get('date')
        reference_id = cleaned_data.get('reference')
        if date and reference_id:
            try:
                _ = Price.objects.get(date=date, reference_id=reference_id)
                self._errors['date'] = self.error_class([_("For this date and reference is already submitted.")])
            except:
                pass
        return cleaned_data


class ProductForm(BaseModelForm):
    class Meta:
        model = Product
        exclude = ('is_deleted',)

    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)


class ProductSpecForm(BaseModelForm):
    class Meta:
        model = ProductSpec
        exclude = ()

    def __init__(self, *args, **kwargs):
        super(ProductSpecForm, self).__init__(*args, **kwargs)


class ShipForm(BaseModelForm):
    class Meta:
        model = Ship
        exclude = ("is_deleted",)

    def __init__(self, *args, **kwargs):
        super(ShipForm, self).__init__(*args, **kwargs)


ShipExtraDocSet = inlineformset_factory(Ship, ShipExtraDoc, can_delete=True, exclude=(), extra=1, )
DocumentSpecFormSet = inlineformset_factory(Document, DocumentSpec, form=DocumentSpecForm, can_delete=True, extra=1, )
ProductSpecFormSet = inlineformset_factory(Product, ProductSpec, form=ProductSpecForm, can_delete=True, extra=1, )
