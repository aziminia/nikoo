from django import forms
from django.utils.translation import gettext_lazy as _

from nikoo.apps.base_information.models import Reference, Document, Cost, Currency, CurrencyRate, AccountNumber, Price, \
    Product, Ship
from nikoo.apps.utils.forms import BaseModelForm


class ReferenceFilterForm(BaseModelForm):
    class Meta:
        model = Reference
        fields = ('name',)

    def __init__(self, *args, **kwargs):
        super(ReferenceFilterForm, self).__init__(*args, **kwargs)
        self.disable_required('name')


class DocumentFilterForm(BaseModelForm):
    class Meta:
        model = Document
        fields = ('name',)

    def __init__(self, *args, **kwargs):
        super(DocumentFilterForm, self).__init__(*args, **kwargs)
        self.disable_required('name')


class CostFilterForm(BaseModelForm):
    class Meta:
        model = Cost
        fields = ('name',)

    def __init__(self, *args, **kwargs):
        super(CostFilterForm, self).__init__(*args, **kwargs)
        self.disable_required('name')


class CurrencyFilterForm(BaseModelForm):
    class Meta:
        model = Currency
        fields = ('name', 'code')

    def __init__(self, *args, **kwargs):
        super(CurrencyFilterForm, self).__init__(*args, **kwargs)
        self.disable_required('name', 'code')


class CurrencyRateFilterForm(BaseModelForm):
    class Meta:
        model = CurrencyRate
        fields = ('name',)

    def __init__(self, *args, **kwargs):
        super(CurrencyRateFilterForm, self).__init__(*args, **kwargs)
        self.disable_required('name')


class AccountNumFilterForm(BaseModelForm):
    class Meta:
        model = AccountNumber
        fields = ('account_number', 'account_name', 'bank_name')

    def __init__(self, *args, **kwargs):
        super(AccountNumFilterForm, self).__init__(*args, **kwargs)
        self.disable_required('account_number', 'account_name', 'bank_name')


class PriceFilterForm(BaseModelForm):
    jalali_date_field = False
    from_date = forms.DateField(label=_("From date"), required=False)
    to_date = forms.DateField(label=_("To date"), required=False)

    class Meta:
        model = Price
        fields = ('reference',)

    def __init__(self, *args, **kwargs):
        super(PriceFilterForm, self).__init__(*args, **kwargs)


class ProductFilterForm(BaseModelForm):
    class Meta:
        model = Product
        fields = ('name',)

    def __init__(self, *args, **kwargs):
        super(ProductFilterForm, self).__init__(*args, **kwargs)
        self.disable_required('name')


class ShipFilterForm(BaseModelForm):
    class Meta:
        model = Ship
        fields = ('name', 'owner',)

    def __init__(self, *args, **kwargs):
        super(ShipFilterForm, self).__init__(*args, **kwargs)
        self.disable_required('name', 'owner', )
