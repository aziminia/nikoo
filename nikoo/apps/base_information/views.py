from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render

from nikoo.apps.utils.calc import get_avg_index_price


@login_required
def get_index_price(request):
    try:
        period = int(request.GET.get('period'))
        index_type = int(request.GET.get('index_type'))
        index_date = request.GET.get('index_date')
    except:
        return HttpResponse(0)
    return HttpResponse(get_avg_index_price(period, index_type, index_date, "price"))
