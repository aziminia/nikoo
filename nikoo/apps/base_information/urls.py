from django.conf.urls import url

from nikoo.apps.base_information.views import get_index_price

urlpatterns = [
    url(r'^index_price', get_index_price, name='index_price'),
]
