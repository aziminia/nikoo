from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, UserManager
from django.db import models
from django.utils.translation import gettext_lazy as _

from nikoo.apps.accounts.menu_list import MENUS
from nikoo.apps.utils.file import upload_profile_to
from nikoo.apps.utils.multi_select_field import CustomMultiSelectField


class User(AbstractBaseUser, PermissionsMixin):
    MONTHLY = 1
    PROJECT_BASED = 2

    SALARY_TYPES_CHOICE = (
        (MONTHLY, _("MONTHLY")),
        (PROJECT_BASED, _("PROJECT_BASED")),
    )
    username = models.CharField(_("Username"), max_length=30, unique=True)
    first_name = models.CharField(_("First name"), max_length=50, null=True, blank=True)
    last_name = models.CharField(_("Last name"), max_length=50, null=True, blank=True)
    mobile = models.CharField(_("Mobile"), max_length=20, null=True, blank=True)
    phone_number = models.CharField(_("Phone number"), max_length=20, null=True, blank=True)
    address = models.TextField(_("Address"), null=True, blank=True)
    picture = models.ImageField(_("Personal picture"), upload_to=upload_profile_to, null=True, blank=True)
    email = models.EmailField(_("Email"))
    salary = models.DecimalField(_("Salary"), max_digits=6, decimal_places=2, null=True, blank=True)
    salary_type = models.IntegerField(_("Salary type"), choices=SALARY_TYPES_CHOICE, default=MONTHLY)
    role = models.ForeignKey('Role', null=True, blank=True, verbose_name=_("Role"))
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    date_joined = models.DateTimeField(_("Date joined"), auto_now_add=True)
    deactivation_date = models.DateField(_("Deactivation date"), null=True, blank=True)
    department = models.ForeignKey('Department', null=True, blank=True, verbose_name=_("Department"),
                                   related_name="user")

    USERNAME_FIELD = 'username'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = ['email']

    objects = UserManager()

    class Meta:
        verbose_name = _("User")
        verbose_name_plural = _("Users")

    def __str__(self):
        return "%s %s (%s)" % (self.first_name, self.last_name, self.username)

    @property
    def full_name(self):
        return "%s %s" % (self.first_name, self.last_name)


class Role(models.Model):
    NO = 0
    YES = 1
    BOOLEAN_CHOICE = (
        (NO, _("No")),
        (YES, _("Yes")),
    )

    SIGN = 3
    VIEW_CHOICE = (
        (NO, _("No")),
        (YES, _("Yes")),
        (SIGN, _("Sign")),

    )
    name = models.CharField(_("Role name"), max_length=100, unique=True)
    excluded_menus = CustomMultiSelectField(_(u"Excluded Menus"), max_length=500, choices=MENUS, null=True, blank=True)
    available_menus = CustomMultiSelectField(_(u"Available Menu"), max_length=500, choices=MENUS, null=True, blank=True)
    create = models.IntegerField(_(u"Contract Creation"), choices=BOOLEAN_CHOICE, default=NO)
    view = models.IntegerField(_(u"Contract View"), choices=VIEW_CHOICE, default=NO)
    edit = models.IntegerField(_(u"Contract Edition"), choices=BOOLEAN_CHOICE, default=NO)
    cancel = models.IntegerField(_(u"Contract cancellation"), choices=BOOLEAN_CHOICE, default=NO)
    confirm = models.IntegerField(_(u"Contract Confirmation"), choices=BOOLEAN_CHOICE, default=NO)
    complete = models.IntegerField(_(u"Contract Completion"), choices=BOOLEAN_CHOICE, default=NO)
    invoice = models.IntegerField(_(u"Invoice Issuance"), choices=BOOLEAN_CHOICE, default=NO)
    bill = models.IntegerField(_(u"Bill Adding"), choices=BOOLEAN_CHOICE, default=NO)
    req_account_number = models.IntegerField(_(u"Request for Account Number"), choices=BOOLEAN_CHOICE, default=NO)
    assign_account_number = models.IntegerField(_(u"Assign Account Number"), choices=BOOLEAN_CHOICE, default=NO)
    document = models.IntegerField(_(u"Document Adding"), choices=BOOLEAN_CHOICE, default=NO)
    department = models.ForeignKey("Department", verbose_name=_("Department"), related_name="role")

    def __str__(self):
        return self.name

    @staticmethod
    def get_role_permission(role):
        acl_inst = None
        try:
            acl_inst = Role.objects.get(name=role)
        except Role.DoesNotExist:
            acl_inst = Role(name=role)
            acl_default = acl_inst.get_default_action_permission()
            for key, val in acl_default.iteritems():
                setattr(acl_inst, key, val)
            acl_inst.save()
        except Role.MultipleObjectsReturned:
            acl_inst = Role.objects.filter(name=role).latest('id')
        except Exception:
            pass
        return acl_inst

    @staticmethod
    def get_menu_full_access():
        access = []
        for opt in MENUS:
            access.append(opt[0])
        return access

    @staticmethod
    def get_menu_full_string():
        access_str = u""
        for opt in MENUS:
            access_str += _(u"%s,") % opt[0]
        if len(access_str) > 0:
            access_str = access_str[:-1]
        return access_str


class Department(models.Model):
    NO = 1
    OPTIONAL = 2
    MANDATORY = 3
    STARTER = 4
    FINAL_SIGNER = 5

    # starter and final_starter contains mandatory option

    ASSOCIATION_CHOICE = (
        (NO, _("No")),
        (OPTIONAL, _("Optional")),
        (MANDATORY, _("Mandatory")),
        (STARTER, _("Starter")),
        (FINAL_SIGNER, _("Final Signer")),
    )
    SELL = 0
    PURCHASE = 1
    ALL = 2
    CONTRACT_CHOICE = (
        (SELL, _("Sell")),
        (PURCHASE, _("Purchase")),
        (ALL, _("All")),

    )
    name = models.CharField(_("Department Name"), max_length=100)
    confirm = models.IntegerField(_("Confirmation"), choices=ASSOCIATION_CHOICE, default=NO)
    complete = models.IntegerField(_("Completion"), choices=ASSOCIATION_CHOICE, default=NO)
    cancel = models.IntegerField(_("Cancellation"), choices=ASSOCIATION_CHOICE, default=NO)
    contract = models.IntegerField(_("Contract Type"), choices=CONTRACT_CHOICE, default=NO)

    def __str__(self):
        return self.name


class UserPermission(models.Model):
    NO = 0
    YES = 1
    SIGN = 3
    ROLE_BASED = 100
    BOOLEAN_CHOICE = (
        (NO, _("No")),
        (YES, _("Yes")),
        (ROLE_BASED, _("Role Based")),

    )
    VIEW_CHOICE = (
        (NO, _("No")),
        (YES, _("Yes")),
        (SIGN, _("Sign")),
        (ROLE_BASED, _("Role Based")),
    )
    excluded_menus = CustomMultiSelectField(_(u"Excluded Menus"), max_length=500, choices=MENUS, null=True, blank=True)
    available_menus = CustomMultiSelectField(_(u"Available Menu"), max_length=500, choices=MENUS, null=True, blank=True)
    user = models.OneToOneField(User, verbose_name=_("User"), related_name="user_permission")
    create = models.IntegerField(_(u"Contract Creation"), choices=BOOLEAN_CHOICE, default=NO)
    view = models.IntegerField(_(u"Contract View"), choices=VIEW_CHOICE, default=NO)
    edit = models.IntegerField(_(u"Contract Edition"), choices=BOOLEAN_CHOICE, default=NO)
    cancel = models.IntegerField(_(u"Contract cancellation"), choices=BOOLEAN_CHOICE, default=NO)
    confirm = models.IntegerField(_(u"Contract Confirmation"), choices=BOOLEAN_CHOICE, default=NO)
    complete = models.IntegerField(_(u"Contract Completion"), choices=BOOLEAN_CHOICE, default=NO)
    invoice = models.IntegerField(_(u"Invoice Issuance"), choices=BOOLEAN_CHOICE, default=NO)
    bill = models.IntegerField(_(u"Bill Adding"), choices=BOOLEAN_CHOICE, default=NO)
    req_account_number = models.IntegerField(_(u"Request for Account Number"), choices=BOOLEAN_CHOICE, default=NO)
    assign_account_number = models.IntegerField(_(u"Assign Account Number"), choices=BOOLEAN_CHOICE, default=NO)
    document = models.IntegerField(_(u"Document Adding"), choices=BOOLEAN_CHOICE, default=NO)

    @staticmethod
    def get_user_permission_or_create(user):
        try:
            custom_permission = user.user_permission
            return custom_permission
        except Exception:
            custom_permission = UserPermission(user=user)
            custom_permission.save()
            return custom_permission

    def reset_custom_permission(self):
        self.excluded_menus = None
        self.available_menus = None
        self.create = UserPermission.ROLE_BASED
        self.view = UserPermission.ROLE_BASED
        self.edit = UserPermission.ROLE_BASED
        self.cancel = UserPermission.ROLE_BASED
        self.confirm = UserPermission.ROLE_BASED
        self.complete = UserPermission.ROLE_BASED
        self.invoice = UserPermission.ROLE_BASED
        self.bill = UserPermission.ROLE_BASED
        self.req_account_number = UserPermission.ROLE_BASED
        self.assign_account_number = UserPermission.ROLE_BASED
        self.document = UserPermission.ROLE_BASED
        return self.save()
