# -*- coding: utf-8 -*-
__author__ = 'A.Ordi'

from django.utils.translation import ugettext_lazy as _

MY_COMPANY_MNG = "1"
USER_MNG = "2"
ROLE_MNG = "3"
MINE_MNG = "4"
PRODUCT_MNG = "5"
REFERENCE_MNG = "6"
PRICE_MNG = "7"
DEPO_MNG = "8"
SHIPPING_COMPANY_MNG = "9"
CUSTOMER_MNG = "10"
SHIP_MNG = "11"

PURCHASE_WORKING_MNG = "12"
PURCHASE_DONE_MNG = "13"
TRUCK_LOADING_MNG = "14"
TRUCK_DISCHARGE_MNG = "15"

SELL_WORKING_MNG = "16"
SELL_DONE_MNG = "17"

DOCUMENT_MNG = "18"
COST_MNG = "19"

CURRENCY_DEFINITION_MNG = "20"
CURRENCY_RATE_MNG = "21"

DEPARTMENT_MNG = "22"
ACCOUNT_NUMBER_MNG = "23"
REQ_ACCOUNT_NUMBER_MNG = "24"

HISTORY_REQ_ACCOUNT_MNG = "25"

REPORT = "26"

MENUS = (
    (MY_COMPANY_MNG, _("My Company Manager")),
    (USER_MNG, _("User Manager")),
    (ROLE_MNG, _("Role Manager")),
    (MINE_MNG, _("Mine Manager")),
    (PRODUCT_MNG, _("Product Manager")),
    (REFERENCE_MNG, _("Reference Manager")),
    (PRICE_MNG, _("Price Manager")),
    (DEPO_MNG, _("Depo Manager")),
    (SHIPPING_COMPANY_MNG, _("Shipping Company Manager")),
    (CUSTOMER_MNG, _("Customer Manager")),
    (SHIP_MNG, _("Ship Manager")),
    (PURCHASE_WORKING_MNG, _("In Progress Purchases Manager")),
    (PURCHASE_DONE_MNG, _("Done Purchases Manager")),
    (TRUCK_LOADING_MNG, _("Truck Loading Manager")),
    (TRUCK_DISCHARGE_MNG, _("Truck Discharge Manager")),
    (SELL_WORKING_MNG, _("In Progress Sell Manager")),
    (SELL_DONE_MNG, _("Done Sell Manager")),
    (DOCUMENT_MNG, _("Document Manager")),
    (COST_MNG, _("Cost Manager")),
    (CURRENCY_DEFINITION_MNG, _("Currency Definition Manager")),
    (CURRENCY_RATE_MNG, _("Currency Rate Manager")),
    (DEPARTMENT_MNG, _("Department Manager")),
    (ACCOUNT_NUMBER_MNG, _("Account Number Manager")),
    (REQ_ACCOUNT_NUMBER_MNG, _("Req Account Number Manager")),
    (HISTORY_REQ_ACCOUNT_MNG, _("History of req Account Number Manager")),
    (REPORT, _("Report")),

)

CONFIGURATION_MENUS_DICT = dict(MENUS)
