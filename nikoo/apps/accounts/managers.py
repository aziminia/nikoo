from django.utils.translation import gettext_lazy as _

from nikoo.apps.accounts.form.filter_forms import UserFilterForm, RoleFilterForm, DeptFilterForm
from nikoo.apps.accounts.form.forms import UserForm, RoleForm, DepartmentForm
from nikoo.apps.accounts.models import User, Role, Department
from nikoo.apps.utils.manager.action import AddAction, DeleteAction, EditAction
from nikoo.apps.utils.manager.main import ObjectManager, ColumnManager


class UserManager(ObjectManager):
    manager_name = "user"
    manager_title = _("User manager")
    filter_form = UserFilterForm
    order_field = '-id'
    top_actions = [AddAction(UserForm)]

    actions = [EditAction(UserForm), DeleteAction()]

    def can_view(self):
        return True

    def get_all_data(self):
        return User.objects.filter(is_superuser=True)

    def get_columns(self):
        columns = [
            ColumnManager('username', _("Username"), '10', False),
            ColumnManager('first_name', _("First name"), '10', False),
            ColumnManager('last_name', _("last_name"), '10', False),
            ColumnManager('email', _("email"), '10', False),
        ]
        return columns


class RoleManager(ObjectManager):
    manager_name = "role"
    manager_title = _("Role manager")
    filter_form = RoleFilterForm
    order_field = '-id'
    top_actions = [AddAction(RoleForm)]

    actions = [EditAction(RoleForm), DeleteAction()]

    def can_view(self):
        return True

    def get_all_data(self):
        return Role.objects.all()

    def get_columns(self):
        columns = [
            ColumnManager('name', _("name"), '10', False),
        ]
        return columns


class DepartmentManager(ObjectManager):
    manager_name = "department"
    manager_title = _("Department Manager")
    filter_form = DeptFilterForm
    order_field = '-id'
    top_actions = [AddAction(DepartmentForm)]

    actions = [EditAction(DepartmentForm)]

    def can_view(self):
        return True

    def get_all_data(self):
        return Department.objects.all()

    def get_columns(self):
        columns = [
            ColumnManager('name', _("Department name"), '10', True),
            ColumnManager('contract', _("Deal Type"), '10', False),
            ColumnManager('confirm', _("Confirmation association"), '10', False),
            ColumnManager('complete', _("Completion association"), '10', False),
            ColumnManager('cancel', _("Cancellation association"), '10', False),
        ]
        return columns

    def get_name(self, value):
        return str(value.name)
