from django.conf.urls import url

from nikoo.apps.accounts import views

urlpatterns = [
    url(r'^login/$', views.user_login, name='login'),
    url(r'^logout/$', views.user_logout, name='logout'),
    url(r'^edit_profile/$', views.edit_profile, name='edit_profile'),
]
