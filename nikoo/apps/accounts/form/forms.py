import re
from collections import OrderedDict

from django import forms
from django.contrib.auth import authenticate
from django.utils.translation import gettext_lazy as _

from nikoo.apps.accounts.models import User, Role, Department
from nikoo.apps.utils import has_number, has_alphabetic_chars
from nikoo.apps.utils.forms import BaseModelForm


class UserForm(BaseModelForm):
    class Meta:
        model = User
        fields = (
            'username', 'first_name', 'last_name', 'mobile', 'phone_number', 'address', 'picture', 'email', 'salary',
            'salary_type', 'role', 'department')
        widgets = {
            'address': forms.Textarea(attrs={'rows': 3}),
        }

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        instance = super(UserForm, self).save(commit=False)
        instance.is_active = True
        instance.save()
        return instance


class RoleForm(BaseModelForm):
    class Meta:
        model = Role
        exclude = ()

    def __init__(self, *args, **kwargs):
        super(RoleForm, self).__init__(*args, **kwargs)


class DepartmentForm(BaseModelForm):
    class Meta:
        model = Department
        exclude = ()


class LoginForm(forms.Form):
    username = forms.CharField(label=_("Username"), required=True,
                               widget=forms.TextInput({'placeholder': _("Username")}))
    password = forms.CharField(label=_("Password"), required=True,
                               widget=forms.PasswordInput({'placeholder': _("Password")}))

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)


class EditProfileForm(BaseModelForm):
    old_password = forms.CharField(label=_("Old password"), widget=forms.PasswordInput(), required=False)
    password = forms.CharField(label=_("New password"), widget=forms.PasswordInput(), required=False, help_text=_(
        "The password should be a combination of numbers and letters, and at least 8 characters long."))
    re_password = forms.CharField(label=_("Repeat password"), widget=forms.PasswordInput(), required=False)
    change_password = forms.BooleanField(label=_("Edit password"), initial=False, required=False)
    username = forms.CharField(label=_("Username"), max_length=30, required=False)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'mobile', 'phone_number', 'email', 'picture')

    def __init__(self, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.field_order = ['username', 'first_name', 'last_name', 'phone_number', 'mobile', 'email', 'picture',
                            'change_password', 'old_password', 'password', 're_password']
        self.fields = OrderedDict((k, self.fields[k]) for k in self.field_order)

        self.fields['username'].initial = self.instance.username
        self.fields['first_name'].label = _("First name")
        self.fields['last_name'].label = _("Last name")
        self.fields['old_password'].widget.hidden = True
        self.fields['password'].widget.hidden = True
        self.fields['re_password'].widget.hidden = True
        self.make_readonly('username')

    def clean_password(self):
        password = self.cleaned_data.get('password')
        if password:
            error_messages = []
            if len(password) < 8:
                error_messages.append(_("Password length must be at least 8 characters."))
            if not (has_number(password) and has_alphabetic_chars(password)):
                error_messages.append(_("The password should be a combination of letters and numbers"))
            if re.search(r"[\u0600-\u06FF]", password):
                error_messages.append(_("Password can not contain Persian letters are."))

            if len(error_messages) > 0:
                self._errors['password'] = self.error_class(error_messages)
        return password

    def clean(self):
        cleaned_data = super(EditProfileForm, self).clean()
        self.clean_numeral_fields(['mobile', 'phone_number'], cleaned_data)
        change_password = cleaned_data.get('change_password')
        old_password = cleaned_data.get('old_password')
        password = cleaned_data.get('password')
        re_password = cleaned_data.get('re_password')

        if change_password:
            if not password:
                self._errors['password'] = self.error_class([_("To change the password, this field is required")])
            if not re_password:
                self._errors['re_password'] = self.error_class([_("To change the password, this field is required")])
            if not old_password:
                self._errors['old_password'] = self.error_class([_("To change the password, this field is required")])
        if password and re_password and old_password:
            if password != re_password:
                self._errors['re_password'] = self.error_class([_("Password and it does not match.")])
            username = self.http_request.user.username
            user = authenticate(username=username, password=old_password)
            if not user:
                self._errors['old_password'] = self.error_class([_("The previous password is not correct.")])
        return cleaned_data

    def save(self, commit=True):
        user = super(EditProfileForm, self).save(commit=False)
        change_pass = self.cleaned_data.get('change_password')
        password = self.cleaned_data.get('password')
        if change_pass is True or change_pass == 'True':
            user.set_password(password)
        user.save()
        return user
