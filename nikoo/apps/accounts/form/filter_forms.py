from nikoo.apps.accounts.models import User, Role, Department
from nikoo.apps.utils.forms import BaseModelForm


class UserFilterForm(BaseModelForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')

    def __init__(self, *args, **kwargs):
        super(UserFilterForm, self).__init__(*args, **kwargs)
        self.disable_required('username', 'first_name', 'last_name', 'email')


class RoleFilterForm(BaseModelForm):
    class Meta:
        model = Role
        fields = ('name',)

    def __init__(self, *args, **kwargs):
        super(RoleFilterForm, self).__init__(*args, **kwargs)
        self.disable_required('name')


class DeptFilterForm(BaseModelForm):
    class Meta:
        model = Department
        fields = ('name',)

    def __init__(self, *args, **kwargs):
        super(DeptFilterForm, self).__init__(*args, **kwargs)
        self.disable_required('name')
