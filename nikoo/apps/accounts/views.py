from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from nikoo.apps.accounts.form.forms import LoginForm, EditProfileForm


def user_login(request):
    next_url = '/'
    if request.user.is_authenticated:
        next_url = request.GET.get('next')
        if next_url:
            return HttpResponseRedirect(next_url)
        else:
            return HttpResponseRedirect(reverse('dashboard'))

    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            if not username or not password:
                messages.error(request, _("Login with username and password is required."))
            else:
                user = authenticate(username=username, password=password)
                if not user or user is None:
                    messages.error(request, _("Your username or password is not correct."))
                else:
                    if not user.is_active:
                        messages.error(request, _("Username or password is incorrect."))
                    else:
                        login(request, user)
                        # TODO check role
                        next_url = request.POST.get('next') or request.GET.get('next') or None
                        if next_url and next_url is not None:
                            return HttpResponseRedirect(next_url)
                        else:
                            return HttpResponseRedirect(reverse('home'))
    else:
        form = LoginForm()
    context = {
        'form': form,
        'next': request.GET.get('next', next_url),
    }
    return render(request, "accounts/login.html", context)


@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('dashboard'))


@login_required
def dashboard(request):
    # purchase = 0
    # sell = 0
    # ships = Ship.objects.filter(Q(bl_file="") | Q(sgs_file__isnull="") | Q(coo_file__isnull=""))
    # deals = Deal.objects.values('deal_type').annotate(dcount=Count('deal_type')).filter(state=Deal.IN_PROGRESS)
    # users = "{:03d}".format(User.objects.count())
    # for deal in deals:
    #     if deal['deal_type'] == Deal.PURCHASE:
    #         purchase = "{:03d}".format(deal['dcount'])
    #     else:
    #         sell = "{:03d}".format(deal['dcount'])
    # product = "{:03d}".format(Product.objects.count())
    # customer = "{:03d}".format(Customer.objects.count())
    # mine = "{:03d}".format(Mine.objects.count())
    # return render(request, "dashboard.html",
    #               {'ships': ships, 'purchase': purchase, 'sell': sell, 'users': users, 'product': product,
    #                'customer': customer, 'mine': mine})
    return render(request, "dashboard.html")


@login_required
def permission_401(request):
    return render(request, "base/permission_denied.html", {'title': _("Permission denied error")})


@login_required
def edit_profile(request):
    if request.method == 'POST':
        form = EditProfileForm(request.POST, request.FILES, http_request=request, instance=request.user)
        if form.is_valid():
            form.save()
            messages.success(request, _("Edit Personal Success"))
            return HttpResponseRedirect(reverse('edit_profile'))
    else:
        form = EditProfileForm(http_request=request, instance=request.user)
    context = {
        'title': _("Edit profile"),
        'form': form
    }
    return render(request, "accounts/edit_profile.html", context)
