import datetime
from itertools import chain

from django.core.exceptions import ValidationError
from django.forms import BooleanField, CheckboxInput, DateField, DateInput, FileField, ClearableFileInput, \
    MultipleChoiceField, CheckboxSelectMultiple
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from nikoo.apps.utils.calverter import gregorian_to_jalali, jalali_to_gregorian
from nikoo.apps.utils.exceptions import ExceededMaxFileSize, ExceededMinFileSize, ExceededFileType, ExceededBadFile, \
    ExceededBadFileName
from nikoo.apps.utils.file import FileHandler


class WidgetBooleanField(CheckboxInput):
    template_name = 'partials/base/forms/checkbox.html'


class WidgetJalaliDateField(DateInput):
    def render(self, name, value, attrs=None, renderer=None):
        view = 'jalali_date'
        if not attrs:
            attrs = {'class': 'form-control'}
        else:
            if 'class' in attrs:
                if 'decade' in attrs['class']:
                    view = 'decade_jalali_date'
                    attrs['class'] += ' form-control'
            else:
                attrs['class'] = 'form-control'
        if value:
            try:
                value = gregorian_to_jalali(value)
            except:
                value = value
        base_field = super(WidgetJalaliDateField, self).render(name, value, attrs)
        html_field = """
        <div class="input-append input-group %s">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>%s</div>""" % (view, base_field)
        return mark_safe(html_field)

    def value_from_datadict(self, data, files, name):
        formatted_value = data.get(name, None)
        if formatted_value:
            valid_value = formatted_value.replace('/', '-')
        else:
            valid_value = formatted_value
        gregorian_value = jalali_to_gregorian(valid_value)
        if gregorian_value:
            return gregorian_value.isoformat()
        else:
            return gregorian_value


class WidgetDateField(DateInput):
    def render(self, name, value, attrs=None, renderer=None):
        view = 'date'
        if not attrs:
            attrs = {'class': 'form-control'}
        else:
            if 'class' in attrs:
                if 'decade' in attrs['class']:
                    view = 'decade_date'
                attrs['class'] += ' form-control'
            else:
                attrs['class'] = 'form-control'
        if value:
            if isinstance(value, datetime.date):
                value = str(value)
            value = value.replace("-", "/")
        base_field = super(WidgetDateField, self).render(name, value, attrs)
        html_field = """
        <div class="input-group %s">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>%s</div>""" % (view, base_field)
        return mark_safe(html_field)

    def value_from_datadict(self, data, files, name):
        formatted_value = data.get(name, None)
        if formatted_value:
            valid_value = formatted_value.replace('/', '-')
        else:
            valid_value = formatted_value
        return valid_value


class WidgetTimeField(DateInput):
    def render(self, name, value, attrs=None, renderer=None):
        time_val = self.get_value(value)
        input = super(WidgetTimeField, self).render(name, time_val, attrs)

        output = """
                <div class="input-group clockpicker" data-autoclose="true">
                                        %s
                                        <span class="input-group-addon">
                                            <span class="fa fa-clock-o"></span>
                                        </span>
                                    </div>
                """ % input
        return mark_safe(output)

    def get_value(self, value):
        if isinstance(value, datetime.datetime):
            time = value.time()
        else:
            time = value
        return time


class WidgetMultiSelect2(CheckboxSelectMultiple):
    def render(self, name, value, attrs=None, choices=()):
        if value is None:
            value = []
        form_id = attrs.get('id', 'multi_select')
        start_element = u'<select multiple="multiple" name="%s" id="%s" tabindex="-1" ' % (name, form_id)
        start_element += u'class="select2-offscreen">'
        output = [start_element]
        # Normalize to strings
        for i, (option_value, option_label) in enumerate(chain(self.choices, choices)):
            # option_label = conditional_escape(force_unicode(option_label))
            if option_value in value:
                output.append(u'<option value="%s" selected="selected" >%s</option>' % (option_value, option_label))
            else:
                output.append(u'<option value="%s">%s</option>' % (option_value, option_label))
        output.append(u'</select>')
        script = u'<script type="text/javascript">' \
                 u'jQuery(function ($) {' \
                 u'$("#%s").select2({"closeOnSelect": false, "placeholder": ""}); ' \
                 u'}); </script>' % form_id
        output.append(script)
        return mark_safe(u'\n'.join(output))


class WidgetMultiSelect(CheckboxSelectMultiple):
    # pass
    template_name = 'partials/base/forms/checkbox_select.html'
    option_template_name = 'partials/base/forms/checkbox_option.html'


class WidgetFileField(ClearableFileInput):
    # template_name = 'base/forms/file_input.html'
    template_name = 'partials/base/forms/clearable_file_input.html'

    def __init__(self, *args, **kwargs):
        self.url_length = kwargs.pop('url_length', 60)
        self.preview = kwargs.pop('preview', True)
        self.image_width = kwargs.pop('image_width', 200)
        super(WidgetFileField, self).__init__(*args, **kwargs)

    def get_context(self, name, value, attrs):
        context = super(ClearableFileInput, self).get_context(name, value, attrs)
        checkbox_name = self.clear_checkbox_name(name)
        checkbox_id = self.clear_checkbox_id(checkbox_name)
        context.update({
            'checkbox_name': checkbox_name,
            'checkbox_id': checkbox_id,
            'is_initial': self.is_initial(value),
            'input_text': self.input_text,
            'initial_text': self.initial_text,
            'clear_checkbox_label': self.clear_checkbox_label,
            'image_width': self.image_width,
            'preview': self.preview,
            'accept': '.jpg, .jpeg'
        })
        return context


class BootstrapBooleanField(BooleanField):
    widget = WidgetBooleanField()


class BootstrapJalaliDateField(DateField):
    widget = WidgetJalaliDateField()

    def to_python(self, value):
        return super(BootstrapJalaliDateField, self).to_python(value)


class BootstrapDateField(DateField):
    widget = WidgetDateField()

    def to_python(self, value):
        return super(BootstrapDateField, self).to_python(value)


class BootstrapFileField(FileField):
    widget = WidgetFileField()

    def __init__(self, *args, **kwargs):
        super(BootstrapFileField, self).__init__(*args, **kwargs)
        self.help_text = _("Maximum upload size is %s KB" % (FileHandler.MAX_FILE_SIZE / FileHandler.KB))

    def validate(self, value):
        super(BootstrapFileField, self).validate(value)
        try:
            FileHandler.validate(value)
        except ExceededMaxFileSize:
            raise ValidationError(_("Maximum upload size is %s KB." % (FileHandler.MAX_FILE_SIZE / FileHandler.KB)))
        except ExceededMinFileSize:
            raise ValidationError(_("Minimum upload size is %s KB." % FileHandler.MIN_FILE_SIZE))
        except ExceededFileType:
            raise ValidationError(_("Files must be of type, jpg or jpeg."))
        except ExceededBadFile:
            raise ValidationError(_("The file is not valid."))
        except ExceededBadFileName:
            raise ValidationError(_("Please use only English characters to record files."))


class BootstrapTimeField(DateField):
    widget = WidgetTimeField()


class BootstrapMultiSelect2FormField(MultipleChoiceField):
    widget = WidgetMultiSelect2()


class BootstrapMultiSelectFormField(MultipleChoiceField):
    widget = WidgetMultiSelect()


class MultipleChoiceFieldByMultipleTag(MultipleChoiceField):
    pass
