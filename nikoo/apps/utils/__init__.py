import re

not_persian_regex = re.compile("[a-zA-z0-9()_]+")


def has_number(input_string):
    return bool(re.search(r'\d', input_string))


def has_alphabetic_chars(input_string):
    return bool(re.search('[a-zA-Z]', input_string))


class BtnClass(object):
    def __init__(self, name, title="", url_address="", btn_type="btn-primary", swal_title=u"", swal_content=u"",
                 is_popup=False, btn_id='btn_id', new_tab=False, is_view=False, spec_url="", icon="", manager_name=""):
        self.name = name
        self.title = title
        self.url_address = url_address
        self.btn_type = btn_type
        self.icon = icon
        self.swal_title = swal_title
        self.swal_content = swal_content
        self.is_popup = is_popup
        self.btn_id = btn_id
        self.new_tab = new_tab
        self.is_view = is_view
        self.spec_url = spec_url
        self.manager_name = manager_name
