import re
from collections import OrderedDict

from django import forms
from django.forms import BooleanField, DateField, ModelMultipleChoiceField, ModelChoiceField, FileField, DateTimeField, \
    TimeField, ChoiceField, MultipleChoiceField
from django.utils.translation import gettext_lazy as _
from multiselectfield import MultiSelectFormField

from nikoo.apps.utils.fields import BootstrapBooleanField, BootstrapJalaliDateField, BootstrapFileField, \
    BootstrapDateField, \
    BootstrapTimeField, BootstrapMultiSelect2FormField, BootstrapMultiSelectFormField, MultipleChoiceFieldByMultipleTag


class BaseForm(forms.Form):
    jalali_date_field = False
    use_select2 = True
    use_multi_select2 = False

    def __init__(self, *args, **kwargs):
        self.http_request = kwargs.pop('http_request', None)
        super(BaseForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            old_field = self.fields[field]
            new_field = old_field
            if isinstance(self.fields[field], BooleanField):
                if isinstance(self.fields[field].widget, forms.widgets.HiddenInput):
                    continue
                new_field = BootstrapBooleanField(label=old_field.label,
                                                  help_text=old_field.help_text,
                                                  required=old_field.required, initial=old_field.initial)
            elif isinstance(self.fields[field], (DateField, DateTimeField)):
                if self.jalali_date_field:
                    new_field = BootstrapJalaliDateField(label=old_field.label,
                                                         help_text=old_field.help_text,
                                                         required=old_field.required, initial=old_field.initial,
                                                         input_formats=['%Y/%m/%d', '%Y-%m-%d'])
                    # initial = gregorian_to_jalali(old_field.initial)
                else:
                    new_field = BootstrapDateField(label=old_field.label,
                                                   help_text=old_field.help_text,
                                                   required=old_field.required, initial=old_field.initial,
                                                   input_formats=['%Y/%m/%d', '%Y-%m-%d'])
            elif isinstance(self.fields[field], TimeField):
                new_field = BootstrapTimeField(label=old_field.label,
                                               help_text=old_field.help_text,
                                               required=old_field.required, initial=old_field.initial)
            elif isinstance(self.fields[field], MultiSelectFormField):
                if self.use_multi_select2:
                    new_field = BootstrapMultiSelect2FormField(label=old_field.label, help_text=old_field.help_text,
                                                               choices=old_field.choices, required=old_field.required,
                                                               initial=old_field.initial)
                else:
                    new_field = BootstrapMultiSelectFormField(label=old_field.label, help_text=old_field.help_text,
                                                              choices=old_field.choices, required=old_field.required,
                                                              initial=old_field.initial)
            elif isinstance(self.fields[field], MultipleChoiceFieldByMultipleTag):
                self.fields[field] = self.fields[field]
            elif isinstance(self.fields[field],
                            (ModelMultipleChoiceField, ModelChoiceField, ChoiceField, MultipleChoiceField)):
                if self.use_select2:
                    self.fields[field].widget.attrs['class'] = 'select2_combo'
                    new_field = self.fields[field]
            elif isinstance(self.fields[field], FileField):
                new_field = BootstrapFileField(label=old_field.label,
                                               help_text=old_field.help_text,
                                               required=old_field.required, initial=old_field.initial)
            self.fields[field] = new_field

    def disable_paste(self, *fields):
        for field in fields:
            try:
                self.fields[field].widget.attrs['onpaste'] = 'return false;'
            except Exception:
                pass

    def disable_required(self, *fields):
        for field in fields:
            self.fields[field].required = False

    def clean_only_english_fields(self, fields, cleaned_data):
        for field in fields:
            data = cleaned_data.get(field)
            if data:
                if not (re.match("^[ a-zA-Z0-9@._-]+$", data.replace(" ", ""))):
                    self._errors[field] = self.error_class([_("Use only English characters and numbers.")])

    def clean_numeral_fields(self, *fields, cleaned_data):
        for field in fields:
            data = cleaned_data.get(field)
            if data:
                if not (re.match("^[0-9]+$", data)):
                    self._errors[field] = self.error_class([_("Please use only numbers.")])

    def make_readonly(self, *fields):
        for field in fields:
            self.fields[field].widget.attrs.update({'readonly': 'readonly', 'disabled': 'disabled'})


class BaseModelForm(forms.ModelForm):
    jalali_date_field = False
    use_select2 = True
    use_multi_select2 = False

    def __init__(self, *args, **kwargs):
        self.http_request = kwargs.pop('http_request', None)
        self.reset = kwargs.pop('reset', None)
        super(BaseModelForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            old_field = self.fields[field]
            new_field = old_field
            if isinstance(self.fields[field], BooleanField):
                if isinstance(self.fields[field].widget, forms.widgets.HiddenInput):
                    continue
                new_field = BootstrapBooleanField(label=old_field.label,
                                                  help_text=old_field.help_text,
                                                  required=old_field.required, initial=old_field.initial)
            elif isinstance(self.fields[field], (DateField, DateTimeField)):
                if self.jalali_date_field:
                    new_field = BootstrapJalaliDateField(label=old_field.label,
                                                         help_text=old_field.help_text,
                                                         required=old_field.required, initial=old_field.initial,
                                                         input_formats=['%Y/%m/%d', '%Y-%m-%d'])
                    # initial = gregorian_to_jalali(old_field.initial)
                else:
                    new_field = BootstrapDateField(label=old_field.label,
                                                   help_text=old_field.help_text,
                                                   required=old_field.required, initial=old_field.initial,
                                                   input_formats=['%Y/%m/%d', '%Y-%m-%d'])
            elif isinstance(self.fields[field], TimeField):
                new_field = BootstrapTimeField(label=old_field.label,
                                               help_text=old_field.help_text,
                                               required=old_field.required, initial=old_field.initial)
            elif isinstance(self.fields[field], MultiSelectFormField):
                if self.use_multi_select2:
                    new_field = BootstrapMultiSelect2FormField(label=old_field.label, help_text=old_field.help_text,
                                                               choices=old_field.choices, required=old_field.required,
                                                               initial=old_field.initial)
                else:
                    new_field = BootstrapMultiSelectFormField(label=old_field.label, help_text=old_field.help_text,
                                                              choices=old_field.choices, required=old_field.required,
                                                              initial=old_field.initial)
            elif isinstance(self.fields[field], MultipleChoiceFieldByMultipleTag):
                self.fields[field] = self.fields[field]
            elif isinstance(self.fields[field],
                            (ModelMultipleChoiceField, ModelChoiceField, ChoiceField, MultipleChoiceField)):
                if self.use_select2:
                    self.fields[field].widget.attrs['class'] = 'select2_combo'
                    new_field = self.fields[field]
            elif isinstance(self.fields[field], FileField):
                new_field = BootstrapFileField(label=old_field.label,
                                               help_text=old_field.help_text,
                                               required=old_field.required, initial=old_field.initial)
            self.fields[field] = new_field

    def disable_paste(self, *fields):
        for field in fields:
            try:
                self.fields[field].widget.attrs['onpaste'] = 'return false;'
            except Exception:
                pass

    def disable_required(self, *fields):
        for field in fields:
            self.fields[field].required = False

    def clean_only_english_fields(self, fields, cleaned_data):
        for field in fields:
            data = cleaned_data.get(field)
            if data:
                if not (re.match("^[ a-zA-Z0-9@._-]+$", data.replace(" ", ""))):
                    self._errors[field] = self.error_class([_("Use only English characters and numbers.")])

    def clean_numeral_fields(self, fields, cleaned_data):
        for field in fields:
            data = cleaned_data.get(field)
            if data:
                if not (re.match("^[0-9]+$", data)):
                    self._errors[field] = self.error_class([_("Please use only numbers.")])

    def make_readonly(self, fields):
        for field in fields:
            self.fields[field].widget.attrs.update({'readonly': 'readonly', 'disabled': 'disabled'})


def re_order_form_fields_by_form_field_order(form, append_fields_not_in_field_order=False):
    if append_fields_not_in_field_order:
        return OrderedDict(
            (k, form.fields[k]) for k in form.field_order + list((set(form.fields) - set(form.field_order))))
    else:
        return OrderedDict((k, form.fields[k]) for k in form.field_order)
