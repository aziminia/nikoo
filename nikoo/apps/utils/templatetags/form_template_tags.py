from django import template
from django.forms import ModelMultipleChoiceField

register = template.Library()


@register.filter
def add_css(field, css):
    if field:
        if isinstance(field.field, (ModelMultipleChoiceField)):
            return field
        # try:
        #     if isinstance(field.field, CustomModelMultipleChoiceField):
        #         if field.data:
        #             field.field.fill_data(field.data)
        #         elif field.initial:
        #             field.field.fill_data(field.initial)
        #         # else:
        #         #     field.field.fill_data('')
        #     prev_attr = field.field.widget.attrs
        #     if "class" in prev_attr:
        #         css = u"%s %s" % (prev_attr["class"], css)
        # except Exception as e:
        #     pass
        return field.as_widget(attrs={"class": css})
    return field
