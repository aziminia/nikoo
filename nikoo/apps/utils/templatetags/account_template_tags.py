from django import template

register = template.Library()


@register.simple_tag
def get_avatar_user(user):
    try:
        if user.picture:
            return user.picture.url
        else:
            return "/static/img/account/avatar.png"
    except:
        return "/static/img/account/avatar.png"


@register.filter
def check_access(action, http_request):
    return True
