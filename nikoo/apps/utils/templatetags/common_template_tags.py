import math

from django import template
from django.utils import formats
from nikoo.apps.utils.calverter import date_to_jalali_tag

register = template.Library()


@register.filter
def is_true(value):
    return value is True


@register.filter
def is_false(value):
    return value is False


@register.simple_tag
def get_application_type(request):
    return request.GET.get('a')


@register.filter
def date_to_jalali(field):
    if field:
        return date_to_jalali_tag(field)
    return ""


@register.filter
def actions(request):
    ss = request.split('/')
    str_list = [x for x in ss if x]
    return str_list[-1]


@register.filter
def check_for_break(value, state):
    if state == 1:
        if value % 4 == 1:
            return True
    if state == 2:
        if value % 4 == 0:
            return True
    return False


@register.filter
def plus(key, value):
    return key + value


@register.filter
def startwith(field, key):
    key = key + "_"
    if field.name.startswith(key):
        return True
    return False


@register.filter
def startwithkey(field, form):
    for key in form.product_specs.keys():
        if field.name.startswith(key):
            return False
    return True


@register.filter
def get_colspan(product):
    return len(product['rows'][0])


@register.filter
def in_payments(var):
    return var not in ['total', 'payable']


@register.filter
def is_provisional(var):
    return var.lower().startswith("provisional")


@register.filter
def is_payable(var):
    return var.lower().startswith("payable")


@register.filter
def count_span_products(value):
    try:
        return len(value[0]) - 1
    except:
        return 1


@register.filter
def count_span(output):
    try:
        if output['invoice_type_id'] in [2, 3]:
            return 8
        if output['invoice_type_id'] == 4:
            return 11
        else:
            return 1
    except:
        return 1


@register.filter
def count_span_round(value, arrow):
    if arrow == 'up':
        return math.ceil(value / 2)
    elif arrow == 'down':
        return math.floor(value / 2)
    else:
        return 0

@register.filter
def to_date(value, arg=None):
    """Formats a date according to the given format."""
    if isinstance(value, str):
        return value
    if value in (None, ''):
        return ''
    try:
        return formats.date_format(value, arg)
    except AttributeError:
        try:
            return format(value, arg)
        except AttributeError:
            return ''
