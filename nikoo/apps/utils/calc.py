from itertools import chain

from nikoo.apps.base_information.models import Price
from nikoo.apps.utils.calverter import get_date_from_str


def get_avg_index_price(period, index_type, index_date, field):
    result_list = get_list_index_price(period, index_type, index_date)
    total = 0
    for price in result_list:
        total += getattr(price, field)
    return total / len(result_list)


def get_list_index_price(period, index_type, index_date):
    if period is None or not index_type or not index_date:
        return 0
    index_date = get_date_from_str(index_date)
    base_filter = Price.objects.filter(reference_id=index_type)
    if index_date:
        current = base_filter.filter(date=index_date)
        result_list = list(current)
        if period > 0:
            last = base_filter.filter(date__lt=index_date).order_by('-date')[:period]
            next = base_filter.filter(date__gt=index_date).order_by('date')[:period]
            result_list = list(chain(current, last, next))
        return sorted(result_list, key=lambda x: x.date, reverse=False)
    return []


def get_list_and_avg_index_price(period, index_type, index_date):
    result_list = get_list_index_price(period, index_type, index_date)
    platts_list = []
    for price in result_list:
        data = {'date': price.date,
                'pr': price.primum,
                'viu': price.viu,
                'platts': price.price
                }
        platts_list.append(data)
    avg_pr = get_avg_index_price(period, index_type, index_date, 'primum')
    avg_viu = get_avg_index_price(period, index_type, index_date, 'viu')
    avg_platts = get_avg_index_price(period, index_type, index_date, 'price')
    data = {'date': "Average",
            'pr': avg_pr,
            'viu': avg_viu,
            'platts': avg_platts
            }
    platts_list.append(data)
    return platts_list
