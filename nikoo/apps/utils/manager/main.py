import json
from datetime import datetime, date
from decimal import Decimal

from django.db import models
from django.db.models import QuerySet, ForeignKey, OneToOneField, prefetch_related_objects
from django.http import HttpResponse, Http404
from django.shortcuts import render
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from nikoo.apps.utils.calverter import show_persian_datetime, gregorian_to_jalali, show_gregorian_datetime, \
    show_gregorian_date
from nikoo.apps.utils.manager.filter import Filter
from nikoo.apps.utils.manager.sitemap import SiteMap
from nikoo.apps.utils.manager.table import Table, Header, Row

list_manager = []


class RegisterManager(type):

    def __new__(cls, name, bases, dct):
        from django.conf.urls import url
        from nikoo.apps.utils.manager.views import process_main_page, process_actions
        from nikoo.urls import urlpatterns
        obj_manager = type.__new__(cls, name, bases, dct)
        if obj_manager.manager_name not in list_manager:
            list_manager.append(obj_manager)
            urlpatterns += [
                url(r'^%s/$' % obj_manager.manager_name, process_main_page,
                    {'manager_name': obj_manager.manager_name},
                    name='process_main_page_%s' % obj_manager.manager_name),
                url(r'^%s/actions/$' % obj_manager.manager_name, process_actions,
                    {'manager_name': obj_manager.manager_name},
                    name='process_action_%s' % obj_manager.manager_name),
            ]
        return obj_manager


class ObjectManager(object, metaclass=RegisterManager):
    __all_data_cached = None
    manager_name = ""
    manager_title = ""
    manager_guide = ""
    filter_form = None
    filter_class = Filter
    filter_handlers = (
        ()
    )

    order_field = None
    data_per_page = 20
    row_num_list = [10, 20, 30]
    aggregation = False
    print_aggregation = False
    excel_aggregation = False
    sitemap_items = []

    actions = []
    group_actions = []
    top_actions = []

    height = 520
    auto_width = True
    show_jalali_date = False
    has_jalali_filter_form = False
    exceptional_gregorian_dates = []

    extra_filter_content = None
    is_printable = False
    is_printable_in_excel = True
    is_customizable = True
    main_template = 'base/manager/main.html'
    first_load_data = True
    data_error = None

    def __init__(self, http_request):
        self.http_request = http_request
        if self.can_view():
            self.sitemap_items = self.get_sitemap_items()
            self.columns = self.get_all_columns()
            rows = http_request.GET.get('rows') or self.data_per_page
            self.filter_obj = self.filter_class(self.http_request, self.filter_form, self.filter_handlers,
                                                self.other_filter_func, rows, self.order_field,
                                                self.has_jalali_filter_form)
            if self.first_load_data:
                all_data = self.get_all_data_cached()
            else:
                if 'is_search_form' in http_request.GET:
                    all_data = self.get_all_data_cached()
                else:
                    all_data = []
            self.filter_form, self.page_data, self.all_data = self.filter_obj.process_filter(all_data)

    def can_view(self):
        return True

    def get_sitemap_items(self):
        return [SiteMap(self.manager_title, reverse('process_main_page_%s' % self.manager_name))]

    def get_columns(self):
        return []

    def get_all_columns(self):
        columns = self.get_columns()
        if self.actions:
            action_columns = ColumnManager('actions', _("Actions"), 5, is_action=True)
            columns += [action_columns]
        return columns

    def get_all_data(self):
        return []

    def get_all_data_cached(self):
        if not self.__all_data_cached:
            self.__all_data_cached = self.get_all_data()
        return self.__all_data_cached

    def render_to_list(self):
        if not self.can_view():
            return HttpResponse(_("You are not allowed to access this page"))
        context = {
            'manager': self,
            'sitemap_items': self.sitemap_items,
            'jalali_date_picker': self.has_jalali_filter_form
        }
        return render(self.http_request, self.main_template, context)

    def other_filter_func(self, all_data, form):
        return all_data

    def process_actions(self):
        action_type = self.http_request.GET.get('t')
        if action_type == 'json':
            return self.process_json()
        elif action_type == 'action':
            return self.process_manage_actions()
        raise Http404()

    def process_json(self):
        table = self._create_data_table(self.page_data)
        json_out = table.get_dgrid_json(self.filter_obj.total_pages, self.filter_obj.page_num,
                                        self.filter_obj.total_data, self.aggregation)
        return HttpResponse(json_out, content_type='application_type')

    def process_manage_actions(self):
        action_name = self.http_request.GET.get('n')
        instances_id = self.http_request.GET.get('i')
        selected_instances = self._get_instances_by_ids(instances_id)
        all_actions = self.actions + self.group_actions + self.top_actions
        for action in all_actions:
            if action.action_name == action_name:
                if action.is_view:
                    return action.action_view(self.http_request, selected_instances)
                else:
                    do_state = action.do(self.http_request, selected_instances)
                    err_msg = False
                    if isinstance(do_state, tuple):
                        err_msg = do_state[1]
                        do_state = do_state[0]
                    if do_state:
                        return HttpResponse(json.dumps(do_state), content_type="application/json")
                        # return HttpResponse('ok')
                    else:
                        if err_msg is False:
                            err_msg = action.error_message
                        return HttpResponse(err_msg)
                        # return HttpResponse(action.error_message)
        raise Http404

    def _create_data_table(self, page_data, columns=None):
        id_columns = ColumnManager('id', 'id', 0)
        if not columns:
            columns = [id_columns] + self.get_all_columns()
        table = Table()
        header = Header()

        column_name_set = set()
        for column in columns:
            header.create_cell(column.column_name, column.column_title, column.column_width, column.aggregation,
                               column.aggregation_type)
            column_name_set.add(column.column_name)

        page_data = self.select_related_data(column_name_set, page_data)

        table.set_header(header)
        for data in page_data:
            row = Row()
            for column in columns:
                column_name = column.column_name
                if column.is_variable:
                    function = getattr(self, 'get_' + column_name)
                    value = function(data)
                elif column.is_action:
                    f = "<div class='actions_inline'>"
                    for action in self.actions:
                        action_name = action.action_name
                        manager_name = self.http_request.path.split('/')[2]
                        if action_name:
                            # TODO
                            have_access = True
                            # have_access = getattr(self.http_request.user.profile.access,
                            #                       manager_name + "_" + action_name)
                        else:
                            # TODO
                            have_access = True
                            # have_access = getattr(self.http_request.user.profile.access, manager_name)
                        if have_access:
                            f += "<button is_popup=%(is_popup)s new_tab=%(new_tab)s is_view=%(is_view)s spc_url='%(spc_url)s' confirm_message='%(confirm_message)s' help_message='%(help_message)s' name='action-%(name)s' id='action-%(id)s' class='action-button btn btn-circle %(class)s ' title='%(title)s'><i class='fa %(icon)s'></i></button>" % {
                                'name': action.action_name, 'class': action.button_class, 'icon': action.icon,
                                'id': data.id, 'is_popup': action.is_popup, 'new_tab': action.new_tab,
                                'title': action.action_title, 'is_view': action.is_view, 'spc_url': action.new_url,
                                'confirm_message': action.default_confirm_message,
                                'help_message': action.default_help_message}
                    f += "</div>"
                    value = mark_safe(f)
                else:
                    value = getattr(data, column.column_name)
                if isinstance(value, bool):
                    if value is True:
                        # TODO
                        value = "بلی"
                    else:
                        value = "خیر"
                elif isinstance(value, Decimal):
                    value = str(value)
                elif column.show_time and isinstance(value, datetime):
                    if self.show_jalali_date and not (column_name in self.exceptional_gregorian_dates):
                        value = show_persian_datetime(value)
                    else:
                        value = show_gregorian_datetime(value)
                elif isinstance(value, date):
                    if self.show_jalali_date and not (column_name in self.exceptional_gregorian_dates):
                        value = gregorian_to_jalali(value)
                    else:
                        value = show_gregorian_date(value)
                elif isinstance(data, models.Model) and not column.is_variable and column_name != 'actions':
                    if data.__class__._meta.get_field(column.column_name).choices:
                        value = getattr(data, 'get_' + column_name + '_display')()
                        # if not isinstance(value, (SafeUnicode, SafeString)):
                        # value = unicode(value)
                        # value = value
                if value is None or value == 'None':
                    value = '-----'
                row.create_cell(column_name, value, column.column_width, column.aggregation)
            table.add_row(row)
        return table

    def _get_instances_by_ids(self, instance_id):
        if instance_id:
            try:
                instance_id = [int(x) for x in instance_id.split(',')]
            except ValueError:
                return []

            all_data = self.get_all_data_cached()
            try:
                instances = []
                for id in instance_id:
                    instances.append(all_data.get(id=id))
            except Exception:
                instances = []
                for data in all_data:
                    if data.pk in instance_id:
                        instances.append(data)
            return instances
        return []

    def select_related_data(self, column_name_set, page_data):
        """
        this function pre-fetche foreign keys. instead of select one by one in sql, it perform one query using IN(ids)

        in managers, explicit_select_related_set should be set for fields which are not explicitly defined in columns,
            but they are used. for example in columns we have username,
            so user should be explicitly defined as related fields to be pre-fetched

        in managers where all_data are list instead of queryset, explicit_select_related_set should be set.
            in statistics manager it is set so far
        """
        try:
            if isinstance(page_data, QuerySet):
                fields = page_data.model._meta.get_fields()
                foreign_key_field_set = set()
                for f in fields:
                    if isinstance(f, ForeignKey) or isinstance(f, OneToOneField):
                        foreign_key_field_set.add(f.name)
                if hasattr(self, 'explicit_select_related_set'):
                    select_related_set = column_name_set.union(set(self.explicit_select_related_set))
                else:
                    select_related_set = column_name_set
                select_related_set = select_related_set.intersection(foreign_key_field_set)
                select_related_list = list(select_related_set)
                if len(select_related_list) > 0:
                    page_data = page_data.prefetch_related(*select_related_list)
            elif isinstance(page_data, list):
                if hasattr(self, 'explicit_select_related_set'):
                    select_related_list = list(self.explicit_select_related_set)
                    if len(select_related_list) > 0:
                        prefetch_related_objects(page_data, *select_related_list)
        except Exception as e:
            print(e)

        return page_data


class ColumnManager(object):
    def __init__(self, column_name, column_title, column_width, is_variable=False, allow_html=False, aggregation=False,
                 aggregation_type='int', classes=None, show_time=False, hidden=False, is_action=False):
        self.column_name = column_name
        self.column_title = column_title
        self.column_width = column_width
        self.is_variable = is_variable
        self.allow_html = allow_html
        self.aggregation = aggregation
        self.aggregation_type = aggregation_type
        self.classes = classes
        self.show_time = show_time
        self.hidden = hidden
        self.is_action = is_action
