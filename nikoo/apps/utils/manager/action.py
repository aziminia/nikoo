import json

from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.utils.translation import gettext_lazy as _

from nikoo.apps.utils.manager.sitemap import SiteMap


class ManagerAction(object):
    action_name = ""
    action_title = ""
    button_class = "btn-primary"
    is_popup = False
    form_title = ""
    new_tab = False
    is_view = False
    back_url = None
    page_name = ""
    icon = ""
    default_confirm_message = ""
    default_help_message = ""
    new_url = ""

    manager_info = None
    sitemap = []

    def do(self, http_request, selected_instances):
        pass

    def action_view(self, http_request, selected_instances):
        pass

    def get_sitemap(self):
        sitemap = []
        if not self.is_popup:
            try:
                if self.manager_info:
                    sitemap.append(
                        SiteMap(self.manager_info[1], reverse("process_main_page_%s") % self.manager_info[0]))
            except:
                pass
            sitemap.append(SiteMap(self.action_title, ""))
        return sitemap


class AddAction(ManagerAction):
    action_name = "add"
    action_title = _("Add")
    is_view = True
    form_title = _("Add")
    template = "base/manager/actions/add_edit.html"
    is_popup = True

    def __init__(self, model_form, save_def=None, action_title=None):
        if action_title:
            self.action_title = action_title
        self.model_form = model_form
        self.save_def = save_def
        if self.is_popup:
            self.template = "base/manager/actions/modal_add_edit.html"

    def action_view(self, http_request, selected_instances):
        action_name = self.action_name
        manager_name = http_request.path.split('/')[2]
        # have_access =getattr(http_request.user.profile.access, manager_name+"_"+action_name)
        # TODO
        have_access = True
        if not have_access:
            return HttpResponseRedirect(reverse('permission_401'))
        if http_request.method == 'POST':
            form = self.model_form(http_request.POST, http_request.FILES, http_request=http_request)
            if form.is_valid():
                if self.save_def:
                    instance = form.save(commit=False)
                    self.save_def(http_request, instance)
                else:
                    form.save()
                if self.is_popup:
                    response = {'status': 'success', 'msg': _("%s successfully completed.") % self.form_title}
                    return HttpResponse(json.dumps(response), content_type="application/json")
                messages.success(http_request, _("%s successfully completed.") % self.form_title)
        else:
            form = self.model_form(http_request=http_request)

        if self.back_url:
            back_url = reverse(self.back_url)
        else:
            back_url = ""

        return render(http_request, self.template,
                      {'form': form, 'title': self.form_title, 'back_url': back_url, 'page_name': self.page_name,
                       'sitemap_items': self.get_sitemap()})


class EditAction(ManagerAction):
    action_name = "edit"
    action_title = _("Edit")
    is_view = True
    form_title = _("Edit")
    template = "base/manager/actions/modal_add_edit.html"
    button_class = "btn-warning"
    icon = 'fa-edit'
    is_popup = True

    def __init__(self, model_form, action_title=None):
        self.model_form = model_form
        if action_title:
            self.form_title = action_title
            self.action_title = action_title

    def action_view(self, http_request, selected_instances):
        action_name = self.action_name
        manager_name = http_request.path.split('/')[2]
        # TODO
        have_access = True
        # have_access = getattr(http_request.user.profile.access, manager_name + "_" + action_name)
        if not have_access:
            return HttpResponseRedirect(reverse('admin_permission_401'))
        if not selected_instances:
            raise Http404()
        if http_request.method == 'POST':
            form = self.model_form(http_request.POST, http_request.FILES, http_request=http_request,
                                   instance=selected_instances[0])
            if form.is_valid():
                form.save()
                response = {'status': 'success', 'msg': _("%s successfully completed.") % self.form_title}
                return HttpResponse(json.dumps(response), content_type="application/json")
        else:
            form = self.model_form(http_request=http_request, instance=selected_instances[0])

        if self.back_url:
            back_url = reverse(self.back_url)
        else:
            back_url = ""

        return render(http_request, self.template,
                      {'form': form, 'title': self.form_title, 'back_url': back_url, 'page_name': self.page_name,
                       'sitemap_items': self.get_sitemap()})


class AddActionSingle(AddAction):
    is_popup = False


class EditActionSingle(EditAction):
    is_popup = False


class DeleteAction(ManagerAction):
    action_name = "delete"
    action_title = _("Delete")
    button_class = "btn-danger"
    default_confirm_message = _("You sure you want to remove this item?")
    default_help_message = _("Unable to retrieve after deletion!")
    icon = 'fa-times'
    is_popup = True
    is_view = False

    def __init__(self, do_function=None, confirm_message=None, help_message=None):
        if do_function:
            self.do_function = do_function
        if confirm_message:
            self.default_confirm_message = confirm_message
        if help_message:
            self.default_help_message = help_message

    def do(self, http_request, selected_instances):
        action_name = self.action_name
        manager_name = http_request.path.split('/')[2]
        # TODO
        have_access = True
        # have_access = getattr(http_request.user.profile.access, manager_name + "_" + action_name)
        if not have_access:
            return HttpResponseRedirect(reverse('admin_permission_401'))
        for instance in selected_instances:
            try:
                instance.delete()
            except:
                pass
        response = {'status': 'error', 'msg': str(_("Selected item deleted successfully."))}
        return response


class SuspendAction(ManagerAction):
    action_name = "suspend"
    action_title = _("Suspend")
    button_class = "btn-info"
    default_confirm_message = _("Are you sure you want to do the suspend?")
    icon = 'fa-check-square-o'
    is_popup = True
    is_view = False

    def __init__(self, do_function=None, confirm_message=None, help_message=None):
        if do_function:
            self.do_function = do_function
        if confirm_message:
            self.default_confirm_message = confirm_message
        if help_message:
            self.default_help_message = help_message

    def do(self, http_request, selected_instances):
        action_name = self.action_name
        manager_name = http_request.path.split('/')[2]
        # TODO
        have_access = True
        # have_access = getattr(http_request.user.profile.access, manager_name + "_" + action_name)
        if not have_access:
            return HttpResponseRedirect(reverse('admin_permission_401'))
        for instance in selected_instances:
            try:
                instance.active = not instance.active
                instance.save()
            except:
                pass
        response = {'status': 'success', 'msg': str(_("The suspend was successful."))}
        return response


class AddFormsetAction(ManagerAction):
    action_name = "add"
    action_title = _("Add")
    form_title = _("Add")
    is_view = True
    template = "base/manager/actions/formset_add_edit.html"
    is_popup = True

    def __init__(self, base_model_form, formset_model_form):
        self.base_model_form = base_model_form
        self.formset_model_form = formset_model_form
        if self.is_popup:
            self.template = "base/manager/actions/modal_formset_add_edit.html"

    def action_view(self, http_request, selected_instance):
        action_name = self.action_name
        manager_name = http_request.path.split('/')[1]
        # TODO
        have_access = True
        # have_access = getattr(http_request.user.profile.access, manager_name + "_" + action_name)
        if not have_access:
            return HttpResponseRedirect(reverse('admin_permission_401'))
        formset = None
        if http_request.method == 'POST':
            form = self.base_model_form(http_request.POST, http_request.FILES, http_request=http_request)
            if form.is_valid():
                data_form = form.save(commit=False)
                formset = self.formset_model_form(http_request.POST, http_request.FILES, instance=data_form)
                if formset.is_valid():
                    data_form.save()
                    formset.save()
                    form.save_m2m()
                    form = self.base_model_form(http_request=http_request)
                    formset = self.formset_model_form()
                    if self.is_popup:
                        response = {'status': 'success', 'msg': _("%s successfully completed.") % self.form_title}
                        return HttpResponse(json.dumps(response), content_type="application/json")
                    messages.success(http_request, _("%s successfully completed.") % self.form_title)
        else:
            form = self.base_model_form(http_request=http_request)
            formset = self.formset_model_form()

        if self.back_url:
            back_url = reverse(self.back_url)
        else:
            back_url = None
        context = {
            'form': form,
            'formset': formset,
            'back_url': back_url,
            'title': self.form_title,
            'sitemap_items': self.get_sitemap()
        }
        return render(http_request, self.template, context)


# class ViewAction(ManagerAction):
#     action_name = "details"
#     action_title = _("View details")
#     is_view = True
#     template = "base_information/details_send_newsletter.html"
#     is_popup = True
#     icon = 'fa-file-text-o'
#     button_class = "btn-warning"
#
#     def action_view(self, http_request, selected_instances):
#         action_name = self.action_name
#         manager_name = http_request.path.split('/')[2]
#         have_access = getattr(http_request.user.profile.access, manager_name + "_" + action_name)
#         if not have_access:
#             if http_request.user.profile.role == 0:
#                 return HttpResponseRedirect(reverse('error_401'))
#             else:
#                 return HttpResponseRedirect(reverse('admin_permission_401'))
#         return render(http_request, self.template,
#                       {'data': selected_instances, 'title': self.form_title, 'page_name': self.page_name,
#                        'sitemap_items': self.get_sitemap()})

class EditFormsetAction(ManagerAction):
    action_name = "edit"
    action_title = _("Edit")
    form_title = _("Edit")
    is_view = True
    template = "base/manager/actions/formset_add_edit.html"
    is_popup = True
    button_class = "btn-warning"
    icon = 'fa-edit'

    def __init__(self, base_model_form, formset_model_form):
        self.base_model_form = base_model_form
        self.formset_model_form = formset_model_form
        if self.is_popup:
            self.template = "base/manager/actions/modal_formset_add_edit.html"

    def action_view(self, http_request, selected_instances):
        action_name = self.action_name
        manager_name = http_request.path.split('/')[2]
        # TODO
        have_access = True
        # have_access = getattr(http_request.user.profile.access, manager_name + "_" + action_name)
        if not have_access:
            return HttpResponseRedirect(reverse('admin_permission_401'))
        if not selected_instances:
            raise Http404()
        formset = None
        if http_request.method == 'POST':
            form = self.base_model_form(http_request.POST, http_request.FILES, http_request=http_request,
                                        instance=selected_instances[0])
            if form.is_valid():
                data_form = form.save(commit=False)
                formset = self.formset_model_form(http_request.POST, http_request.FILES, instance=data_form)
                if formset.is_valid():
                    data_form.save()
                    formset.save()
                    form.save_m2m()
                    if self.is_popup:
                        response = {'status': 'success', 'msg': _("%s successfully completed.") % self.form_title}
                        return HttpResponse(json.dumps(response), content_type="application/json")
                    messages.success(http_request, _("%s successfully completed.") % self.form_title)
        else:
            form = self.base_model_form(http_request=http_request, instance=selected_instances[0])
            formset = self.formset_model_form(instance=selected_instances[0])

        if self.back_url:
            back_url = reverse(self.back_url)
        else:
            back_url = None
        context = {
            'form': form,
            'formset': formset,
            'back_url': back_url,
            'title': self.form_title,
            'sitemap_items': self.get_sitemap()
        }
        return render(http_request, self.template, context)


class AddFormsetActionSingle(AddFormsetAction):
    is_popup = False


class EditFormsetActionSingle(EditFormsetAction):
    is_popup = False


class AddApplicationTypeFormsetActionSingle(AddFormsetAction):
    is_popup = False
    template = "base/manager/actions/application_type_formset_add_edit.html"


class EditAppTypeFormsetActionSingle(EditFormsetAction):
    is_popup = False
    template = "base/manager/actions/application_type_formset_add_edit.html"
