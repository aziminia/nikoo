from django.conf import settings


def install_manager():
    for app in settings.INSTALLED_APPS:
        try:
            __import__(app + '.managers')
        except Exception as e:
            print(e)
