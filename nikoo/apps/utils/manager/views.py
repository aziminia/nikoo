from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, Http404
from django.urls import reverse

from nikoo.apps.utils.manager.main import list_manager


@login_required
def process_main_page(http_request, manager_name):
    for manager in list_manager:
        if manager_name == manager.manager_name:
            manager_object = manager(http_request=http_request)
            if manager_object.can_view():
                if http_request.method == 'POST':
                    # TODO
                    try:  # if this manager has a process_extra_filter_option function
                        return manager_object.process_extra_filter_option()
                    except Exception:
                        pass
                return manager_object.render_to_list()
            else:
                return HttpResponseRedirect(reverse('admin_permission_401'))
    return Http404()


@login_required
def process_actions(http_request, manager_name):
    for manager in list_manager:
        if manager_name == manager.manager_name:
            manager_object = manager(http_request=http_request)
            if manager_object.can_view():
                return manager_object.process_actions()
            break
    return Http404()
