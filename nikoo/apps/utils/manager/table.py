import json
from collections import OrderedDict

from django.utils.translation import gettext_lazy as _


class Cell(object):
    def __init__(self, name, value, width, aggregation):
        self.name = name
        self.value = value
        self.width = width
        self.aggregation = aggregation


class Row(object):
    def __init__(self):
        self.cells = []

    def add_cell(self, cell):
        self.cells.append(cell)

    def create_cell(self, name, value, width, aggregation):
        self.add_cell(Cell(name, value, width, aggregation))

    def get_cells_dict(self):
        cells_dict = OrderedDict()
        for cell in self.cells:
            cells_dict[cell.name] = cell.value
        return cells_dict

    def __iter__(self):
        return iter(self.cells)


class Header(object):
    def __init__(self):
        self.cells = []
        self.sums = []

    def add_cell(self, cell):
        self.cells.append(cell)

    def create_cell(self, name, value, width, aggregation, aggregation_type):
        self.add_cell(Cell(name, value, width, aggregation))
        if aggregation_type == "str":
            self.sums.append(_("All"))
        else:
            self.sums.append(0)

    def aggregate(self, row):
        i = 0
        for cell in row:
            if cell.aggregation:
                try:
                    value = int(cell.value)
                except:
                    value = 0
                self.sums[i] += value
            i += 1

    def __iter__(self):
        return iter(self.cells)


class Table(object):
    def __init__(self):
        self.header = None
        self.rows = []
        self.name = None

    def set_name(self, name):
        self.name = name

    def set_header(self, header):
        self.header = header

    def add_row(self, row):
        self.rows.append(row)
        self.header.aggregate(row)

    def get_dgrid_json(self, total_page, current_page, all_data_count, aggregation=False):
        json_dict = OrderedDict({
            'total': total_page,
            'page': current_page,
            'recordsTotal': all_data_count,
            'recordsFiltered': all_data_count
        })

        json_rows = []
        for row in self.rows:
            json_rows.append(row.get_cells_dict())
        json_dict['data'] = json_rows

        footer_rows = {}
        i = 0
        for cell in self.header:
            footer_rows[cell.name] = self.header.sums[i]
            i += 1

        try:
            footer_rows.update({self.header.cells[1].name: _("Total sum in page:")})
        except Exception:
            pass

        if aggregation:
            json_dict['userdata'] = footer_rows

        return json.dumps(json_dict)

    def __iter__(self):
        return iter(self.rows)
