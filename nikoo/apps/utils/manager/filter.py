import copy
import datetime

from django import forms
from django.http import QueryDict

from nikoo.apps.utils.calverter import gregorian_to_jalali, jalali_to_gregorian
from nikoo.apps.utils.pagination import CustomPaginator


class Filter(object):
    check_field = None
    initiation_required_fields = []

    def __init__(self, http_request, filter_form, filter_handlers, other_filter_func, data_per_page, order_field=None,
                 jalali_filter=None):
        self.http_request = http_request
        self.filter_form = filter_form
        self.filter_handlers = filter_handlers
        self.other_filter_func = other_filter_func
        self.data_per_page = data_per_page
        self.order_field = order_field
        self.ordering_handle()
        self.page_num = self.http_request.GET.get('page') or 1
        self.jalali_filter = jalali_filter
        self.all_data = None
        self.total_pages = 0
        self.total_data = 0

    def process_filter(self, all_data, use_other_filter_func=True):
        kwargs = {}
        form = None
        form_data = {}

        if self.filter_form:
            if self.http_request.GET:
                form = self.filter_form(self.http_request.GET, http_request=self.http_request)
            else:
                form = self.filter_form(http_request=self.http_request)
            form_data = copy.copy(form.data)

            if self.check_field not in self.http_request.GET:
                for field_name in self.initiation_required_fields:
                    initial_value = form.fields[field_name].initial
                    if isinstance(form.fields[field_name], (forms.DateField, forms.DateTimeField)):
                        if self.jalali_filter:
                            initial_value = gregorian_to_jalali(str(form.fields[field_name].initial))
                    if isinstance(form_data, QueryDict):
                        form_data.setlist(field_name, [initial_value])
                    else:
                        form_data[field_name] = initial_value

            if self.filter_handlers:
                for handler in self.filter_handlers:
                    self.check_handler(handler, kwargs, form_data)
            else:
                for field in form.fields:
                    if isinstance(form.fields[field], forms.CharField):
                        handler = (field, 'str')
                    elif isinstance(form.fields[field], forms.DateField):
                        handler = (field, 'pdate')
                    elif isinstance(form.fields[field], forms.ModelChoiceField):
                        handler = (field, 'm2o')
                    elif isinstance(form.fields[field], forms.MultipleChoiceField):
                        handler = (field, 'm2m')
                    elif isinstance(form.fields[field], forms.NullBooleanField):
                        handler = (field, 'null_bool')
                    elif isinstance(form.fields[field], forms.BooleanField):
                        handler = (field, 'bool')
                    else:
                        handler = (field, '')
                    self.check_handler(handler, kwargs, form_data)

        if hasattr(all_data, 'model'):
            if self.order_field.replace('-', '') in [f.name for f in all_data.model._meta.get_fields()]:
                all_data = all_data.filter(**kwargs).order_by(self.order_field).distinct()
            else:
                all_data = all_data.filter(**kwargs).distinct()
        if use_other_filter_func:
            all_data = self.other_filter_func(all_data, form_data)

        paginate_data = self.get_paginated_data_from_all_data(all_data)
        return form, paginate_data, all_data

    def check_handler(self, handler, kwargs, form_data):
        field_name = handler[0]
        field_type = handler[1]

        if len(handler) > 2:
            django_lookup = handler[2] or field_name
        else:
            django_lookup = field_name

        if field_type == 'm2m':
            try:
                field_value = form_data.getlist(field_name)
            except:
                field_value = form_data.get(field_name)
        else:
            field_value = form_data.get(field_name)

        if field_value and field_value != 'None':
            if field_type == 'str':
                try:
                    field_value = field_value.strip()
                except:
                    pass
                kwargs[django_lookup + "__icontains"] = field_value
            elif field_type == 'int':
                kwargs[django_lookup] = int(field_value)
            elif field_type == 'bool':
                if field_value == 'on':
                    kwargs[django_lookup] = True
            elif field_type == 'null_bool':
                if field_value in (2, "2", "on"):
                    kwargs[django_lookup] = True
                elif field_value in (3, "3"):
                    kwargs[django_lookup] = False
            elif field_type == 'm2o':
                kwargs[django_lookup + "__id"] = field_value
            elif field_type == 'm2m':
                kwargs[django_lookup + "__in"] = field_value
            elif field_type == 'pdate':
                kwargs[django_lookup] = jalali_to_gregorian(field_value).isoformat()
            elif field_type == 'str_date':
                changed_format = field_value.replace('/', '-')
                kwargs[django_lookup] = changed_format
            elif field_type == 'str_date_time_min':
                try:
                    field_value = str(field_value)
                    if self.jalali_filter:
                        changed_format = jalali_to_gregorian(field_value)
                        date = datetime.date(year=changed_format.year, month=changed_format.month,
                                             day=changed_format.day)
                    else:
                        changed_format = field_value.replace("/", "-")
                        date_list = changed_format.split("-")
                        date = datetime.date(year=int(date_list[0]), month=int(date_list[1]), day=int(date_list[2]))

                    value = datetime.datetime.combine(date, datetime.time.min)
                    kwargs[django_lookup] = value
                except:
                    pass
            elif field_type == 'str_date_time_max':
                try:
                    field_value = str(field_value)
                    if self.jalali_filter:
                        changed_format = jalali_to_gregorian(field_value)
                        date = datetime.date(year=changed_format.year, month=changed_format.month,
                                             day=changed_format.day)
                    else:
                        changed_format = field_value.replace("/", "-")
                        date_list = changed_format.split("-")
                        date = datetime.date(year=int(date_list[0]), month=int(date_list[1]), day=int(date_list[2]))

                    value = datetime.datetime.combine(date, datetime.time.max)
                    kwargs[django_lookup] = value
                except:
                    pass
            else:
                kwargs[django_lookup] = field_value

    def get_paginated_data_from_all_data(self, all_data):
        self.all_data = all_data
        p = CustomPaginator(all_data, self.data_per_page)
        self.total_pages = p.num_pages
        self.total_data = p.count
        if int(self.page_num) > self.total_pages:
            self.page_num = self.total_pages
        page = p.page(self.page_num)
        paginate_data = page.object_list
        return paginate_data

    def ordering_handle(self):
        if not self.order_field:
            self.order_field = 'id'
            order_field = self.http_request.GET.get('sidx')
            order_type = self.http_request.GET.get('sord')
            if order_field:
                if order_type == 'asc':
                    self.order_field = order_field
                else:
                    self.order_field = '-' + order_field
