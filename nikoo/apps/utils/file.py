import os
import uuid
from datetime import datetime

import magic

from nikoo.apps.utils import not_persian_regex
from nikoo.apps.utils.exceptions import ExceededBadFileName, ExceededFileType, ExceededMaxFileSize, ExceededMinFileSize


class FileHandler(object):
    MAX_FILE_SIZE = 512000
    MIN_FILE_SIZE = 10
    KB = 1024

    @classmethod
    def validate(cls, file):
        if file:
            val = os.path.splitext(file.name)[0]
            if not not_persian_regex.match(val):
                raise ExceededBadFileName()
            mime_type = magic.from_buffer(file.read(), mime=True)
            if mime_type not in ['image/jpeg', 'image/pjpeg']:
                raise ExceededFileType()
            ext = os.path.splitext(file.name)[-1].lower()
            valid_extensions = ['.jpg', '.jpeg', '.JPG', '.JPEG']
            if ext not in valid_extensions:
                raise ExceededFileType()
            if file._size > FileHandler.MAX_FILE_SIZE:
                raise ExceededMaxFileSize()
            elif file._size < FileHandler.MIN_FILE_SIZE:
                raise ExceededMinFileSize()

    @classmethod
    def generate_file_path_profile(cls, instance, file_name):
        now = datetime.now()
        extension = file_name.split('.')[-1].lower()
        str_now = str(now.year) + str(now.month) + str(now.day) + str(now.hour) + str(now.minute) + str(now.second)
        file_name = "%s_%s.%s" % (str_now, uuid.uuid4(), extension)
        return os.path.join('profile', str(instance.username), file_name)

    @classmethod
    def generate_file_on_folder(cls, filename, folder_name):
        now = datetime.now()
        ext = filename.split('.')[-1].lower()
        filename = "%s.%s" % (uuid.uuid4(), ext)
        return os.path.join(folder_name, str(now.year), str(now.month), str(now.day), str(now.hour), filename)


def upload_profile_to(instance, file_name):
    return FileHandler.generate_file_path_profile(instance, file_name)


def ship_document_upload_to(instance, filename):
    return FileHandler.generate_file_on_folder(filename, 'ship_documents')


def deal_agreement_upload_to(instance, filename):
    return FileHandler.generate_file_on_folder(filename, 'purchase_agreement')


def deal_file_upload_to(instance, filename):
    return FileHandler.generate_file_on_folder(filename, 'deal_file')


def bill_slip_upload_to(instance, filename):
    return FileHandler.generate_file_on_folder(filename, 'bill_slip')
