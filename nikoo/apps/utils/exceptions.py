class ExceededMaxFileSize(Exception):
    pass


class ExceededMinFileSize(Exception):
    pass


class ExceededFileType(Exception):
    pass


class ExceededBadFile(Exception):
    pass


class ExceededBadFileName(Exception):
    pass
