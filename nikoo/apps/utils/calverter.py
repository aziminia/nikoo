GREGORIAN_EPOCH = 1721425.5
GREGORIAN_WEEKDAYS = ("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")

ISLAMIC_EPOCH = 1948439.5
ISLAMIC_WEEKDAYS = ("al-ahad", "al-'ithnayn", "ath-thalatha'", "al-arbia`aa'", "al-khamis", "al-jumu`a", "as-sabt")

JALALI_EPOCH = 1948320.5
JALALI_WEEKDAYS = ("Yekshanbeh", "Doshanbeh", "Seshhanbeh", "Chaharshanbeh", "Panjshanbeh", "Jomeh", "Shanbeh")

import math
from datetime import datetime, date


class Calverter:
    """
    Converter Main Class
    """

    def __init__(self):
        self.J0000 = 1721424.5  # Julian date of Gregorian epoch: 0000-01-01
        self.J1970 = 2440587.5  # Julian date at Unix epoch: 1970-01-01
        self.JMJD = 2400000.5  # Epoch of Modified Julian Date system
        self.J1900 = 2415020.5  # Epoch (day 1) of Excel 1900 date system (PC)
        self.J1904 = 2416480.5  # Epoch (day 0) of Excel 1904 date system (Mac)

        self.norm_leap = ("Normal year", "Leap year")

    def jwday(self, j):
        "Calculate day of week from Julian day"

        return int(math.floor((j + 1.5))) % 7

    def weekday_before(self, weekday, jd):
        """
        Return Julian date of given weekday (0 = Sunday)
        in the seven days ending on jd.
        """

        return jd - self.jwday(jd - weekday)

    def search_weekday(self, weekday, jd, direction, offset):
        """
        Determine the Julian date for:

        @param weekday: Day of week desired, 0 = Sunday
        @param jd: Julian date to begin search
        @param direction: 1 = next weekday, -1 = last weekday
        @param offset: Offset from jd to begin search
        """

        return self.weekday_before(weekday, jd + (direction * offset))

    # Utility weekday functions, just wrappers for search_weekday

    def nearest_weekday(self, weekday, jd):
        return self.search_weekday(weekday, jd, 1, 3)

    def next_weekday(self, weekday, jd):
        return self.search_weekday(weekday, jd, 1, 7)

    def next_or_current_weekday(self, weekday, jd):
        return self.search_weekday(weekday, jd, 1, 6)

    def previous_weekday(self, weekday, jd):
        return self.search_weekday(weekday, jd, -1, 1)

    def previous_or_current_weekday(self, weekday, jd):
        return self.search_weekday(weekday, jd, 1, 0)

    def leap_gregorian(self, year):
        "Is a given year in the Gregorian calendar a leap year ?"

        return ((year % 4) == 0) and (not (((year % 100) == 0) and ((year % 400) != 0)))

    def gregorian_to_jd(self, year, month, day):
        "Determine Julian day number from Gregorian calendar date"

        tm = 0 if month <= 2 else (-1 if self.leap_gregorian(year) else -2)

        return (GREGORIAN_EPOCH - 1) + (365 * (year - 1)) + math.floor((year - 1) / 4) + (
            -math.floor((year - 1) / 100)) + \
               math.floor((year - 1) / 400) + math.floor((((367 * month) - 362) / 12) + tm + day)

    def jd_to_gregorian(self, jd):
        "Calculate Gregorian calendar date from Julian day"

        wjd = math.floor(jd - 0.5) + 0.5
        depoch = wjd - GREGORIAN_EPOCH
        quadricent = math.floor(depoch / 146097)
        dqc = depoch % 146097
        cent = math.floor(dqc / 36524)
        dcent = dqc % 36524
        quad = math.floor(dcent / 1461)
        dquad = dcent % 1461
        yindex = math.floor(dquad / 365)
        year = int((quadricent * 400) + (cent * 100) + (quad * 4) + yindex)
        if not ((cent == 4) or (yindex == 4)):
            year += 1

        yearday = wjd - self.gregorian_to_jd(year, 1, 1)

        leapadj = 0 if wjd < self.gregorian_to_jd(year, 3, 1) else (1 if self.leap_gregorian(year) else 2)

        month = int(math.floor((((yearday + leapadj) * 12) + 373) / 367))
        day = int(wjd - self.gregorian_to_jd(year, month, 1)) + 1

        return year, month, day

    def n_weeks(self, weekday, jd, nthweek):

        j = 7 * nthweek
        if nthweek > 0:
            j += self.previous_weekday(weekday, jd)
        else:
            j += self.next_weekday(weekday, jd)
        return j

    def iso_to_julian(self, year, week, day):
        "Return Julian day of given ISO year, week, and day"

        return day + self.n_weeks(0, self.gregorian_to_jd(year - 1, 12, 28), week)

    def jd_to_iso(self, jd):
        "Return array of ISO (year, week, day) for Julian day"

        year = self.jd_to_gregorian(jd - 3)[0]
        if jd >= self.iso_to_julian(year + 1, 1, 1):
            year += 1

        week = int(math.floor((jd - self.iso_to_julian(year, 1, 1)) / 7) + 1)
        day = self.jwday(jd)
        if day == 0:
            day = 7

        return year, week, day

    def iso_day_to_julian(self, year, day):
        "Return Julian day of given ISO year, and day of year"

        return (day - 1) + self.gregorian_to_jd(year, 1, 1)

    def jd_to_iso_day(self, jd):
        "Return array of ISO (year, day_of_year) for Julian day"

        year = self.jd_to_gregorian(jd)[0]
        day = int(math.floor(jd - self.gregorian_to_jd(year, 1, 1))) + 1
        return year, day

    def pad(self, Str, howlong, padwith):
        "Pad a string to a given length with a given fill character. "

        s = str(Str)

        while s.length < howlong:
            s = padwith + s
        return s

    def leap_islamic(self, year):
        "Is a given year a leap year in the Islamic calendar ?"

        return (((year * 11) + 14) % 30) < 11

    def islamic_to_jd(self, year, month, day):
        "Determine Julian day from Islamic date"

        return (day + math.ceil(29.5 * (month - 1)) + (year - 1) * 354 + \
                math.floor((3 + (11 * year)) / 30) + ISLAMIC_EPOCH) - 1

    def jd_to_islamic(self, jd):
        "Calculate Islamic date from Julian day"

        jd = math.floor(jd) + 0.5
        year = int(math.floor(((30 * (jd - ISLAMIC_EPOCH)) + 10646) / 10631))
        month = int(min(12, math.ceil((jd - (29 + self.islamic_to_jd(year, 1, 1))) / 29.5) + 1))
        day = int(jd - self.islamic_to_jd(year, month, 1)) + 1;
        return year, month, day

    def leap_jalali(self, year):
        "Is a given year a leap year in the Jalali calendar ?"

        return ((((((year - 474 if year > 0 else 473) % 2820) + 474) + 38) * 682) % 2816) < 682

    def jalali_to_jd(self, year, month, day):
        "Determine Julian day from Jalali date"

        epbase = year - 474 if year >= 0 else 473
        epyear = 474 + (epbase % 2820)

        if month <= 7:
            mm = (month - 1) * 31
        else:
            mm = ((month - 1) * 30) + 6

        return day + mm + math.floor(((epyear * 682) - 110) / 2816) + (epyear - 1) * 365 + \
               math.floor(epbase / 2820) * 1029983 + (JALALI_EPOCH - 1)

    def jd_to_jalali(self, jd):
        "Calculate Jalali date from Julian day"

        jd = math.floor(jd) + 0.5
        depoch = jd - self.jalali_to_jd(475, 1, 1)
        cycle = math.floor(depoch / 1029983)
        cyear = depoch % 1029983
        if cyear == 1029982:
            ycycle = 2820
        else:
            aux1 = math.floor(cyear / 366)
            aux2 = cyear % 366
            ycycle = math.floor(((2134 * aux1) + (2816 * aux2) + 2815) / 1028522) + aux1 + 1

        year = int(ycycle + (2820 * cycle) + 474)
        if year <= 0:
            year -= 1

        yday = (jd - self.jalali_to_jd(year, 1, 1)) + 1
        if yday <= 186:
            month = int(math.ceil(yday / 31))
        else:
            month = int(math.ceil((yday - 6) / 30))

        day = int(jd - self.jalali_to_jd(year, month, 1)) + 1
        return year, month, day


def gregorian_to_jalali(date):
    if date is None or date == '':
        return None
    calc = Calverter()
    str_date = str(date)
    year = str_date[:4]
    month = str_date[5:7]
    day = str_date[8:10]
    jd = calc.gregorian_to_jd(year=int(year), month=int(month), day=int(day))
    jalali = calc.jd_to_jalali(jd)
    return "%s/%s/%s" % (str(jalali[0]).rjust(4, '0'), str(jalali[1]).rjust(2, '0'), str(jalali[2]).rjust(2, '0'))


def show_persian_datetime(date_time):
    if date_time:
        date = gregorian_to_jalali(datetime.date(date_time.year, date_time.month, date_time.day))
        return u"%s %s:%s:%s" % (date, date_time.hour, date_time.minute, date_time.second)
    else:
        return u"---"


def show_gregorian_datetime(datetime):
    if datetime:
        return u"%s/%s/%s %s:%s:%s" % (datetime.year, datetime.month, datetime.day, datetime.hour, datetime.minute,
                                       datetime.second)
    else:
        return u"---"


def show_gregorian_date(datetime):
    if datetime:
        return u"%s/%s/%s " % (datetime.year, datetime.month, datetime.day)
    else:
        return u"---"


def jalali_to_gregorian(str_date):
    try:
        calc = Calverter()
        if len(str_date) == 8:
            year = str_date[:4]
            month = str_date[4:6]
            day = str_date[6:8]
        elif len(str_date) == 10:
            year = str_date[:4]
            month = str_date[5:7]
            day = str_date[8:10]
        else:
            splitter = str_date.split('/')
            year = splitter[0]
            month = splitter[1]
            day = splitter[2]

        if not year.isdigit() or not month.isdigit() or not day.isdigit():
            return None

        jd = calc.jalali_to_jd(int(year), int(month), int(day))
        g = calc.jd_to_gregorian(jd)
        return date(g[0], g[1], g[2])
    except Exception as e:
        return None


def filter_jalali_to_gregorian(str_date):
    calc = Calverter()
    jd = calc.jalali_to_jd(str_date.year, str_date.month, str_date.day)
    g = calc.jd_to_gregorian(jd)
    return datetime.strptime(str(g[0]) + '-' + str(g[1]) + '-' + str(g[2]), '%Y-%m-%d')


def pdate_string_to_miladi(year, month, day):
    calc = Calverter()
    jd = calc.jalali_to_jd(year, month, day)
    y, m, d = calc.jd_to_gregorian(jd)
    return date(year=y, month=m, day=d)


def get_date_from_jalali_str(date):
    try:
        date = pdate_string_to_miladi(year=date.year, month=date.month, day=date.day)
        return date
    except Exception:
        return None


def str_jalali_to_gregorian(str_date):
    try:
        calc = Calverter()
        if len(str_date) == 8:
            year = str_date[:4]
            month = str_date[4:6]
            day = str_date[6:8]
        elif len(str_date) == 10:
            year = str_date[:4]
            month = str_date[5:7]
            day = str_date[8:10]
        else:
            splitter = str_date.split('/')
            year = splitter[0]
            month = splitter[1]
            day = splitter[2]

        if not year.isdigit() or not month.isdigit() or not day.isdigit():
            return None

        jd = calc.jalali_to_jd(int(year), int(month), int(day))
        g = calc.jd_to_gregorian(jd)
        return date(g[0], g[1], g[2])
    except Exception as e:
        return None


def date_to_jalali_tag(date_object):
    calc = Calverter()
    jd = calc.gregorian_to_jd(year=date_object.year, month=date_object.month, day=date_object.day)
    jalali = calc.jd_to_jalali(jd)
    full_date = str(jalali[0]) + '/' + str(jalali[1]) + '/' + str(jalali[2])
    return full_date


def get_date_from_str(date_str):
    """
        covert a date string to date object
    """
    try:
        date_str = str(date_str)
        if "/" in date_str:
            num_list = date_str.split("/")
        else:
            num_list = date_str.split("-")
        date = datetime(year=int(num_list[0]), month=int(num_list[1]), day=int(num_list[2])).date()
        return date
    except Exception:
        return None
