from django.db import models


class LogicalDeletedManager(models.Manager):
    def get_queryset(self):
        return super(LogicalDeletedManager, self).get_queryset().exclude(is_deleted=True)
