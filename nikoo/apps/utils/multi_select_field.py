# -*- coding: utf-8 -*-
# Copyright (c) 2012 by Pablo Martín <goinnn@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this programe.  If not, see <http://www.gnu.org/licenses/>.

import sys

from django.utils.encoding import python_2_unicode_compatible
from multiselectfield.db.fields import MultiSelectField




@python_2_unicode_compatible
class MSFList(list):
    def __str__(msgl):
        l = [msgl.choices.get(int(i)) if i.isdigit() else msgl.choices.get(i) for i in msgl]
        return u', '.join([str(s) for s in l])

    def set_choices(self, choices):
        self.choices = choices


class CustomMultiSelectField(MultiSelectField):

    def to_python(self, value):
        choices = dict(self.flatchoices)
        if value:
            if isinstance(value, list):
                return value
            else:
                msf_list = MSFList(value.split(','))
                msf_list.set_choices(choices)
                return msf_list
        return MSFList([])