from django.core.paginator import Paginator
from django.utils.functional import cached_property


class CustomPaginator(Paginator):
    @cached_property
    def count(self):
        """
        Returns the total number of objects, across all pages.
        """
        try:
            return self.object_list.only('id').count()
        except (AttributeError, TypeError):
            # AttributeError if object_list has no count() method.
            # TypeError if object_list.count() requires arguments
            # (i.e. is of type list).
            return len(self.object_list)


QuerySetPaginator = CustomPaginator  # For backwards-compatibility.
