import datetime
from collections import OrderedDict

from django import forms
from django.http import Http404
from django.utils.translation import gettext_lazy as _

from nikoo.apps.base_information.models import CurrencyRate, Document, DocumentSpec, Product, ProductSpec
from nikoo.apps.deal.models import MyCompany, Mine, Deal, BillSlip, DealDocumentSpec, \
    ProductDeal, ProductSpecDeal, ShipDeal, DealPurchaseCost
from nikoo.apps.utils.calc import get_avg_index_price
from nikoo.apps.utils.fields import BootstrapDateField, BootstrapJalaliDateField, BootstrapBooleanField, \
    BootstrapFileField
from nikoo.apps.utils.forms import BaseModelForm, BaseForm


class MyCompanyForm(BaseModelForm):
    class Meta:
        model = MyCompany
        exclude = ('is_deleted',)
        widgets = {
            'address': forms.Textarea(attrs={'rows': 3}),
        }

    def __init__(self, *args, **kwargs):
        super(MyCompanyForm, self).__init__(*args, **kwargs)


class MineForm(BaseModelForm):
    class Meta:
        model = Mine
        exclude = ('is_deleted',)
        widgets = {
            'company_address': forms.Textarea(attrs={'rows': 3}),
            'location': forms.Textarea(attrs={'rows': 3}),
        }

    def __init__(self, *args, **kwargs):
        super(MineForm, self).__init__(*args, **kwargs)

        self.field_order = ['name', 'owner', 'company_name', 'company_address', 'company_phone_number',
                            'cell_phone_number', 'email', 'location']
        self.fields = OrderedDict((k, self.fields[k]) for k in self.field_order)

    def clean_company_phone_number(self):
        company_phone_number = self.cleaned_data.get('company_phone_number')
        if company_phone_number:
            if not company_phone_number.isdigit():
                self._errors['company_phone_number'] = self.error_class([_("Please use only numbers.")])
        return company_phone_number

    def clean_cell_phone_number(self):
        cell_phone_number = self.cleaned_data.get('cell_phone_number')
        if cell_phone_number:
            if not cell_phone_number.isdigit():
                self._errors['cell_phone_number'] = self.error_class([_("Please use only numbers.")])
        return cell_phone_number


# ***************************************SELL******************************************
class SellForm(BaseModelForm):
    class Meta:
        model = Deal
        exclude = ('deal_type', 'mine', 'state', 'creator', 'created_date', 'is_deleted', 'documents', 'platts')
        widgets = {
            'description': forms.Textarea(attrs={'rows': 3}),
        }

    def __init__(self, *args, **kwargs):
        super(SellForm, self).__init__(*args, **kwargs)
        self.layout_general = ['vessel_name', 'agreement_no', 'agreement_types', 'project_date', 'ship', 'my_company',
                               'index_types', 'period', 'index_date', 'index_price', 'down_payment', 'description',
                               'min_weight_diff', 'max_weight_diff', ]
        self.field_order = ['vessel_name', 'agreement_no', 'project_date', 'ship', 'my_company', 'agreement_types',
                            'index_types', 'index_date', 'period', 'index_price', 'down_payment', 'description',
                            'min_weight_diff', 'max_weight_diff', 'from_step1', 'viu_step1', 'from_step2', 'to_step2',
                            'viu_step2', 'from_step3', 'to_step3', 'viu_step3', 'from_step4', 'viu_step4']
        self.fields = OrderedDict((k, self.fields[k]) for k in self.field_order)

        if not self.instance.id:
            self.fields['project_date'].initial = datetime.datetime.today().date()
            self.fields['index_date'].initial = datetime.datetime.today().date()
        self.fields['ship'].required = True

    def save(self, commit=True):
        instance = super(SellForm, self).save(commit=False)
        instance.deal_type = Deal.SELL
        instance.creator = self.http_request.user
        instance.save()
        return instance


# ********************************Bill Slip******************************************

class BillSlipForm(BaseModelForm):
    class Meta:
        model = BillSlip
        exclude = ('deal', 'amount_to_dollar')
        widgets = {
            'description': forms.Textarea(attrs={'rows': 3}),
        }

    def __init__(self, *args, **kwargs):
        self.deal_id = kwargs.pop('deal_id', None)
        super(BillSlipForm, self).__init__(*args, **kwargs)
        self.fields['date'].initial = datetime.datetime.today().date()

    def save(self, commit=True):
        instance = super(BillSlipForm, self).save(commit=False)
        instance.deal_id = self.deal_id
        date = self.cleaned_data.get('date')
        amount = self.cleaned_data.get('amount')
        unit = self.cleaned_data.get('unit')
        try:
            to_dollar = CurrencyRate.objects.get(name=unit, date=date)
        except CurrencyRate.DoesNotExist as e:
            to_dollar = CurrencyRate()
            to_dollar.rate = 1
        if unit.is_based:
            instance.amount_to_dollar = amount
        else:
            instance.amount_to_dollar = amount * to_dollar.rate
        instance.save()
        return instance


# ********************************DOCUMENT******************************************

class DocumentWizardForm(BaseForm):
    saved_document = forms.ModelChoiceField(label=_("Document"), queryset=Document.objects.all(), required=True)

    def __init__(self, *args, **kwargs):
        super(DocumentWizardForm, self).__init__(*args, **kwargs)


class DocumentDealForm(BaseForm):
    def __init__(self, *args, **kwargs):
        self.document_id = kwargs.pop('document_id', None)
        self.deal_id = kwargs.pop('deal_id', None)
        super(DocumentDealForm, self).__init__(*args, **kwargs)
        self.document_specs = {}
        self.make_document_specs(self.document_id)

    def make_document_specs(self, document_id):
        fields = DocumentSpec.objects.filter(document_id=document_id)
        self.make_product_specs_for_form(fields)

    def make_product_specs_for_form(self, fields):
        for field in fields:
            field_name = field.get_field_name()
            if field.field_type == DocumentSpec.STRING_TYPE:
                self.fields[field_name] = forms.CharField(label=field.field_label, max_length=1000,
                                                          required=field.required)
            elif field.field_type == DocumentSpec.NUMBER_TYPE:
                self.fields[field_name] = forms.IntegerField(label=field.field_label, required=field.required)
            elif field.field_type == DocumentSpec.GREGORIAN_DATE_TYPE:
                self.fields[field_name] = BootstrapDateField(label=field.field_label, required=field.required,
                                                             input_formats=['%Y/%m/%d', '%Y-%m-%d'])
            elif field.field_type == DocumentSpec.JALALI_DATE_TYPE:
                self.fields[field_name] = BootstrapJalaliDateField(label=field.field_label, required=field.required,
                                                                   input_formats=['%Y/%m/%d', '%Y-%m-%d'])
            elif field.field_type == DocumentSpec.BOOLEAN_TYPE:
                self.fields[field_name] = BootstrapBooleanField(label=field.field_label, required=field.required)
            elif field.field_type == DocumentSpec.FILE_TYPE:
                self.fields[field_name] = BootstrapFileField(label=field.field_label, required=field.required)
            self.document_specs.update({field_name: field})

    def save_extra_fields(self):
        for item in self.document_specs.keys():
            document_spec_deal = DealDocumentSpec(deal_id=self.deal_id, field_type=self.document_specs[item].field_type,
                                                  field=self.document_specs[item])
            document_spec_deal.set_value(self.cleaned_data.get(item))
            document_spec_deal.save()

    def save(self):
        self.save_extra_fields()
        deal = Deal.objects.get(id=self.deal_id)
        document = Document.objects.get(id=self.document_id)
        deal.documents.add(document)


class ProductSellForm(BaseModelForm):
    class Meta:
        model = ProductDeal
        exclude = ('deal', 'info_type', 'name', 'moisture', 'wmt', 'demurrage',)

    def __init__(self, *args, **kwargs):
        self.deal_id = kwargs.pop('deal_id', None)
        super(ProductSellForm, self).__init__(*args, **kwargs)

        try:
            deal = Deal.objects.get(id=self.deal_id)
            self.fields['premium_alfa'].initial = get_avg_index_price(deal.period, deal.index_types,
                                                                      deal.index_date, 'primum')
        except:
            pass

    def save(self, commit=True):
        instance = super(ProductSellForm, self).save(commit=False)
        instance.deal_id = self.deal_id
        instance.name = "Contract"
        instance.info_type = ProductDeal.CONTRACT
        instance.save()


class ProductPurchaseForm(BaseModelForm):
    class Meta:
        model = ProductDeal
        exclude = ('deal', 'info_type', 'name', 'volume')

    def __init__(self, *args, **kwargs):
        self.deal_id = kwargs.pop('deal_id', None)
        super(ProductPurchaseForm, self).__init__(*args, **kwargs)
        self.fields['demurrage'].required = True
        self.fields['wmt'].required = True
        self.fields['moisture'].required = True
        self.field_order = ['product', 'premium_alfa', 'premium_share', 'wmt', 'moisture', 'adjustment', 'demurrage',
                            'penalty_type']
        self.fields = OrderedDict((k, self.fields[k]) for k in self.field_order)
        try:
            deal = Deal.objects.get(id=self.deal_id)
            self.fields['premium_alfa'].initial = get_avg_index_price(deal.period, deal.index_types,
                                                                      deal.index_date, 'primum')
        except:
            pass

    def save(self, commit=True):
        instance = super(ProductPurchaseForm, self).save(commit=False)
        instance.deal_id = self.deal_id
        instance.name = "Contract"
        instance.info_type = ProductDeal.CONTRACT
        instance.save()


# ********************************CONTRACT******************************************

class ContractForm(BaseForm):
    saved_product = forms.ModelChoiceField(label=_("Product"), queryset=None, required=True)

    def __init__(self, *args, **kwargs):
        self.deal_id = kwargs.pop('deal_id', None)
        super(ContractForm, self).__init__(*args, **kwargs)
        self.fields['saved_product'].queryset = Product.objects.filter(product_deal__deal_id=self.deal_id,
                                                                       product_deal__info_type=ProductDeal.CONTRACT)


class ContractDealForm(BaseForm):
    def __init__(self, *args, **kwargs):
        self.product_id = kwargs.pop('product_id', None)
        self.deal_id = kwargs.pop('deal_id', None)
        super(ContractDealForm, self).__init__(*args, **kwargs)
        try:
            product_deal = ProductDeal.objects.get(deal_id=self.deal_id, product_id=self.product_id,
                                                   info_type=ProductDeal.CONTRACT)
            self.product_deal = product_deal.id
        except:
            raise Http404

        deal_type = product_deal.deal.deal_type
        self.product_specs = OrderedDict()
        self.product_spec_deals = {}
        self.make_product_specs(self.product_id, deal_type)

    def make_product_specs(self, product_id, deal_type):
        fields = ProductSpec.objects.filter(product_id=product_id)
        if deal_type == Deal.PURCHASE:
            fields = fields.filter(purchase=True)
        elif deal_type == Deal.SELL:
            fields = fields.filter(sell=True)
        self.make_product_specs_for_form(fields)

    def make_product_specs_for_form(self, fields):
        for field in fields:
            field_name = field.get_field_name()
            spec_id = str(field.id)
            self.fields[spec_id + '_quality'] = forms.DecimalField(label=_(field_name), max_digits=9,
                                                                   decimal_places=4, required=True)
            self.fields[spec_id + '_viu'] = forms.DecimalField(label=_('VIU'), max_digits=9, decimal_places=4,
                                                               required=True)
            self.fields[spec_id + '_step'] = forms.DecimalField(label=_('Step'), max_digits=9, decimal_places=4,
                                                                required=True)
            self.product_specs.update({field_name: field})

    def save(self):
        for key in self.product_specs.keys():
            spect_field_id = str(self.product_specs[key].id)
            quality = self.cleaned_data.get(spect_field_id + "_quality")
            viu = self.cleaned_data.get(spect_field_id + "_viu")
            step = self.cleaned_data.get(spect_field_id + "_step")
            product_spec_deal = ProductSpecDeal(deal_id=self.deal_id, product_deal_id=self.product_deal,
                                                spect_field=self.product_specs[key], quality=quality, viu=viu,
                                                step=step)
            product_spec_deal.save()


# ********************************SGS******************************************

class SGSForm(BaseForm):
    saved_product_sgs = forms.ModelChoiceField(label=_("Product"), queryset=None, required=True)

    def __init__(self, *args, **kwargs):
        self.deal_id = kwargs.pop('deal_id', None)
        super(SGSForm, self).__init__(*args, **kwargs)
        self.fields['saved_product_sgs'].queryset = Product.objects.filter(product_deal__deal_id=self.deal_id,
                                                                           product_deal__info_type=ProductDeal.CONTRACT)


class SGSDealForm(BaseForm):
    def __init__(self, *args, **kwargs):
        self.product_deal = kwargs.pop('product_deal', None)
        self.deal_id = kwargs.pop('deal_id', None)
        self.initial_data = kwargs.pop('initial_data', False)
        super(SGSDealForm, self).__init__(*args, **kwargs)

        self.product_dict = OrderedDict()
        self.make_product_fields()

    def make_product_fields(self):
        self.fields['volume'] = forms.DecimalField(label=_('Volume'), max_digits=9, decimal_places=4, required=True)
        product_fields = ['volume']
        if self.initial_data:
            self.fields['volume'].initial = self.product_deal.volume
        if self.product_deal.penalty_type == ProductDeal.PENALTY_FIXED:
            self.fields['penalty_award'] = forms.DecimalField(label=_('Penalty/Bonus'), max_digits=9, decimal_places=4,
                                                              required=True)
            product_fields.append('penalty_award')
        self.product_dict.update({'fields': product_fields})

        spec_list = []
        for deal_spec in ProductSpecDeal.objects.filter(deal_id=self.deal_id, product_deal=self.product_deal):
            field_name = deal_spec.spect_field.get_field_name()
            spec_id = str(deal_spec.spect_field.id)
            spc = {'name': field_name, 'spect_field_id': deal_spec.spect_field.id}
            self.fields[spec_id + '_quality'] = forms.DecimalField(label=_(field_name), max_digits=9, decimal_places=4,
                                                                   required=True)
            if self.initial_data:
                self.fields[spec_id + '_quality'].initial = deal_spec.quality
            spec_field = [spec_id + '_quality']
            spc.update({'fields': spec_field})
            spec_list.append(spc)
        self.product_dict.update({'spec': spec_list})

    def save(self):
        product_deal = ProductDeal(deal=self.product_deal.deal, product=self.product_deal.product,
                                   premium_alfa=self.product_deal.premium_alfa,
                                   premium_share=self.product_deal.premium_share,
                                   adjustment=self.product_deal.adjustment,
                                   penalty_type=self.product_deal.penalty_type, name="SGS", info_type=ProductDeal.SGS)
        product_deal.volume = self.cleaned_data.get("volume")
        product_deal.save()

        for deal_spec in ProductSpecDeal.objects.filter(deal_id=self.deal_id, product_deal=self.product_deal):
            quality = self.cleaned_data.get(str(deal_spec.spect_field.id) + "_quality")
            product_spec_deal = ProductSpecDeal(deal_id=deal_spec.deal_id, product_deal=product_deal,
                                                spect_field=deal_spec.spect_field, quality=quality, viu=deal_spec.viu,
                                                step=deal_spec.step)
            product_spec_deal.save()


# ********************************CIQ******************************************

class CIQForm(BaseForm):
    saved_product_ciq = forms.ModelChoiceField(label=_("Product"), queryset=None, required=True)

    def __init__(self, *args, **kwargs):
        self.deal_id = kwargs.pop('deal_id', None)
        super(CIQForm, self).__init__(*args, **kwargs)
        self.fields['saved_product_ciq'].queryset = Product.objects.filter(product_deal__deal_id=self.deal_id,
                                                                           product_deal__info_type=ProductDeal.CONTRACT)


class CIQDealForm(BaseForm):
    def __init__(self, *args, **kwargs):
        self.product_deal = kwargs.pop('product_deal', None)
        self.deal_id = kwargs.pop('deal_id', None)
        self.initial_data = kwargs.pop('initial_data', False)
        super(CIQDealForm, self).__init__(*args, **kwargs)

        self.product_dict = OrderedDict()
        self.make_product_fields()

    def make_product_fields(self):
        self.fields['volume'] = forms.DecimalField(label=_('Volume'), max_digits=9, decimal_places=4, required=True)
        product_fields = ['volume']
        if self.initial_data:
            self.fields['volume'].initial = self.product_deal.volume
        if self.product_deal.penalty_type == ProductDeal.PENALTY_FIXED:
            self.fields['penalty_award'] = forms.DecimalField(label=_('Penalty/Bonus'), max_digits=9, decimal_places=4,
                                                              required=True)
            product_fields.append('penalty_award')
        self.product_dict.update({'fields': product_fields})

        spec_list = []
        for deal_spec in ProductSpecDeal.objects.filter(deal_id=self.deal_id, product_deal=self.product_deal):
            field_name = deal_spec.spect_field.get_field_name()
            spec_id = str(deal_spec.spect_field.id)
            spc = {'name': field_name, 'spect_field_id': deal_spec.spect_field.id}
            self.fields[spec_id + '_quality'] = forms.DecimalField(label=_(field_name), max_digits=9, decimal_places=4,
                                                                   required=True)
            if self.initial_data:
                self.fields[spec_id + '_quality'].initial = deal_spec.quality
            spec_field = [spec_id + '_quality']
            spc.update({'fields': spec_field})
            spec_list.append(spc)
        self.product_dict.update({'spec': spec_list})

    def save(self):
        product_deal = ProductDeal(deal=self.product_deal.deal, product=self.product_deal.product,
                                   premium_alfa=self.product_deal.premium_alfa,
                                   premium_share=self.product_deal.premium_share,
                                   adjustment=self.product_deal.adjustment,
                                   penalty_type=self.product_deal.penalty_type, info_type=ProductDeal.CIQ)
        product_deal_count = ProductDeal.objects.filter(deal=self.product_deal.deal, product=self.product_deal.product,
                                                        info_type=ProductDeal.CIQ).count()
        product_deal.name = "CIQ-%s" % str(product_deal_count + 1)
        product_deal.volume = self.cleaned_data.get("volume")
        product_deal.save()

        for deal_spec in ProductSpecDeal.objects.filter(deal_id=self.deal_id, product_deal=self.product_deal):
            quality = self.cleaned_data.get(str(deal_spec.spect_field.id) + "_quality")
            product_spec_deal = ProductSpecDeal(deal_id=deal_spec.deal_id, product_deal=product_deal,
                                                spect_field=deal_spec.spect_field, quality=quality, viu=deal_spec.viu,
                                                step=deal_spec.step)
            product_spec_deal.save()


# ***************************************SHIP DEAL******************************************
class ShipDealForm(BaseModelForm):
    class Meta:
        model = ShipDeal
        exclude = ('deal', 'is_deleted')
        widgets = {
            'destination': forms.Textarea(attrs={'rows': 3}),
        }

    def __init__(self, *args, **kwargs):
        self.deal_id = kwargs.pop('deal_id', None)
        super(ShipDealForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        instance = super(ShipDealForm, self).save(commit=False)
        instance.deal_id = self.deal_id
        instance.save()
        return instance


# ***************************************PURCHASE******************************************
class PurchaseForm(BaseModelForm):
    class Meta:
        model = Deal
        exclude = ('deal_type', 'down_payment', 'state', 'documents', 'creator', 'created_date', 'is_deleted',)
        widgets = {
            'description': forms.Textarea(attrs={'rows': 3}),
        }

    def __init__(self, *args, **kwargs):
        super(PurchaseForm, self).__init__(*args, **kwargs)
        self.layout_general = ['vessel_name', 'agreement_no', 'agreement_types', 'project_date', 'platts', 'mine',
                               'ship', 'my_company', 'index_types', 'period', 'index_date', 'index_price',
                               'description', 'min_weight_diff', 'max_weight_diff']
        self.field_order = ['vessel_name', 'agreement_no', 'project_date', 'platts', 'mine', 'ship', 'my_company',
                            'agreement_types', 'index_types', 'period', 'index_date', 'index_price', 'description',
                            'min_weight_diff', 'max_weight_diff', 'from_step1', 'viu_step1', 'from_step2', 'to_step2',
                            'viu_step2', 'from_step3', 'to_step3', 'viu_step3', 'from_step4', 'viu_step4']
        self.fields = OrderedDict((k, self.fields[k]) for k in self.field_order)

        if not self.instance.id:
            self.fields['project_date'].initial = datetime.datetime.today().date()
            self.fields['index_date'].initial = datetime.datetime.today().date()
        self.fields['ship'].required = True
        self.fields['mine'].required = True

    def save(self, commit=True):
        instance = super(PurchaseForm, self).save(commit=False)
        instance.deal_type = Deal.PURCHASE
        instance.creator = self.http_request.user
        instance.save()
        return instance

# ***************************************COST FOR PURCHASE******************************************
class CostPurchaseForm(BaseModelForm):
    class Meta:
        model = DealPurchaseCost
        exclude = ('deal',)

    def __init__(self, *args, **kwargs):
        self.deal_id = kwargs.pop('deal_id', None)
        super(CostPurchaseForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        instance = super(CostPurchaseForm, self).save(commit=False)
        instance.deal_id = self.deal_id
        instance.save()
