from nikoo.apps.deal.models import MyCompany, Mine, Deal
from nikoo.apps.utils.forms import BaseModelForm


class MyCompanyFilterForm(BaseModelForm):
    class Meta:
        model = MyCompany
        fields = ('name',)

    def __init__(self, *args, **kwargs):
        super(MyCompanyFilterForm, self).__init__(*args, **kwargs)
        self.disable_required('name')


class MineFilterForm(BaseModelForm):
    class Meta:
        model = Mine
        fields = ('name', 'owner', 'company_name', 'email')

    def __init__(self, *args, **kwargs):
        super(MineFilterForm, self).__init__(*args, **kwargs)
        self.disable_required('name', 'owner', 'company_name', 'email')


class SellFilterForm(BaseModelForm):
    class Meta:
        model = Deal
        fields = ('agreement_no',)

    def __init__(self, *args, **kwargs):
        super(SellFilterForm, self).__init__(*args, **kwargs)
        self.disable_required('agreement_no', )
