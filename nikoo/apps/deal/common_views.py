# ********************************DOCUMENT******************************************
import json

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from nikoo.apps.base_information.models import History
from nikoo.apps.deal.form.forms import DocumentWizardForm, DocumentDealForm, ContractForm, ContractDealForm, \
    SGSDealForm, SGSForm
from nikoo.apps.deal.models import ProductDeal


@login_required
def add_document(request, deal_id):
    form = DocumentWizardForm(http_request=request)
    context = {
        'form': form,
        'title': _("Add Document to Deal"),
        "deal_id": deal_id
    }
    return render(request, "deal/partials/modal_add_item_deal.html", context=context)


@login_required
def ajax_add_document(request, deal_id, document_id):
    context = {
        'title': _("Add Document to Deal"),
        "deal_id": deal_id
    }
    if request.POST:
        form = DocumentDealForm(request.POST, request.FILES, document_id=document_id, deal_id=deal_id)
        if form.is_valid():
            form.save()
            History.action_log(user=request.user, action_type=History.ADD_DOCUMENT, deal_id=deal_id)
            response = {'status': 'success', 'msg': str(_("Document deal successfully completed."))}
            return HttpResponse(json.dumps(response), content_type="application/json")
        else:
            context['form'] = form
            return render(request, "deal/partials/_ajax_add_fields.html", context=context)
    else:
        form = DocumentDealForm(document_id=document_id, deal_id=deal_id)
    context['form'] = form
    content = render_to_string('deal/partials/_ajax_add_fields.html', context=context, request=request)
    return HttpResponse(json.dumps({'content': content}), content_type="application/json")


# ********************************CONTRACT******************************************
@login_required
def add_contract(request, deal_id):
    form = ContractForm(http_request=request, deal_id=deal_id)
    context = {
        'form': form,
        'title': _("Add contract to deal"),
        "deal_id": deal_id
    }
    return render(request, "deal/partials/modal_add_item_deal.html", context=context)


@login_required
def ajax_add_contract(request, deal_id, product_id):
    context = {
        'title': _("Add contract to deal"),
        "deal_id": deal_id
    }
    if request.POST:
        form = ContractDealForm(request.POST, request.FILES, http_request=request, product_id=product_id,
                                deal_id=deal_id)
        if form.is_valid():
            form.save()
            History.action_log(user=request.user, action_type=History.ADD_CONTRACT, deal_id=deal_id)
            response = {'status': 'success', 'msg': str(_("Contract deal successfully completed."))}
            return HttpResponse(json.dumps(response), content_type="application/json")
        else:
            context['form'] = form
            return render(request, "deal/partials/_ajax_add_fields.html", context=context)
    else:
        form = ContractDealForm(http_request=request, deal_id=deal_id, product_id=product_id)
    context['form'] = form
    content = render_to_string('deal/partials/_ajax_add_fields.html', context=context, request=request)
    return HttpResponse(json.dumps({'content': content}), content_type="application/json")


# ********************************SGS******************************************
@login_required
def add_sgs(request, deal_id):
    form = SGSForm(http_request=request, deal_id=deal_id)
    context = {
        'form': form,
        'title': _("Add SGS to Deal"),
        "deal_id": deal_id
    }
    return render(request, "deal/partials/modal_add_item_deal.html", context=context)


@login_required
def ajax_add_sgs(request, deal_id, product_id):
    try:
        product_deal = ProductDeal.objects.get(deal_id=deal_id, product_id=product_id, info_type=ProductDeal.CONTRACT)
    except:
        return HttpResponseRedirect(reverse('permission_401'))
    context = {
        'title': _("Add SGS to deal"),
        "deal_id": deal_id
    }
    if request.POST:
        form = SGSDealForm(request.POST, request.FILES, http_request=request, deal_id=deal_id,
                           product_deal=product_deal)
        if form.is_valid():
            form.save()
            History.action_log(user=request.user, action_type=History.ADD_SGS, deal_id=deal_id)
            response = {'status': 'success', 'msg': str(_("SGS deal successfully completed."))}
            return HttpResponse(json.dumps(response), content_type="application/json")
        else:
            context['form'] = form
            return render(request, "deal/partials/_ajax_add_fields_SGS_CIQ.html", context=context)
    else:
        form = SGSDealForm(http_request=request, deal_id=deal_id, product_deal=product_deal, initial_data=True)
    context['form'] = form
    content = render_to_string('deal/partials/_ajax_add_fields_SGS_CIQ.html', context=context, request=request)
    return HttpResponse(json.dumps({'content': content}), content_type="application/json")
