import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from nikoo.apps.base_information.models import History
from nikoo.apps.deal.form.forms import SellForm, BillSlipForm, ProductSellForm, CIQForm, CIQDealForm, ShipDealForm
from nikoo.apps.deal.models import Deal, ProductDeal, ShipDeal
from nikoo.apps.utils import BtnClass
from nikoo.apps.utils.manager.sitemap import SiteMap


@login_required
def add_sell(request):
    if request.method == 'POST':
        form = SellForm(request.POST, http_request=request)
        if form.is_valid():
            purchase_form = form.save()
            messages.success(request, _("Data saved successfully"))
            return HttpResponseRedirect(reverse('sell_view', kwargs={'deal_id': purchase_form.id}))
    else:
        form = SellForm(http_request=request)
    context = {
        'form': form,
        'title': _("New sell contract"),
        'jalali_date_picker': False
    }
    context['sitemap_items'] = [SiteMap(context['title'], "")]
    return render(request, "deal/sell/add_edit_sell.html", context=context)


@login_required
def edit_sell(request, deal_id):
    try:
        instance = Deal.objects.get(pk=deal_id, deal_type=Deal.SELL)
    except:
        return HttpResponseRedirect(reverse('page_404'))

    if request.method == 'POST':
        form = SellForm(request.POST, request.FILES, http_request=request, instance=instance)
        if form.is_valid():
            purchase_form = form.save()
            messages.success(request, _("Data saved successfully"))
            return HttpResponseRedirect(reverse('sell_view', kwargs={'deal_id': purchase_form.id}))
    else:
        form = SellForm(http_request=request, instance=instance)
    context = {
        'form': form,
        'title': _("Edit sell"),
        'jalali_date_picker': False
    }
    context['sitemap_items'] = [SiteMap(context['title'], "")]
    return render(request, "deal/sell/add_edit_sell.html", context=context)


@login_required
def sell_cancel(request, deal_id):
    try:
        instance = Deal.objects.get(pk=deal_id, deal_type=Deal.SELL)
    except:
        return HttpResponseRedirect(reverse('page_404'))
    instance.delete()
    messages.success(request, _("The contract was canceled successfully."))
    return HttpResponseRedirect(reverse('process_main_page_sell_working'))


@login_required
def sell_view(request, deal_id):
    try:
        instance = Deal.objects.get(pk=deal_id, deal_type=Deal.SELL)
    except:
        raise Http404(_('There is an error to render Purchase View page'))

    # documents = Deal.get_documents(deal_id=id)
    # invoices = Invoice.get_invoices_by_type(deal_id=id)
    primary_btn = generate_buttons(request.user, instance)

    context = {'title': _("View sell"), 'instance': instance, "primary_btn": primary_btn}
    context['sitemap_items'] = [SiteMap(context['title'], "")]
    return render(request, 'deal/sell/view_details_sell.html', context=context)


# ********************************Bill Slip******************************************
@login_required
def add_bill_slip(request, deal_id):
    if request.POST:
        form = BillSlipForm(request.POST, http_request=request, deal_id=deal_id)
        if form.is_valid():
            form.save()
            History.action_log(user=request.user, action_type=History.ADD_BILL, deal_id=deal_id)
            response = {'status': 'success', 'msg': str(_("Bill Slip was generated Successfully."))}
            return HttpResponse(json.dumps(response), content_type="application/json")
    else:
        form = BillSlipForm(http_request=request, deal_id=deal_id)
    context = {
        'form': form,
        'title': _("Add Bill Slip To Deal"),
    }
    return render(request, "deal/partials/modal_add_item_deal.html", context=context)


# ********************************PRODUCT******************************************
@login_required
def add_product_sell(request, deal_id):
    if request.POST:
        form = ProductSellForm(request.POST, http_request=request, deal_id=deal_id)
        if form.is_valid():
            form.save()
            History.action_log(user=request.user, action_type=History.ADD_PRODUCT, deal_id=deal_id)
            response = {'status': 'success', 'msg': str(_("Product added Successfully."))}
            return HttpResponse(json.dumps(response), content_type="application/json")
    else:
        form = ProductSellForm(http_request=request, deal_id=deal_id)
    context = {
        'form': form,
        'title': _("Add Product To Deal"),
    }
    return render(request, "deal/partials/modal_add_item_deal.html", context=context)


# ********************************CIQ******************************************
@login_required
def add_ciq(request, deal_id):
    form = CIQForm(http_request=request, deal_id=deal_id)
    context = {
        'form': form,
        'title': _("Add CIQ to Deal"),
        "deal_id": deal_id
    }
    return render(request, "deal/partials/modal_add_item_deal.html", context=context)


@login_required
def ajax_add_ciq(request, deal_id, product_id):
    try:
        product_deal = ProductDeal.objects.get(deal_id=deal_id, product_id=product_id, info_type=ProductDeal.CONTRACT)
    except:
        return HttpResponseRedirect(reverse('permission_401'))
    context = {
        'title': _("Add CIQ to Deal"),
        "deal_id": deal_id
    }
    if request.POST:
        form = CIQDealForm(request.POST, request.FILES, http_request=request, deal_id=deal_id,
                           product_deal=product_deal)
        if form.is_valid():
            form.save()
            History.action_log(user=request.user, action_type=History.ADD_CIQ, deal_id=deal_id)
            response = {'status': 'success', 'msg': str(_("CIQ deal successfully completed."))}
            return HttpResponse(json.dumps(response), content_type="application/json")
        else:
            context['form'] = form
            return render(request, "deal/partials/_ajax_add_fields_SGS_CIQ.html", context=context)
    else:
        form = CIQDealForm(http_request=request, deal_id=deal_id, product_deal=product_deal, initial_data=True)
    context['form'] = form
    content = render_to_string('deal/partials/_ajax_add_fields_SGS_CIQ.html', context=context, request=request)
    return HttpResponse(json.dumps({'content': content}), content_type="application/json")


@login_required
def ship_info(request, deal_id):
    try:
        instance = ShipDeal.objects.get(deal_id=deal_id)
    except:
        instance = None

    if request.method == 'POST':
        form = ShipDealForm(request.POST, request.FILES, http_request=request, instance=instance, deal_id=deal_id)
        if form.is_valid():
            form.save()
            action_type = History.ADD_SHIP_INFO if instance else History.EDIT_SHIP_INFO
            History.action_log(user=request.user, action_type=action_type, deal_id=deal_id)
            response = {'status': 'success', 'msg': str(_("Ship info updated."))}
            return HttpResponse(json.dumps(response), content_type="application/json")
    else:
        form = ShipDealForm(http_request=request, instance=instance, deal_id=deal_id)
    context = {
        'form': form,
        'title': _("Edit ship info") if instance else _("Add ship info"),
        'jalali_date_picker': False
    }
    context['sitemap_items'] = [SiteMap(context['title'], "")]
    return render(request, "deal/partials/modal_add_item_deal.html", context=context)


def generate_buttons(user, deal):
    primary_btn = []

    if deal.deal_type == Deal.PURCHASE:
        edit_url = reverse('purchase_edit', kwargs={'deal_id': deal.id})
    elif deal.deal_type == Deal.SELL:
        edit_url = reverse('sell_edit', kwargs={'deal_id': deal.id})
    else:
        edit_url = None
    if edit_url:
        primary_btn.append(
            BtnClass(name="edit", title=_("Edit"), url_address=edit_url, btn_type="btn-warning", is_popup=False,
                     new_tab=False, is_view=True, icon="fa-pencil"))
    if deal.deal_type == Deal.PURCHASE:
        cancel_url = reverse('purchase_cancel', kwargs={'deal_id': deal.id})
    elif deal.deal_type == Deal.SELL:
        cancel_url = reverse('sell_cancel', kwargs={'deal_id': deal.id})
    else:
        cancel_url = None
    if cancel_url:
        primary_btn.append(
            BtnClass(name="Cancel", title=_("Cancel contract"), url_address=cancel_url, btn_type="btn-danger",
                     is_popup=False, new_tab=False, is_view=True, icon="fa-close"))

    add_bill_slip_url = reverse('add_bill_slip', kwargs={'deal_id': deal.id})
    primary_btn.append(
        BtnClass(name="bill", title=_("Add Bill slip"), url_address=add_bill_slip_url, btn_type="btn-primary",
                 is_popup=True, new_tab=False, is_view=True, icon="fa-dollar"))

    add_document_url = reverse('add_document_deal', kwargs={'deal_id': deal.id})
    primary_btn.append(
        BtnClass(name="doc", title=_("Add Document"), url_address=add_document_url, btn_type="btn-primary",
                 is_popup=True, new_tab=False, is_view=True, icon="fa-file"))

    add_product_url = reverse('add_product_sell', kwargs={'deal_id': deal.id})
    primary_btn.append(
        BtnClass(name="product", title=_("Add product"), url_address=add_product_url, btn_type="btn-primary",
                 is_popup=True, new_tab=False, is_view=True, icon="fa-recycle"))

    if deal.deal_type == Deal.PURCHASE:
        invoice_url = reverse('purchase_invoice', kwargs={'deal_id': deal.id})
    elif deal.deal_type == Deal.SELL:
        invoice_url = reverse('sell_invoice', kwargs={'deal_id': deal.id})
    else:
        invoice_url = None
    if invoice_url:
        primary_btn.append(
            BtnClass(name="add", title=_("Invoice"), url_address=invoice_url, btn_type="btn-primary", is_popup=False,
                     new_tab=False, is_view=True, icon="fa-print"))

    add_contract_url = reverse('add_contract_product_deal', kwargs={'deal_id': deal.id})
    primary_btn.append(
        BtnClass(name="Contract", title=_("Add contract"), url_address=add_contract_url, btn_type="btn-primary",
                 is_popup=True,
                 new_tab=False, is_view=True, icon="fa-dollar"))

    add_sgs_url = reverse('add_sgs_product_deal', kwargs={'deal_id': deal.id})
    primary_btn.append(
        BtnClass(name="Sgs", title=_("Add Sgs"), url_address=add_sgs_url, btn_type="btn-primary",
                 is_popup=True, new_tab=False, is_view=True, icon="fa-dollar"))

    add_ciq_url = reverse('add_ciq_product_deal', kwargs={'deal_id': deal.id})
    primary_btn.append(
        BtnClass(name="Ciq", title=_("Add Ciq"), url_address=add_ciq_url, btn_type="btn-primary",
                 is_popup=True, new_tab=False, is_view=True, icon="fa-dollar"))

    add_ship_info_url = reverse('sell_ship', kwargs={'deal_id': deal.id})
    primary_btn.append(
        BtnClass(name="ship", title=_("Ship info"), url_address=add_ship_info_url, btn_type="btn-primary",
                 is_popup=True, new_tab=False, is_view=True, icon="fa-dollar"))
    return primary_btn
