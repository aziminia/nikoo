from nikoo.apps.utils.manager.action import ManagerAction


class EditSell(ManagerAction):
    new_url = '/deal/sell_edit/'
    new_tab = True
    icon = 'fa-pencil'
    button_class = "btn-warning"


class ViewDetailsSell(ManagerAction):
    new_url = '/deal/sell_view/'
    new_tab = True
    icon = 'fa-th-list'

class EditPurchase(ManagerAction):
    new_url = '/deal/purchase_edit/'
    new_tab = True
    icon = 'fa-pencil'
    button_class = "btn-warning"


class ViewDetailsPurchase(ManagerAction):
    new_url = '/deal/purchase_view/'
    new_tab = True
    icon = 'fa-th-list'
