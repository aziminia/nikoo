from django.conf.urls import url, include

from nikoo.apps.deal.common_views import add_document, ajax_add_document, add_contract, ajax_add_contract, add_sgs, \
    ajax_add_sgs
from nikoo.apps.deal.purchase_views import *
from nikoo.apps.deal.views import *

urlpatterns = [
    url(r'^invoice/', include('nikoo.apps.invoice.urls')),

    url(r'^add_bill_slip/(?P<deal_id>\w+)/$', add_bill_slip, name='add_bill_slip'),

    url(r'^add_product_sell/(?P<deal_id>\d+)/$', add_product_sell, name='add_product_sell'),

    # url(r'^add_contract/(?P<deal_id>\d+)/$', add_contract, name='add_contract_product_deal'),
    # url(r'^ajax_add_contract/(?P<deal_id>\d+)/(?P<product_id>\d+)/$', ajax_add_contract, name='add_contract_deal_ajax'),



    url(r'^add_ciq/(?P<deal_id>\d+)/$', add_ciq, name='add_ciq_product_deal'),
    url(r'^ajax_add_ciq/(?P<deal_id>\d+)/(?P<product_id>\d+)/$', ajax_add_ciq, name='ajax_add_ciq'),

    # sell
    url(r'^add_sell/$', add_sell, name='add_sell'),
    url(r'^sell_edit/(?P<deal_id>\w+)/$', edit_sell, name='sell_edit'),
    url(r'^sell_view/(?P<deal_id>\w+)/$', sell_view, name='sell_view'),
    url(r'^sell_cancel/(?P<deal_id>\w+)/$', sell_cancel, name='sell_cancel'),

    url(r'^ship/(?P<deal_id>\w+)/$', ship_info, name='sell_ship'),

    # purchase
    url(r'^add_purchase/$', add_purchase, name='add_purchase'),
    url(r'^purchase_view/(?P<deal_id>\w+)/$', purchase_view, name='purchase_view'),
    url(r'^purchase_cancel/(?P<deal_id>\w+)/$', purchase_cancel, name='purchase_cancel'),
    url(r'^purchase_edit/(?P<deal_id>\w+)/$', edit_purchase, name='purchase_edit'),
    url(r'^add_product_purchase/(?P<deal_id>\d+)/$', add_product_purchase, name='add_product_purchase'),
    url(r'^add_cost_purchase/(?P<deal_id>\d+)/$', add_cost_purchase, name='add_cost_purchase'),

    # common
    url(r'^add_document/(?P<deal_id>\d+)/$', add_document, name='add_document_deal'),
    url(r'^ajax_add_document/(?P<deal_id>\d+)/(?P<document_id>\d+)/$', ajax_add_document,
        name='add_document_deal_ajax'),
    url(r'^add_contract/(?P<deal_id>\d+)/$', add_contract, name='add_contract_product_deal'),
    url(r'^ajax_add_contract/(?P<deal_id>\d+)/(?P<product_id>\d+)/$', ajax_add_contract, name='ajax_add_contract'),
    url(r'^add_sgs/(?P<deal_id>\d+)/$', add_sgs, name='add_sgs_product_deal'),
    url(r'^ajax_add_sgs/(?P<deal_id>\d+)/(?P<product_id>\d+)/$', ajax_add_sgs, name='ajax_add_sgs'),
]
