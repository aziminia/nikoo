import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from nikoo.apps.base_information.models import History
from nikoo.apps.deal.form.forms import PurchaseForm, ProductPurchaseForm, CostPurchaseForm
from nikoo.apps.deal.models import Deal
from nikoo.apps.utils import BtnClass
from nikoo.apps.utils.manager.sitemap import SiteMap


@login_required
def add_purchase(request):
    if request.method == 'POST':
        form = PurchaseForm(request.POST, http_request=request)
        if form.is_valid():
            purchase_form = form.save()
            messages.success(request, _("Purchase information saved successfully."))
            return HttpResponseRedirect(reverse('purchase_view', kwargs={'deal_id': purchase_form.id}))
    else:
        form = PurchaseForm(http_request=request)
    context = {
        'form': form,
        'title': _("New purchase"),
        'jalali_date_picker': False
    }
    context['sitemap_items'] = [SiteMap(context['title'], "")]
    return render(request, "deal/purchase/add_edit_purchase.html", context=context)


@login_required
def purchase_view(request, deal_id):
    try:
        instance = Deal.objects.get(pk=deal_id, deal_type=Deal.PURCHASE)
    except:
        raise Http404(_('There is an error to render Purchase View page'))

    primary_btn = generate_buttons_purchase(request.user, instance)

    context = {'title': _("View purchase"), 'instance': instance, "primary_btn": primary_btn}
    context['sitemap_items'] = [SiteMap(context['title'], "")]
    return render(request, 'deal/purchase/view_details_purchase.html', context=context)


@login_required
def edit_purchase(request, deal_id):
    try:
        instance = Deal.objects.get(pk=deal_id, deal_type=Deal.PURCHASE)
    except:
        return HttpResponseRedirect(reverse('page_404'))

    if request.method == 'POST':
        form = PurchaseForm(request.POST, http_request=request, instance=instance)
        if form.is_valid():
            purchase_form = form.save()
            messages.success(request, _("Data saved successfully"))
            return HttpResponseRedirect(reverse('purchase_view', kwargs={'deal_id': purchase_form.id}))
    else:
        form = PurchaseForm(http_request=request, instance=instance)
    context = {
        'form': form,
        'title': _("Edit purchase"),
        'jalali_date_picker': False
    }
    context['sitemap_items'] = [SiteMap(context['title'], "")]
    return render(request, "deal/purchase/add_edit_purchase.html", context=context)


@login_required
def purchase_cancel(request, deal_id):
    try:
        instance = Deal.objects.get(pk=deal_id, deal_type=Deal.PURCHASE)
    except:
        return HttpResponseRedirect(reverse('page_404'))
    instance.delete()
    messages.success(request, _("The contract was canceled successfully."))
    return HttpResponseRedirect(reverse('process_main_page_purchase_working'))


# ********************************PRODUCT******************************************
@login_required
def add_product_purchase(request, deal_id):
    if request.POST:
        form = ProductPurchaseForm(request.POST, http_request=request, deal_id=deal_id)
        if form.is_valid():
            form.save()
            History.action_log(user=request.user, action_type=History.ADD_PRODUCT, deal_id=deal_id)
            response = {'status': 'success', 'msg': str(_("Product added Successfully."))}
            return HttpResponse(json.dumps(response), content_type="application/json")
    else:
        form = ProductPurchaseForm(http_request=request, deal_id=deal_id)
    context = {
        'form': form,
        'title': _("Add product to deal"),
    }
    return render(request, "deal/partials/modal_add_item_deal.html", context=context)


# ********************************COST******************************************
@login_required
def add_cost_purchase(request, deal_id):
    if request.POST:
        form = CostPurchaseForm(request.POST, http_request=request, deal_id=deal_id)
        if form.is_valid():
            form.save()
            response = {'status': 'success', 'msg': str(_("cost added Successfully."))}
            return HttpResponse(json.dumps(response), content_type="application/json")
    else:
        form = CostPurchaseForm(http_request=request, deal_id=deal_id)
    context = {
        'form': form,
        'title': _("Add cost to purchase"),
    }
    return render(request, "deal/partials/modal_add_item_deal.html", context=context)


def generate_buttons_purchase(user, deal):
    primary_btn = []
    if deal.deal_type == Deal.PURCHASE:
        edit_url = reverse('purchase_edit', kwargs={'deal_id': deal.id})
    elif deal.deal_type == Deal.SELL:
        edit_url = reverse('sell_edit', kwargs={'deal_id': deal.id})
    else:
        edit_url = None
    if edit_url:
        primary_btn.append(
            BtnClass(name="edit", title=_("Edit"), url_address=edit_url, btn_type="btn-warning", is_popup=False,
                     new_tab=False, is_view=True, icon="fa-pencil"))

    if deal.deal_type == Deal.PURCHASE:
        cancel_url = reverse('purchase_cancel', kwargs={'deal_id': deal.id})
    elif deal.deal_type == Deal.SELL:
        cancel_url = reverse('sell_cancel', kwargs={'deal_id': deal.id})
    else:
        cancel_url = None
    if cancel_url:
        primary_btn.append(
            BtnClass(name="Cancel", title=_("Cancel contract"), url_address=cancel_url, btn_type="btn-danger",
                     is_popup=False, new_tab=False, is_view=True, icon="fa-close"))

    add_document_url = reverse('add_document_deal', kwargs={'deal_id': deal.id})
    primary_btn.append(
        BtnClass(name="doc", title=_("Add Document"), url_address=add_document_url, btn_type="btn-primary",
                 is_popup=True, new_tab=False, is_view=True, icon="fa-file"))

    if deal.deal_type == Deal.PURCHASE:
        add_product_url = reverse('add_product_purchase', kwargs={'deal_id': deal.id})
    elif deal.deal_type == Deal.SELL:
        add_product_url = reverse('add_product_sell', kwargs={'deal_id': deal.id})
    else:
        add_product_url = None
    if add_product_url:
        primary_btn.append(
            BtnClass(name="product", title=_("Add product"), url_address=add_product_url, btn_type="btn-primary",
                     is_popup=True, new_tab=False, is_view=True, icon="fa-recycle"))

    add_contract_url = reverse('add_contract_product_deal', kwargs={'deal_id': deal.id})
    primary_btn.append(
        BtnClass(name="Contract", title=_("Add contract"), url_address=add_contract_url, btn_type="btn-primary",
                 is_popup=True, new_tab=False, is_view=True, icon="fa-dollar"))

    add_sgs_url = reverse('add_sgs_product_deal', kwargs={'deal_id': deal.id})
    primary_btn.append(
        BtnClass(name="Sgs", title=_("Add Sgs"), url_address=add_sgs_url, btn_type="btn-primary",
                 is_popup=True, new_tab=False, is_view=True, icon="fa-dollar"))

    add_cost_url = reverse('add_cost_purchase', kwargs={'deal_id': deal.id})
    primary_btn.append(
        BtnClass(name="cost", title=_("Add cost"), url_address=add_cost_url, btn_type="btn-primary",
                 is_popup=True, new_tab=False, is_view=True, icon="fa-dollar"))

    invoice_url = reverse('purchase_invoice', kwargs={'deal_id': deal.id})
    if invoice_url:
        primary_btn.append(
            BtnClass(name="add", title=_("Invoice"), url_address=invoice_url, btn_type="btn-primary", is_popup=False,
                     new_tab=False, is_view=True, icon="fa-print"))

    return primary_btn
