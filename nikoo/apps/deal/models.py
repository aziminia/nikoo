from django.db import models
from django.utils.translation import gettext_lazy as _

from nikoo.apps.base_information.models import DocumentSpec
from nikoo.apps.utils.file import bill_slip_upload_to, deal_file_upload_to
from nikoo.apps.utils.models import LogicalDeletedManager


class Deal(models.Model):
    PURCHASE = 1
    SELL = 2
    DEAL_TYPE = (
        (PURCHASE, _("deal")),
        (SELL, _("Sell")),
    )

    WORK = 1
    FOT = 2
    FOB = 3
    CFR = 4
    AGREEMENT_TYPE_CHOICE = (
        (WORK, _("ExWORK")),
        (FOT, _("FOT")),
        (FOB, _("FOB")),
        (CFR, _("CFR")),
    )

    PLATTS = 1
    UMETAL = 2
    FIXED = 3
    INDEX_TYPES_CHOICE = (
        (PLATTS, _("PLATTS")),
        (UMETAL, _("UMETAL")),
        (FIXED, _("FIXED")),
    )

    IN_PROGRESS = 1
    FINISHED = 2
    DRAFT = 3
    CANCELED = 4
    ARCHIVE = 5
    STATE_CHOICES = (
        (IN_PROGRESS, _("In progress")),
        (FINISHED, _("Completed")),
        (DRAFT, _("Draft")),
        (CANCELED, _("Canceled")),
        (ARCHIVE, _("Archive")),
    )

    PERIOD_CHOICES = ((0, 0), (1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10),)

    deal_type = models.IntegerField(_("Deal type"), choices=DEAL_TYPE, default=PURCHASE)
    agreement_no = models.CharField(_("Agreement number"), max_length=50)
    vessel_name = models.CharField(_("Vessel name"), max_length=100)
    agreement_types = models.IntegerField(_("Agreement types"), choices=AGREEMENT_TYPE_CHOICE, default=WORK)
    project_date = models.DateField(_("Project date"))
    index_types = models.ForeignKey('base_information.Reference', verbose_name=_("Index types"),
                                    related_name='deal_reference')
    period = models.IntegerField(_("Period"), choices=PERIOD_CHOICES, default=2)
    index_date = models.DateField(_("Index Date"))
    index_price = models.DecimalField(_("Index price"), max_digits=9, decimal_places=4, default=0)
    down_payment = models.DecimalField(_("Down Payment"), max_digits=15, decimal_places=4, default=0)
    description = models.TextField(_("Description"), null=True, blank=True)
    ship = models.ForeignKey('base_information.Ship', verbose_name=_("Ship name"), null=True, blank=True)
    my_company = models.ForeignKey('MyCompany', verbose_name=_("My Company"))
    mine = models.ForeignKey('Mine', verbose_name=_("Mine"), null=True, blank=True)
    state = models.IntegerField(_("Status"), choices=STATE_CHOICES, default=IN_PROGRESS)
    documents = models.ManyToManyField('base_information.Document', blank=True)
    from_step1 = models.DecimalField(_("Greater than"), max_digits=9, decimal_places=4, default=0)
    viu_step1 = models.DecimalField(_("Viu"), max_digits=9, decimal_places=4, default=0)
    from_step2 = models.DecimalField(_("From"), max_digits=9, decimal_places=4, default=0)
    to_step2 = models.DecimalField(_("To"), max_digits=9, decimal_places=4, default=0)
    viu_step2 = models.DecimalField(_("Viu"), max_digits=9, decimal_places=4, default=0)
    from_step3 = models.DecimalField(_("From"), max_digits=9, decimal_places=4, default=0)
    to_step3 = models.DecimalField(_("To"), max_digits=9, decimal_places=4, default=0)
    viu_step3 = models.DecimalField(_("Viu"), max_digits=9, decimal_places=4, default=0)
    from_step4 = models.DecimalField(_("Less than"), max_digits=9, decimal_places=4, default=0)
    viu_step4 = models.DecimalField(_("Viu"), max_digits=9, decimal_places=4, default=0)
    min_weight_diff = models.DecimalField(_("Min weight difference"), max_digits=9, decimal_places=4, default=0)
    max_weight_diff = models.DecimalField(_("Max weight difference"), max_digits=9, decimal_places=4, default=0)
    platts = models.DecimalField(_("Platts contract"), max_digits=9, decimal_places=4, default=0)
    creator = models.ForeignKey('accounts.User', verbose_name=_("Creator"), related_name="deal_creator")
    created_date = models.DateTimeField(_("Project created date"), auto_now_add=True)
    is_deleted = models.BooleanField(_("Deleted"), default=False)
    objects = LogicalDeletedManager()

    def delete(self, using=None, keep_parents=False):
        self.is_deleted = True
        self.save()

    def __str__(self):
        return "%s - %s" % (self.vessel_name, self.agreement_no)

    # def get_bill_total(self):
    #     bill_slip = BillSlip.objects.filter(deal=self)
    #     total = 0
    #     for bill in bill_slip:
    #         total += bill.amount_to_dollar
    #     return total
    #
    # def get_residual(self):
    #     return self.total_price - (self.down_payment + self.get_bill_total())
    #
    # @staticmethod
    # def get_documents(deal_id):
    #     documents = DealDocumentSpec.objects.filter(deal_id=deal_id)
    #     result_list = dict()
    #     for document in documents:
    #         doc = str(document.field.document.name)
    #         if doc not in result_list:
    #             result_list[doc] = []
    #         result_list[doc].append(document)
    #     return result_list


class MyCompany(models.Model):
    name = models.CharField(_("Company name"), max_length=50, null=True, blank=True)
    address = models.TextField(_("Company address"), max_length=150, null=True, blank=True)
    phone_number = models.CharField(_("Company phone number"), max_length=15, null=True, blank=True)
    cell_phone_number = models.CharField(_("Cell phone number"), max_length=15, null=True, blank=True)
    email = models.EmailField(_("Email address"))
    is_deleted = models.BooleanField(_("Deleted"), default=False)
    objects = LogicalDeletedManager()

    def delete(self, using=None, keep_parents=False):
        self.is_deleted = True
        self.save()

    def __str__(self):
        return self.name


class Mine(models.Model):
    name = models.CharField(_("Mine"), max_length=50)
    owner = models.CharField(_("Mine owner"), max_length=50)
    company_name = models.CharField(_("Company name"), max_length=50, null=True, blank=True)
    company_address = models.TextField(_("Company address"), max_length=150, null=True, blank=True)
    company_phone_number = models.CharField(_("Company phone number"), max_length=15, null=True, blank=True)
    cell_phone_number = models.CharField(_("Cell phone number"), max_length=15, null=True, blank=True)
    email = models.EmailField(_("Email address"))
    location = models.TextField(_("Mine location"), null=True, blank=True)
    is_deleted = models.BooleanField(_("Deleted"), default=False)
    objects = LogicalDeletedManager()

    def delete(self, using=None, keep_parents=False):
        self.is_deleted = True
        self.save()

    def __str__(self):
        return self.name


class ShipDeal(models.Model):
    deal = models.OneToOneField('Deal', verbose_name=_('Deal'), related_name='ship_deal')
    destination = models.TextField(_("Destination port"))
    capacity = models.DecimalField(_("Capacity"), max_digits=9, decimal_places=4)
    cost = models.DecimalField(_("Cost per tone"), max_digits=9, decimal_places=4)
    deprecate_date = models.DateField(_("Departure date"))
    laycan_start = models.DateField(_("Ship laycan Start"))
    laycan_end = models.DateField(_("Ship laycan End"))
    bl_date = models.DateField(_("BL Date"))
    loading_start_date = models.DateField(_("Ship Loading Start date"))
    loading_rate = models.IntegerField(_("Loading rate"))
    discharge_rate = models.IntegerField(_("Discharge rate"))
    agent_name = models.CharField(_("Agent name"), max_length=50)
    agent_phone_number = models.CharField(_("Agent phone number"), max_length=15)
    is_deleted = models.BooleanField(_("Deleted"), default=False)
    objects = LogicalDeletedManager()

    def delete(self, using=None, keep_parents=False):
        self.is_deleted = True
        self.save()

    def __str__(self):
        return self.deal.ship.name


class BillSlip(models.Model):
    name = models.CharField(_("Bill name"), max_length=100)
    date = models.DateField(_("Date"))
    unit = models.ForeignKey('base_information.Currency', verbose_name=_('Currency'))
    deal = models.ForeignKey('Deal', verbose_name=_('Deal'), related_name='bill_slip')
    amount = models.DecimalField(_("Amount"), max_digits=15, decimal_places=4, default=0)
    amount_to_dollar = models.DecimalField(_("Amount In Dollar"), max_digits=15, decimal_places=4, default=0)
    description = models.TextField(_("Description"), null=True, blank=True)
    bank_slip_file = models.FileField(_("Bill Slip"), upload_to=bill_slip_upload_to, null=True, blank=True)

    def get_name(self):
        return "%s" % self.name


class DealDocumentSpec(models.Model):
    deal = models.ForeignKey('Deal', verbose_name=_(u"Deal"), related_name="document_spec_deal")
    field_type = models.IntegerField(_("Type"), choices=DocumentSpec.FIELD_TYPES)
    field = models.ForeignKey('base_information.DocumentSpec', verbose_name=_(u"Field Name"),
                              related_name="document_extra_fields")
    file = models.FileField(_("File"), upload_to=deal_file_upload_to, null=True, blank=True)
    value = models.TextField('Value', null=True, blank=True)

    def set_value(self, value):
        if self.field_type == DocumentSpec.FILE_TYPE:
            self.file = value
        else:
            self.value = value


class ProductDeal(models.Model):
    PENALTY_DEFAULT = 1
    PENALTY_STAIRS = 2
    PENALTY_FIXED = 3
    PENALTY_CHOICES = (
        (PENALTY_DEFAULT, _("Default")),
        (PENALTY_STAIRS, _("STAIRS")),
        (PENALTY_FIXED, _("FIXED")),
    )

    CONTRACT = 1
    SGS = 2
    CIQ = 3
    INFO_CHOICES = (
        (CONTRACT, _("Contract")),
        (SGS, _("Sgs")),
        (CIQ, _("Ciq")),
    )
    deal = models.ForeignKey('Deal', verbose_name=_(u"Deal"), related_name="product_deal")
    product = models.ForeignKey('base_information.Product', verbose_name=_(u"Product name"),
                                related_name="product_deal")
    premium_alfa = models.DecimalField(_("Alfa"), max_digits=9, decimal_places=4, default=0)
    premium_share = models.DecimalField(_("Share %"), max_digits=9, decimal_places=4, default=0)
    volume = models.DecimalField(_("Volume"), max_digits=9, decimal_places=4, default=0)
    moisture = models.DecimalField(_("Moisture(%)"), max_digits=9, decimal_places=4, default=0, null=True, blank=True)
    wmt = models.DecimalField(_("WMT"), max_digits=9, decimal_places=4, default=0, null=True, blank=True)
    demurrage = models.DecimalField(_("Demurrage"), max_digits=9, decimal_places=4, default=0, null=True, blank=True)
    adjustment = models.DecimalField(_("Adjustment"), max_digits=9, decimal_places=4, default=0)
    penalty_type = models.IntegerField(_("Penalty Type"), choices=PENALTY_CHOICES, default=PENALTY_DEFAULT)
    name = models.CharField(_("Name"), max_length=100)
    info_type = models.IntegerField(_("Type"), choices=INFO_CHOICES, default=CONTRACT)

    def __str__(self):
        return self.product

    def get_info_type(self):
        if self.info_type == self.CONTRACT:
            return "Contract"
        elif self.info_type == self.SGS:
            return "SGS"
        elif self.info_type == self.CIQ:
            return self.name
        return ""


class ProductSpecDeal(models.Model):
    deal = models.ForeignKey('Deal', verbose_name=_(u"Deal"), related_name="deal_product_spec")
    product_deal = models.ForeignKey('ProductDeal', verbose_name=_(u"Product Deal"), related_name="deal_product_spec")
    spect_field = models.ForeignKey('base_information.ProductSpec', verbose_name=_(u"field name"),
                                    related_name="deal_product_spec")
    quality = models.DecimalField(_("Quality"), max_digits=9, decimal_places=4)
    viu = models.DecimalField(_("viu"), max_digits=9, decimal_places=4)
    step = models.DecimalField(_("step"), max_digits=9, decimal_places=4, default=1)

    def get_value(self, field):
        if field:
            return getattr(self, field)

    def set_value(self, field, value):
        if value:
            setattr(self, field, value)

    def __str__(self):
        return "%s %s" % (self.deal, self.spect_field)


class DealPurchaseCost(models.Model):
    deal = models.ForeignKey('Deal', verbose_name=_('Deal'), related_name='cost_purchase_deal')
    cost_name = models.ForeignKey('base_information.Cost', verbose_name=_("Cost Name"), related_name="cost_purchase_deal")
    price = models.DecimalField(_("Price"), max_digits=12, decimal_places=4)

    def __str__(self):
        return self.cost_name
