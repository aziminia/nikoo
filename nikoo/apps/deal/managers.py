from django.utils.translation import gettext_lazy as _

from nikoo.apps.deal.actions import EditSell, ViewDetailsSell, ViewDetailsPurchase, EditPurchase
from nikoo.apps.deal.form.filter_forms import MyCompanyFilterForm, MineFilterForm, SellFilterForm
from nikoo.apps.deal.form.forms import MyCompanyForm, MineForm
from nikoo.apps.deal.models import MyCompany, Mine, Deal
from nikoo.apps.utils.manager.action import EditAction, DeleteAction, AddAction
from nikoo.apps.utils.manager.main import ObjectManager, ColumnManager


class MyCompanyManager(ObjectManager):
    manager_name = "my_company"
    manager_title = _("My Company")
    filter_form = MyCompanyFilterForm
    order_field = '-id'
    top_actions = [AddAction(MyCompanyForm)]

    actions = [EditAction(MyCompanyForm), DeleteAction()]

    def can_view(self):
        return True

    def get_all_data(self):
        return MyCompany.objects.all()

    def get_columns(self):
        columns = [
            ColumnManager('name', _("My Company"), '10', False),

        ]
        return columns


class MineManager(ObjectManager):
    manager_name = "mine"
    manager_title = _("Mines")
    filter_form = MineFilterForm
    order_field = '-id'
    top_actions = [AddAction(MineForm)]
    actions = [EditAction(MineForm), DeleteAction()]

    def can_view(self):
        return True

    def get_all_data(self):
        return Mine.objects.all()

    def get_columns(self):
        columns = [
            ColumnManager('name', _("Mine"), '10', False),
            ColumnManager('owner', _("Mine Owner"), '10', False),
            ColumnManager('company_name', _("Company name"), '10', False),
            ColumnManager('company_phone_number', _("Company phone number"), '10', False),
            ColumnManager('cell_phone_number', _("Cell phone number"), '10', False),
            ColumnManager('email', _("email"), '10', False),
        ]
        return columns


class SellWorkingManager(ObjectManager):
    manager_name = "sell_working"
    manager_title = _("Current sell")
    filter_form = SellFilterForm
    order_field = '-id'

    actions = [ViewDetailsSell(), EditSell()]

    def can_view(self):
        return True

    def get_all_data(self):
        return Deal.objects.filter(state=Deal.IN_PROGRESS, deal_type=Deal.SELL)

    def get_columns(self):
        columns = [
            ColumnManager('agreement_no', _("Agreement Number"), '10', False),
            ColumnManager('vessel_name', _("Vessel name"), '10', False),
        ]
        return columns


class SellDoneManager(ObjectManager):
    manager_name = "sell_done"
    manager_title = _("Completed sell")
    filter_form = SellFilterForm
    order_field = '-id'

    actions = [ViewDetailsSell()]

    def can_view(self):
        return True

    def get_all_data(self):
        return Deal.objects.filter(state=Deal.FINISHED, deal_type=Deal.SELL)

    def get_columns(self):
        columns = [
            ColumnManager('agreement_no', _("Agreement Number"), '10', False),
            ColumnManager('vessel_name', _("Vessel name"), '10', False),
        ]
        return columns


class PurchaseWorkingManager(ObjectManager):
    manager_name = "purchase_working"
    manager_title = _("Current purchase")
    filter_form = SellFilterForm
    order_field = '-id'

    actions = [ViewDetailsPurchase(), EditPurchase()]

    def can_view(self):
        return True

    def get_all_data(self):
        return Deal.objects.filter(state=Deal.IN_PROGRESS, deal_type=Deal.PURCHASE)

    def get_columns(self):
        columns = [
            ColumnManager('agreement_no', _("Agreement Number"), '10', False),
            ColumnManager('vessel_name', _("Vessel name"), '10', False),
        ]
        return columns


class PurchaseDoneManager(ObjectManager):
    manager_name = "purchase_done"
    manager_title = _("Completed purchase")
    filter_form = SellFilterForm
    order_field = '-id'

    actions = [ViewDetailsPurchase()]

    def can_view(self):
        return True

    def get_all_data(self):
        return Deal.objects.filter(state=Deal.FINISHED, deal_type=Deal.PURCHASE)

    def get_columns(self):
        columns = [
            ColumnManager('agreement_no', _("Agreement Number"), '10', False),
            ColumnManager('vessel_name', _("Vessel name"), '10', False),
        ]
        return columns
